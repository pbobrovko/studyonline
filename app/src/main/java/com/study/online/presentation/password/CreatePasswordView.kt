package com.study.online.presentation.password

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface CreatePasswordView : MvpView {

    fun showUserEmail(email: String)

    fun showBrokenPhoneNumberMessage()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    fun enableConfirmButton(isEnable: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showPasswordError(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showConfirmPasswordError(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showProgress(isShow: Boolean)
}