package com.study.online.presentation.privacypolicy

import com.study.online.BuildConfig
import com.study.online.app.di.PrimitiveWrapper
import com.study.online.app.di.PrivacyPolicyLabel
import com.study.online.core.global.ErrorHandler
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import io.reactivex.subjects.PublishSubject
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class TermsAndConditionsPresenter @Inject constructor(
    private val isFromRegistration: PrimitiveWrapper<Boolean>,
    private val flowRouter: FlowRouter,
    @PrivacyPolicyLabel private val privacyPolicySubject: PublishSubject<Boolean>,
    private val errorHandler: ErrorHandler
) : BasePresenter<TermsAndConditionsView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showConfirmButton(isFromRegistration.value)
        viewState.setLoadUrl(BuildConfig.TERMS_ENDPOINT)
    }

    fun onConfirmClicked() {
        privacyPolicySubject.onNext(true)
        flowRouter.finishFlow()
    }

    fun onBackPressed() {
        if (isFromRegistration.value) {
            flowRouter.exit()
        } else {
            flowRouter.finishFlow()
        }
    }

    fun onErrorReceived(error: Throwable, messageListener: (ErrorDialogType) -> Unit) {
        errorHandler.handleErrorDialog(error, messageListener)
    }
}