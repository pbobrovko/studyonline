package com.study.online.presentation.main.timetable

enum class TimetableLabelState {
    YESTERDAY,
    TODAY,
    TOMORROW,
    HIDE
}