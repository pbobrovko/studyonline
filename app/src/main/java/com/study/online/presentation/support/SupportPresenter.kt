package com.study.online.presentation.support

import android.content.Intent
import android.net.Uri
import com.study.online.core.featureflow.SupportScreens
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.domain.entity.socket.Roles
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class SupportPresenter @Inject constructor(
    private val flowRouter: FlowRouter
) : BasePresenter<SupportView>() {

    private var isPermissionGranted = false

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.checkPermission()
    }

    fun onBackPressed() {
        flowRouter.finishFlow()
    }

    fun onDownloadTeacherClicked() {
        flowRouter.navigateTo(SupportScreens.PdfViewerScreen(Roles.TEACHER))
    }

    fun onDownloadStudentClicked() {
        flowRouter.navigateTo(SupportScreens.PdfViewerScreen(Roles.STUDENT))
    }

    fun onDownloadGuestClicked() {
        flowRouter.navigateTo(SupportScreens.PdfViewerScreen(Roles.GUEST))
    }

    fun onPermissionResult(isGranted: Boolean) {
        isPermissionGranted = isGranted
    }

    fun onPhoneNumberClicked(number: String) {
        val uri = Uri.fromParts(TEL, number, null)
        viewState.startContactApplication(Intent(Intent.ACTION_DIAL, uri))
    }

    fun onEmailClicked(email: String) {
        val uri = Uri.fromParts(MAILTO, email, null)
        viewState.startContactApplication(Intent(Intent.ACTION_SENDTO, uri))
    }

    companion object {
        private const val TEL = "tel"
        private const val MAILTO = "mailto"
    }
}