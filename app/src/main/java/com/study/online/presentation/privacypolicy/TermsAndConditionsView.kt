package com.study.online.presentation.privacypolicy

import moxy.MvpView

interface TermsAndConditionsView : MvpView {

    fun showConfirmButton(isShow: Boolean)

    fun setLoadUrl(url: String)
}