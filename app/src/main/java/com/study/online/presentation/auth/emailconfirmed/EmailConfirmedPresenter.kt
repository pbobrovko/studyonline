package com.study.online.presentation.auth.emailconfirmed

import com.study.online.app.di.MaskedEmailLabel
import com.study.online.app.di.PrimitiveWrapper
import com.study.online.app.di.ResendDataLabel
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.domain.entity.Email
import com.study.online.domain.entity.ResetPasswordRequest
import com.study.online.domain.interactor.AuthInteractor
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class EmailConfirmedPresenter @Inject constructor(
    @MaskedEmailLabel private val maskedEmail: String,
    @ResendDataLabel private val resendData: String,
    private val flowRouter: FlowRouter,
    private val authInteractor: AuthInteractor,
    private val router: Router,
    private val errorHandler: ErrorHandler,
    private val isRegistration: PrimitiveWrapper<Boolean>
) : BasePresenter<EmailConfirmedView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showMaskedEmail(maskedEmail)
    }

    fun onResendMailClicked() {
        if (isRegistration.value) {
            authInteractor
                .registerEmail(Email(resendData))
                .subscribe(
                    // we don't have to do anything if successful
                    {},
                    { errorHandler.handleError(it) { message -> viewState.showMessage(message) } }
                )
                .connect()
        } else {
            authInteractor
                .resetPassword(ResetPasswordRequest(resendData))
                .subscribe(
                    // we don't have to do anything if successful
                    {},
                    { errorHandler.handleError(it) { message -> viewState.showMessage(message) } }
                )
                .connect()
        }
    }

    fun onSupportServiceClicked() {
        router.navigateTo(Screens.SupportFlow)
    }

    fun onBackPressed() {
        flowRouter.exit()
    }
}