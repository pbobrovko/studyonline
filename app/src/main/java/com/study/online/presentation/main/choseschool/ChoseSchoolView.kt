package com.study.online.presentation.main.choseschool

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface ChoseSchoolView : MvpView {

    fun setSchools(schoolNames: List<String>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showSelectedSchool(schoolName: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showSchoolError(message: String)
}