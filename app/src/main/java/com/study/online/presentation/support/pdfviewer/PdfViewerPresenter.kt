package com.study.online.presentation.support.pdfviewer

import com.study.online.core.global.ErrorHandler
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.domain.entity.socket.Roles
import com.study.online.domain.interactor.FileInteractor
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class PdfViewerPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val errorHandler: ErrorHandler,
    private val role: Roles,
    private val fileInteractor: FileInteractor
) : BasePresenter<PdfViewerView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        loadPdf()
    }

    private fun loadPdf() {
        fileInteractor
            .getRolePdf(role)
            .subscribe(
                { viewState.showPdf(it) },
                { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
            )
            .connect()
    }

    fun onBackPressed() {
        flowRouter.exit()
    }
}