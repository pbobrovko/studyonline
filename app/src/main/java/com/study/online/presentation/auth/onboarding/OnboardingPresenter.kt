package com.study.online.presentation.auth.onboarding

import com.study.online.app.di.PrivacyPolicyLabel
import com.study.online.core.featureflow.AuthScreens
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import io.reactivex.subjects.PublishSubject
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class OnboardingPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    @PrivacyPolicyLabel private val privacyPolicySubject: PublishSubject<Boolean>
) : BasePresenter<OnboardingView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        privacyPolicySubject
            .subscribe(
                { flowRouter.navigateTo(AuthScreens.SignUpScreen) },
                {}
            )
            .connect()
    }

    fun onGuestLoginClicked() {
        flowRouter.startFlow(AuthScreens.GuestLoginScreen(""))
    }

    fun onLoginClicked() {
        flowRouter.navigateTo(AuthScreens.LoginScreen)
    }

    fun onSignUpClicked() {
        flowRouter.startFlow(Screens.PrivacyPolicyFlow(true, false))
    }

    fun onSupportClicked() {
        flowRouter.startFlow(Screens.SupportFlow)
    }

    fun onBackPressed() {
        flowRouter.finishFlow()
    }
}