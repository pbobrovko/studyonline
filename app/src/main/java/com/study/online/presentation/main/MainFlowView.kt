package com.study.online.presentation.main

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainFlowView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun openDrawer()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showLanguageMenu()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showUserMenu()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showUserInitials(initials: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun recreateActivity()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    fun showCurrentLanguage(language: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun initDrawer()
}