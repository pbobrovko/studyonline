package com.study.online.presentation.support

import android.content.Intent
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface SupportView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun startContactApplication(intent: Intent)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun checkPermission()
}