package com.study.online.presentation.auth.signup

import android.util.Patterns
import com.study.online.R
import com.study.online.core.featureflow.AuthScreens
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.ResourceManager
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.domain.entity.Email
import com.study.online.domain.exceptions.ServerException
import com.study.online.domain.interactor.AuthInteractor
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class SignUpPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val router: Router,
    private val authInteractor: AuthInteractor,
    private val errorHandler: ErrorHandler,
    private val resourceManager: ResourceManager
) : BasePresenter<SignUpView>() {

    fun onGetLinkClicked(userEmail: String) {
        authInteractor
            .registerEmail(Email(userEmail))
            .subscribe(
                { maskedEmail ->
                    flowRouter.navigateTo(
                        AuthScreens.AuthActionConfirmedScreen(maskedEmail.email, userEmail, true)
                    )
                },
                {
                    if (it is ServerException && it.code == CODE_EMAIL_NOT_FOUND) {
                        viewState.showEmailNotFoundError(
                            resourceManager.getString(R.string.email_not_found)
                        )
                    } else {
                        errorHandler.handleError(it) { message -> viewState.showMessage(message) }
                    }
                }
            )
            .connect()
    }

    fun onGoToLoginClicked() {
        flowRouter.navigateTo(AuthScreens.LoginScreen)
    }

    fun onSupportClicked() {
        router.navigateTo(Screens.SupportFlow)
    }

    fun onEmailChanged(email: String) {
        viewState.enableLoginButton(Patterns.EMAIL_ADDRESS.matcher(email).matches())
    }

    fun onBackPressed() {
        flowRouter.exit()
    }

    companion object {
        private const val CODE_EMAIL_NOT_FOUND = 404
    }
}