package com.study.online.presentation.main.lesson

import com.study.online.domain.entity.socket.lesson.LessonParticipate
import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import com.study.online.domain.entity.socket.lessonboard.BoardSize
import com.study.online.domain.entity.socket.lessonboard.ShapeData
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface LessonView : MvpView {

    fun showLessonName(name: String)

    fun showLessonTheme(theme: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun checkPermissions(permissions: List<String>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun requestPermissions(permissions: List<String>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun setUserFrameListener(listener: OnFrameListener)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun setTeacherFrameListener(listener: OnFrameListener)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showPresenters(presenters: List<Pair<OnFrameListener, LessonParticipate>>)

    fun showUserStatus(text: String, iconId: Int, colorId: Int)

    fun showUserName(name: String)

    fun showTeacherName(name: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showErrorDialog(typeErrorDialog: ErrorDialogType)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showLessonEndedDialog()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun drawShapes(shapesData: List<ShapeData>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun undoShape(shapeData: ShapeData)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun cleanBoard()

    fun setBoardSize(size: BoardSize)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showBoard(isShow: Boolean)
}