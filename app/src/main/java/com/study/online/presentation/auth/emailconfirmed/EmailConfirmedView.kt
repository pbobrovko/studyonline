package com.study.online.presentation.auth.emailconfirmed

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface EmailConfirmedView : MvpView {

    fun showMaskedEmail(email: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}