package com.study.online.presentation.main.lesson

import android.os.Handler
import android.os.Looper
import com.study.online.coreui.view.TextureViewRenderer
import org.webrtc.VideoRenderer

class OnFrameListener {

    private val handler = Handler(Looper.getMainLooper())
    private var target: TextureViewRenderer? = null

    val renderer = VideoRenderer.Callbacks {
        handler.post {
            target?.renderFrame(it)
            onTargetRemoved?.invoke(false)
        }
    }
    var onTargetRemoved: ((Boolean) -> Unit)? = null

    fun setTarget(target: TextureViewRenderer?) {
        if (target == null) {
            onTargetRemoved?.invoke(true)
        }

        this.target = target
    }

    fun release() {
        target?.release()
    }
}