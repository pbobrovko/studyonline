package com.study.online.presentation.main.lesson.chat

import com.study.online.core.global.ErrorHandler
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.domain.entity.TimetableLesson
import com.study.online.domain.exceptions.InvalidUserDataException
import com.study.online.domain.interactor.LessonInteractor
import com.study.online.domain.interactor.ProfileInteractor
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class ChatPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val lessonInteractor: LessonInteractor,
    private val errorHandler: ErrorHandler,
    private val timetableLesson: TimetableLesson,
    private val profileInteractor: ProfileInteractor
) : BasePresenter<ChatView>() {

    private var isFirstList = true

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        val user = profileInteractor.user
        if (user == null) {
            errorHandler.handleErrorDialog(InvalidUserDataException())
        } else {
            viewState.initChatAdapter(user.userName)
        }

        lessonInteractor
            .observeChatMessages()
            .subscribe(
                {
                    viewState.setItems(it)

                    if (isFirstList && it.isNotEmpty()) {
                        viewState.scrollToEndOfChat(it.size - 1)
                        isFirstList = false
                    }
                },
                { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
            )
            .connect()
    }

    fun onSendMessageClicked(message: String) {
        if (message.isNotBlank()) {
            lessonInteractor
                .sendChatMessage(message, timetableLesson.id.toString())
                .subscribe(
                    {},
                    { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
                )
                .connect()
        }
    }

    fun onBackPressed() {
        flowRouter.exit()
    }
}