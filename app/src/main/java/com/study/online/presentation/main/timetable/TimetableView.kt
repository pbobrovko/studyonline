package com.study.online.presentation.main.timetable

import com.study.online.domain.entity.Timetable
import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface TimetableView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    fun showTimetable(timetables: List<Timetable>, atPosition: Int?)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showAdapterDate(date: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showScrollButton(isShow: Boolean, isScrollUp: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showTimetableLabel(labelState: TimetableLabelState)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun scrollToCurrentDate(position: Int)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showLessonOverflowDialog()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showErrorDialog(typeErrorDialog: ErrorDialogType)

    fun showProgress(isShow: Boolean)
}