package com.study.online.presentation.privacypolicy

import com.study.online.BuildConfig
import com.study.online.app.di.PrimitiveWrapper
import com.study.online.core.featureflow.PrivacyPolicyScreens
import com.study.online.core.global.ErrorHandler
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class PrivacyPolicyPresenter @Inject constructor(
    private val isFromRegistration: PrimitiveWrapper<Boolean>,
    private val flowRouter: FlowRouter,
    private val errorHandler: ErrorHandler
) : BasePresenter<PrivacyPolicyView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showConfirmButton(isFromRegistration.value)
        viewState.setLoadUrl(BuildConfig.POLICY_ENDPOINT)
    }

    fun onConfirmClicked() {
        flowRouter.navigateTo(
            PrivacyPolicyScreens.TermsAndConditionsScreen(isFromRegistration.value)
        )
    }

    fun onBackPressed() {
        flowRouter.finishFlow()
    }

    fun onErrorReceived(error: Throwable, messageListener: (ErrorDialogType) -> Unit) {
        errorHandler.handleErrorDialog(error, messageListener)
    }
}