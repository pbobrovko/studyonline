package com.study.online.presentation.password

import com.study.online.R
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.ResourceManager
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.domain.entity.CreatePasswordInfo
import com.study.online.domain.entity.CreatePassword
import com.study.online.domain.interactor.AuthInteractor
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import java.util.regex.Pattern
import javax.inject.Inject

@InjectViewState
class CreatePasswordPresenter @Inject constructor(
    private val router: Router,
    private val createPasswordInfo: CreatePasswordInfo,
    private val emailCode: String,
    private val authInteractor: AuthInteractor,
    private val errorHandler: ErrorHandler,
    private val resourceManager: ResourceManager
) : BasePresenter<CreatePasswordView>() {

    private val pattern = Pattern.compile(PASSWORD_PATTERN)
    private val specialSymbolsPattern = Pattern.compile(SPECIAL_SYMBOLS_PATTERN)

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showUserEmail(createPasswordInfo.email)
    }

    fun onConfirmClicked(password: String, confirmPassword: String) {
        if (password == confirmPassword) {
            authInteractor
                .createPassword(CreatePassword(createPasswordInfo.user, password, emailCode))
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                    { router.newRootScreen(Screens.MainFlow) },
                    { errorHandler.handleError(it) { message -> viewState.showMessage(message) } }
                )
                .connect()
        } else {
            viewState.showConfirmPasswordError(
                resourceManager.getString(R.string.password_not_match)
            )
        }
    }

    fun onFieldsChanged(password: String) {
        val isEnable = when {
            !pattern.matcher(password).matches() ||
                    specialSymbolsPattern.matcher(password).matches()
            -> {
                viewState.showPasswordError(resourceManager.getString(R.string.password_incorrect))
                false
            }
            password.length > MAXIMUM_PASSWORD_LENGTH -> {
                viewState.showPasswordError(resourceManager.getString(R.string.password_to_long))
                false
            }
            else -> {
                viewState.showPasswordError("")
                true
            }
        }

        viewState.enableConfirmButton(isEnable)
    }

    fun onBackPressed() {
        router.exit()
    }

    companion object {
        private const val PASSWORD_PATTERN = "^(?=.*?[A-Z])(?=.*?[0-9])\\S{8,}\$"
        private const val SPECIAL_SYMBOLS_PATTERN =
            "^(?=.*?[!#\$%&€£¥₩₽×÷‘•(){|}~:-;<=>?@*+,./^_`'“\" \\t\\r\\n\\f-]).*\$"

        private const val MAXIMUM_PASSWORD_LENGTH = 50
    }
}