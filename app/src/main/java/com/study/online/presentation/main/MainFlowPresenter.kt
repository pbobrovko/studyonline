package com.study.online.presentation.main

import com.study.online.R
import com.study.online.core.featureflow.MainScreens
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.ResourceManager
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.data.storage.Prefs
import com.study.online.domain.interactor.AuthInteractor
import com.study.online.domain.interactor.LessonInteractor
import com.study.online.domain.interactor.ProfileInteractor
import com.study.online.extension.LOCALE_KAZAKHSTAN
import com.study.online.extension.LOCALE_RUSSIAN
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import java.util.*
import javax.inject.Inject

@InjectViewState
class MainFlowPresenter @Inject constructor(
    private val prefs: Prefs,
    private val lessonInteractor: LessonInteractor,
    private val authInteractor: AuthInteractor,
    private val router: Router,
    private val resourceManager: ResourceManager,
    private val errorHandler: ErrorHandler,
    private val profileInteractor: ProfileInteractor
) : BasePresenter<MainFlowView>() {

    fun onFlowStarted() {
        handleCurrentLanguage()

        if (prefs.currentSchool == null) {
            loadUserProfile()
        } else {
            viewState.initDrawer()
            createUserInitials()
        }
    }

    fun onBurgerClicked() {
        viewState.openDrawer()
    }

    fun onLanguageMenuClicked() {
        viewState.showLanguageMenu()
    }

    fun onNotificationsClicked() {
    }

    fun onUserMenuClicked() {
        viewState.showUserMenu()
    }

    fun onLanguageItemClicked(itemId: Int) {
        val locale = if (itemId == R.id.languageRus) {
            LOCALE_RUSSIAN
        } else {
            LOCALE_KAZAKHSTAN
        }

        prefs.currentLocale = locale
        viewState.recreateActivity()
        handleCurrentLanguage()
    }

    fun onUserItemClicked(itemId: Int) {
        if (itemId == R.id.userLogout) {
            logout()
        } else {
            router.navigateTo(Screens.SupportFlow)
        }
    }

    fun onBackPressed() {
        router.exit()
    }

    fun onSupportClicked() {
        router.navigateTo(Screens.SupportFlow)
    }

    fun onTermsAndConditionsClicked() {
        router.navigateTo(Screens.PrivacyPolicyFlow(false, true))
    }

    fun onPrivacyPolicyClicked() {
        router.navigateTo(Screens.PrivacyPolicyFlow(false, false))
    }

    private fun loadUserProfile() {
        lessonInteractor
            .getUser()
            .subscribe(
                {
                    val schools = it.schools
                    when (schools.size) {
                        ONE_SCHOOL -> {
                            prefs.currentSchool = schools[0]
                            viewState.initDrawer()
                        }
                        else -> router.navigateTo(MainScreens.ChoseSchoolScreen(schools))
                    }
                    createUserInitials()
                },
                {
                    logout()
                }
            )
            .connect()
    }

    private fun createUserInitials() {
        profileInteractor.user?.let {
            val firstName = it.firstName
            val lastName = it.lastName
            val firstCharacter = if (firstName.isNotEmpty()) {
                firstName.substring(INITIALS_START_POS, INITIALS_END_POS)
            } else {
                ""
            }
            val lastCharacter = if (lastName.isNotEmpty()) {
                lastName.substring(INITIALS_START_POS, INITIALS_END_POS)
            } else {
                ""
            }
            viewState.showUserInitials(
                "$firstCharacter$lastCharacter".toUpperCase(Locale.getDefault())
            )
        }
    }

    private fun handleCurrentLanguage() {
        val languageId = if (prefs.currentLocale == LOCALE_RUSSIAN) {
            R.string.rus
        } else {
            R.string.kaz
        }

        viewState.showCurrentLanguage(resourceManager.getString(languageId))
    }

    private fun logout() {
        authInteractor
            .logout()
            .subscribe(
                { router.newRootScreen(Screens.AuthFlow) },
                { errorHandler.handleError(it) { message -> viewState.showMessage(message) } }
            )
            .connect()
    }

    companion object {
        private const val ONE_SCHOOL = 1
        private const val INITIALS_START_POS = 0
        private const val INITIALS_END_POS = 1
    }
}