package com.study.online.presentation.main.choseschool

import com.study.online.R
import com.study.online.core.global.ResourceManager
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.data.storage.Prefs
import com.study.online.domain.entity.School
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class ChooseSchoolPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val schools: List<School>,
    private val prefs: Prefs,
    private val resourceManager: ResourceManager
) : BasePresenter<ChoseSchoolView>() {

    private var currentSchool = prefs.currentSchool

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.setSchools(schools.map { it.name })
    }

    override fun attachView(view: ChoseSchoolView?) {
        super.attachView(view)

        currentSchool?.let { viewState.showSelectedSchool(it.name) }
    }

    fun onSchoolChanged(schoolName: String) {
        currentSchool = schools.find { it.name == schoolName }
    }

    fun onChoseClicked() {
        if (currentSchool != null || currentSchool?.name?.isNotEmpty() == true) {
            prefs.currentSchool = currentSchool
            flowRouter.finishFlow()
        } else {
            viewState.showSchoolError(resourceManager.getString(R.string.school_error))
        }
    }

    fun onSupportClicked() {
        flowRouter.startFlow(Screens.SupportFlow)
    }

    fun onBackPressed() {
        flowRouter.finishChain()
    }
}