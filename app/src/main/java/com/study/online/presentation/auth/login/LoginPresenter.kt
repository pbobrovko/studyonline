package com.study.online.presentation.auth.login

import com.study.online.R
import com.study.online.core.featureflow.AuthScreens
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.ResourceManager
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.domain.exceptions.ServerException
import com.study.online.domain.interactor.AuthInteractor
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class LoginPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val router: Router,
    private val resourceManager: ResourceManager,
    private val authInteractor: AuthInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<LoginView>() {

    fun onLoginClicked(user: String, password: String) {
        when {
            user.isEmpty() -> {
                viewState.showLoginError(resourceManager.getString(R.string.enter_login))
                viewState.showPasswordError("")
            }
            password.isEmpty() -> {
                viewState.showPasswordError(resourceManager.getString(R.string.enter_password))
                viewState.showLoginError("")
            }
            else -> {
                viewState.showLoginError("")
                viewState.showPasswordError("")
                login(user, password)
            }
        }
    }

    fun onGoToSignUpClicked() {
        flowRouter.replaceScreen(AuthScreens.SignUpScreen)
    }

    fun onSupportClicked() {
        router.navigateTo(Screens.SupportFlow)
    }

    fun onForgotPasswordClicked() {
        flowRouter.navigateTo(AuthScreens.ResetPasswordScreen)
    }

    fun onBackPressed() {
        flowRouter.exit()
    }

    private fun login(user: String, password: String) {
        authInteractor
            .login(user, password)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { router.newRootScreen(Screens.MainFlow) },
                {
                    if (it is ServerException && it.code == CODE_ACCOUNT_NOT_FOUND) {
                        viewState.showMessage(resourceManager.getString(R.string.login_error))
                    } else {
                        errorHandler.handleError(it) { message ->
                            viewState.showMessage(message)
                        }
                    }
                }
            )
            .connect()
    }

    companion object {
        private const val CODE_ACCOUNT_NOT_FOUND = 400
    }
}