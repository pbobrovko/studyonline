package com.study.online.presentation.auth.guestlogin

import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface GuestLoginView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showProgress(isShow: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showInviteLink(link: String)

    fun showLastNameError(message: String)

    fun showFirstNameError(message: String)

    fun showPatronymicError(message: String)

    fun showInviteLinkError(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showLessonOverflowDialog()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showErrorDialog(typeErrorDialog: ErrorDialogType)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showLessonEndedDialog()
}