package com.study.online.presentation.main.timetable

import com.study.online.core.global.DateFormatter
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import com.study.online.domain.entity.Timetable
import com.study.online.domain.entity.TimetableDate
import com.study.online.domain.entity.TimetableLesson
import com.study.online.domain.entity.socket.Roles
import com.study.online.domain.entity.toTimetableDate
import com.study.online.domain.exceptions.LessonOverflowException
import com.study.online.domain.interactor.LessonInteractor
import com.study.online.domain.interactor.ProfileInteractor
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class TimetablePresenter @Inject constructor(
    private val router: Router,
    private val errorHandler: ErrorHandler,
    private val lessonInteractor: LessonInteractor,
    private val dateFormatter: DateFormatter,
    private val profileInteractor: ProfileInteractor
) : BasePresenter<TimetableView>() {

    private val timetables = mutableListOf<Timetable>()
    private var currentDatePosition: Int? = null

    override fun attachView(view: TimetableView?) {
        super.attachView(view)

        lessonInteractor
            .getLessons()
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { timetableData ->
                    timetables.clear()
                    timetableData.content.forEach {
                        val timetableDate = it.toTimetableDate()
                        timetables.add(timetableDate)
                        timetables.addAll(it.timetableLessons)

                        val isCurrentDate = dateFormatter.isToday(timetableDate.date)
                        if (isCurrentDate) {
                            currentDatePosition = timetables.indexOf(timetableDate)
                        }
                    }

                    viewState.showTimetable(timetables, currentDatePosition)
                },
                { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
            )
            .connect()
    }

    fun onLessonClicked(position: Int) {}

    fun onEnterLessonClicked(position: Int) {
        if (profileInteractor.user?.role == Roles.TEACHER) {
            viewState.showErrorDialog(ErrorDialogType.FEATURE_IN_DEVELOPMENT)
            return
        }

        val lesson = timetables[position] as TimetableLesson
        val lessonId = lesson.id.toString()
        lessonInteractor
            .connectToLesson(lessonId)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { router.navigateTo(Screens.LessonFlow(lesson)) },
                {
                    if (it is LessonOverflowException) {
                        viewState.showLessonOverflowDialog()
                    } else {
                        errorHandler.handleErrorDialog(it) { type ->
                            viewState.showErrorDialog(type)
                        }
                    }
                }
            )
            .connect()
    }

    fun onAdapterPositionChanged(position: Int) {
        when {
            position < currentDatePosition ?: Int.MIN_VALUE ->
                viewState.showScrollButton(true, false)
            position > currentDatePosition ?: Int.MAX_VALUE ->
                viewState.showScrollButton(true, true)
            else -> viewState.showScrollButton(false, false)
        }

        when (val item = timetables[position]) {
            is TimetableLesson -> {
                updateTimetableDate(item.time)
            }
            is TimetableDate -> {
                updateTimetableDate(item.date)
            }
        }
    }

    fun onScrollClicked() {
        currentDatePosition?.let { viewState.scrollToCurrentDate(it) }
    }

    private fun updateTimetableDate(time: String) {
        viewState.showAdapterDate(dateFormatter.formatToTimetableDate(time))

        val timetableLabel = when {
            dateFormatter.isToday(time) -> TimetableLabelState.TODAY
            dateFormatter.isYesterday(time) -> TimetableLabelState.YESTERDAY
            dateFormatter.isTomorrow(time) -> TimetableLabelState.TOMORROW
            else -> TimetableLabelState.HIDE
        }
        viewState.showTimetableLabel(timetableLabel)
    }
}