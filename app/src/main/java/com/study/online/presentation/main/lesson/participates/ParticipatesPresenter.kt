package com.study.online.presentation.main.lesson.participates

import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class ParticipatesPresenter @Inject constructor(
    private val flowRouter: FlowRouter
) : BasePresenter<ParticipatesView>() {

    fun onBackPressed() {
        flowRouter.exit()
    }
}