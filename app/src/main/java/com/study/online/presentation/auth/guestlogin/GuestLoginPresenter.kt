package com.study.online.presentation.auth.guestlogin

import android.net.Uri
import com.study.online.R
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.ResourceManager
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.domain.exceptions.LessonEndedException
import com.study.online.domain.exceptions.LessonOverflowException
import com.study.online.domain.interactor.AuthInteractor
import com.study.online.domain.interactor.LessonInteractor
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import java.util.regex.Pattern
import javax.inject.Inject

@InjectViewState
class GuestLoginPresenter @Inject constructor(
    private val router: Router,
    private val inviteLink: String,
    private val resourceManager: ResourceManager,
    private val lessonInteractor: LessonInteractor,
    private val authInteractor: AuthInteractor,
    private val errorHandler: ErrorHandler
) : BasePresenter<GuestLoginView>() {

    private val pattern = Pattern.compile(SPECIAL_SYMBOLS_PATTERN)
    private val requiredField = resourceManager.getString(R.string.required_field)
    private val invalidCharacter = resourceManager.getString(R.string.invalid_character)
    private val invalidGuestLink = resourceManager.getString(R.string.invalid_guest_link)

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showInviteLink(inviteLink)
    }

    fun onRegisterGuestClicked(
        inviteLink: String,
        lastName: String,
        firstName: String,
        patronymic: String
    ) {
        val uri = Uri.parse(inviteLink)

        val isSuccess = when {
            inviteLink.isBlank() -> {
                viewState.showInviteLinkError(requiredField)
                false
            }
            uri.pathSegments.size < GUEST_LINK_SIZE -> {
                viewState.showInviteLinkError(invalidGuestLink)
                false
            }
            lastName.isBlank() -> {
                viewState.showLastNameError(requiredField)
                false
            }
            pattern.matcher(lastName).find() -> {
                viewState.showLastNameError(invalidCharacter)
                false
            }
            firstName.isBlank() -> {
                viewState.showFirstNameError(requiredField)
                false
            }
            pattern.matcher(firstName).find() -> {
                viewState.showFirstNameError(invalidCharacter)
                false
            }
            patronymic.isBlank() -> {
                viewState.showPatronymicError(requiredField)
                false
            }
            pattern.matcher(patronymic).find() -> {
                viewState.showPatronymicError(invalidCharacter)
                false
            }
            else -> true
        }

        if (inviteLink.isNotBlank() && uri.pathSegments.size >= GUEST_LINK_SIZE) {
            viewState.showInviteLinkError("")
        }

        if (lastName.isNotBlank() && !pattern.matcher(lastName).find()) {
            viewState.showLastNameError("")
        }

        if (firstName.isNotBlank() && !pattern.matcher(firstName).find()) {
            viewState.showFirstNameError("")
        }

        if (patronymic.isNotBlank() && !pattern.matcher(patronymic).find()) {
            viewState.showPatronymicError("")
        }

        if (isSuccess) {
            val refLessonId = uri.pathSegments[2]
            val lessonId = uri.pathSegments[3]
            lessonInteractor
                .joinAsGuest(refLessonId, firstName, lastName, patronymic, lessonId)
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                    { router.replaceScreen(Screens.LessonFlow(it)) },
                    {
                        when (it) {
                            is LessonOverflowException -> viewState.showLessonOverflowDialog()
                            is LessonEndedException -> viewState.showLessonEndedDialog()
                            else -> {
                                errorHandler.handleErrorDialog(it) { type ->
                                    viewState.showErrorDialog(type)
                                }
                            }
                        }
                    }
                )
                .connect()
        }
    }

    fun onTechSupportClicked() {
        router.navigateTo(Screens.SupportFlow)
    }

    fun onTermsAndConditionsClicked() {
        router.navigateTo(Screens.PrivacyPolicyFlow(false, true))
    }

    fun onPrivacyPolicyClicked() {
        router.navigateTo(Screens.PrivacyPolicyFlow(false, false))
    }

    fun onBackPressed() {
        router.exit()
    }

    companion object {
        private const val GUEST_LINK_SIZE = 4

        private const val SPECIAL_SYMBOLS_PATTERN =
            "^(?=.*?[!#\$%&€£¥₩₽×÷‘•(){|}~:-;<=>?@*+,./^_`'\" \\t\\r\\n\\f-]).*\$"
    }
}