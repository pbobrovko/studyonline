package com.study.online.presentation.auth.login

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface LoginView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showLoginError(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showPasswordError(message: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    fun enableLoginButton(isEnable: Boolean)

    fun showProgress(isShow: Boolean)
}