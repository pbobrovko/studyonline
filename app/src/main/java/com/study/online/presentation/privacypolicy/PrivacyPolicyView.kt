package com.study.online.presentation.privacypolicy

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface PrivacyPolicyView : MvpView {

    fun showConfirmButton(isShow: Boolean)

    fun setLoadUrl(url: String)
}