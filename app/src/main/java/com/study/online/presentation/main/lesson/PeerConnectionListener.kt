package com.study.online.presentation.main.lesson

import com.study.online.domain.entity.socket.lesson.LessonParticipate

interface PeerConnectionListener {

    fun setUserFrameListener(listener: OnFrameListener)

    fun setTeacherFrameListener(listener: OnFrameListener)

    fun showPresenters(presenters: List<Pair<OnFrameListener, LessonParticipate>>)

    fun showTeacherName(teacher: LessonParticipate?)
}