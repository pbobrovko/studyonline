package com.study.online.presentation.support.pdfviewer

import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType
import java.io.File

@StateStrategyType(AddToEndSingleStrategy::class)
interface PdfViewerView : MvpView {

    fun showPdf(file: File)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showErrorDialog(typeErrorDialog: ErrorDialogType)
}