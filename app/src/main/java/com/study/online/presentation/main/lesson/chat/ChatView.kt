package com.study.online.presentation.main.lesson.chat

import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import com.study.online.domain.entity.socket.lesson.ChatMessage
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface ChatView : MvpView {

    fun initChatAdapter(userName: String)

    fun setItems(items: List<ChatMessage>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showErrorDialog(typeErrorDialog: ErrorDialogType)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun scrollToEndOfChat(position: Int)
}