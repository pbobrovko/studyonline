package com.study.online.presentation.main.lesson

import com.study.online.R
import com.study.online.core.featureflow.LessonScreens
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.ResourceManager
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.domain.entity.TimetableLesson
import com.study.online.domain.entity.socket.Roles
import com.study.online.domain.entity.socket.lesson.LessonParticipate
import com.study.online.domain.entity.socket.lesson.PresenterStatus
import com.study.online.domain.entity.socket.lessonboard.BoardDataTypes
import com.study.online.domain.entity.socket.lessonboard.ShapeData
import com.study.online.domain.interactor.BoardInteractor
import com.study.online.domain.interactor.LessonInteractor
import com.study.online.domain.interactor.ProfileInteractor
import com.study.online.extension.getName
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class LessonPresenter @Inject constructor(
    private val router: Router,
    private val timetableLesson: TimetableLesson,
    private val flowRouter: FlowRouter,
    private val lessonInteractor: LessonInteractor,
    private val resourceManager: ResourceManager,
    private val peerConnectionDelegate: PeerConnectionDelegate,
    private val errorHandler: ErrorHandler,
    private val profileInteractor: ProfileInteractor,
    private val boardInteractor: BoardInteractor
) : BasePresenter<LessonView>(), PeerConnectionListener {

    private var prevPresenterStatus = PresenterStatus.NOT_READY_TO_ANSWER
    private var presenterStatus = PresenterStatus.NOT_READY_TO_ANSWER
    private var isPermissionGranted = false
    private val boardShapes = mutableListOf<ShapeData>()
    private var isBoardInitialized = false

    private val permissions = listOf(
        android.Manifest.permission.CAMERA,
        android.Manifest.permission.RECORD_AUDIO
    )

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showLessonName(timetableLesson.subject)
        viewState.showLessonTheme(timetableLesson.topic)
        viewState.checkPermissions(permissions)

        profileInteractor.user?.let {
            val userName = String.getName(
                it.firstName,
                it.lastName,
                it.patronymic
            )

            viewState.showUserName(userName)
        }

        handlePresenterStatus()
        peerConnectionDelegate.setPeerConnectionListener(this)
    }

    override fun attachView(view: LessonView?) {
        super.attachView(view)

        if (isPermissionGranted) {
            peerConnectionDelegate.onAttachView()
        }
    }

    override fun destroyView(view: LessonView?) {
        super.destroyView(view)

        peerConnectionDelegate.onDestroyView()
    }

    override fun onDestroy() {
        lessonInteractor
            .leaveLesson(timetableLesson.id.toString())
            .doOnTerminate {
                super.onDestroy()

                peerConnectionDelegate.onDestroy()
            }
            .subscribe({}, {})
            .connect()
    }

    override fun setUserFrameListener(listener: OnFrameListener) {
        viewState.setUserFrameListener(listener)
    }

    override fun setTeacherFrameListener(listener: OnFrameListener) {
        viewState.setTeacherFrameListener(listener)
    }

    override fun showPresenters(presenters: List<Pair<OnFrameListener, LessonParticipate>>) {
        viewState.showPresenters(presenters)
    }

    override fun showTeacherName(teacher: LessonParticipate?) {
        val name = if (teacher != null) {
            String.getName(
                teacher.userData.firstName,
                teacher.userData.lastName,
                teacher.userData.patronymic
            )
        } else {
            ""
        }
        viewState.showTeacherName(name)
    }

    fun onParticipantsClicked() {
        flowRouter.navigateTo(LessonScreens.LessonParticipatesScreen)
    }

    fun onChatClicked() {
        flowRouter.navigateTo(LessonScreens.LessonChatScreen(timetableLesson))
    }

    fun onHandUpClicked() {
        if (presenterStatus == PresenterStatus.PRESENTER) {
            return
        }

        prevPresenterStatus = presenterStatus
        presenterStatus = if (presenterStatus == PresenterStatus.NOT_READY_TO_ANSWER) {
            PresenterStatus.READY_TO_ANSWER
        } else {
            PresenterStatus.NOT_READY_TO_ANSWER
        }
        handlePresenterStatus()

        lessonInteractor
            .handUp(presenterStatus, timetableLesson)
            .subscribe(
                {},
                {
                    presenterStatus = prevPresenterStatus
                    handlePresenterStatus()
                    errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) }
                }
            )
            .connect()
    }

    fun onSettingsClicked() {
        // TODO: handle settings click
    }

    fun onPermissionsGranted(isGranted: Boolean) {
        if (isGranted) {
            joinLesson()
            peerConnectionDelegate.initLocalMedia()
            peerConnectionDelegate.onAttachView()
        } else {
            viewState.requestPermissions(permissions)
        }
        isPermissionGranted = isGranted
    }

    fun onBackPressed() {
        router.exit()
    }

    fun onLessonEndDialogDismissed() {
        router.exit()
    }

    fun onBoardSizeChanged() {
        isBoardInitialized = true

        if (boardShapes.isNotEmpty()) {
            viewState.drawShapes(boardShapes)
        }
    }

    private fun joinLesson() {
        lessonInteractor
            .joinLesson(timetableLesson.id.toString())
            .subscribe(
                {
                    observeLessonEvents()
                    observeBoardEvents()
                },
                { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
            )
            .connect()
    }

    private fun observeLessonEvents() {
        lessonInteractor
            .observeLessonParticipates()
            .subscribe(
                {
                    peerConnectionDelegate.onParticipantsChange(it)
                    findTeacher(it)
                },
                { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
            )
            .connect()

        // TODO: handle new chat messages
        lessonInteractor
            .observeChatMessages()
            .subscribe({}, {})
            .connect()

        lessonInteractor
            .observeHandUpEvents()
            .subscribe(
                {
                    if (it.userName == profileInteractor.user?.userName ?: "") {
                        presenterStatus = it.presenterStatus
                        handlePresenterStatus()
                    }
                    peerConnectionDelegate.onHandUpEvent(it)
                },
                { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
            )
            .connect()

        lessonInteractor
            .observeFinishLesson()
            .subscribe(
                { viewState.showLessonEndedDialog() },
                { router.exit() }
            )
            .connect()
    }

    private fun observeBoardEvents() {
        boardInteractor
            .observeBoardData()
            .subscribe(
                { handleShapeEvent(it) },
                { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
            )
            .connect()

        boardInteractor
            .observeBoardState()
            .subscribe(
                { viewState.showBoard(it) },
                { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
            )
            .connect()

        boardInteractor
            .observeBoardSize()
            .subscribe(
                { viewState.setBoardSize(it) },
                { errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) } }
            )
            .connect()
    }

    private fun handlePresenterStatus() {
        val colorId: Int
        val imageId: Int
        val text: String

        when (presenterStatus) {
            PresenterStatus.NOT_READY_TO_ANSWER -> {
                text = resourceManager.getString(R.string.hand_up)
                colorId = R.color.yellow_orange
                imageId = R.drawable.ic_hand_up_yellow
            }
            PresenterStatus.READY_TO_ANSWER -> {
                text = resourceManager.getString(R.string.hand_down)
                colorId = R.color.gray_chateau
                imageId = R.drawable.ic_hand_down
            }
            PresenterStatus.PRESENTER -> {
                text = resourceManager.getString(R.string.you_presenter)
                colorId = R.color.blue
                imageId = R.drawable.ic_hand_up_blue
            }
        }
        viewState.showUserStatus(text, imageId, colorId)
    }

    private fun handleShapeEvent(event: Pair<BoardDataTypes, List<ShapeData>>) {
        when (event.first) {
            BoardDataTypes.JOIN_BOARD,
            BoardDataTypes.ADD_CHANGE -> {
                if (isBoardInitialized) {
                    viewState.drawShapes(event.second)
                } else {
                    boardShapes.addAll(event.second)
                }
            }
            BoardDataTypes.UNDO_LAST_CHANGE -> {
                if (isBoardInitialized) {
                    viewState.undoShape(event.second[0])
                } else {
                    boardShapes.remove(event.second[0])
                }
            }
            BoardDataTypes.REDO_LAST_CHANGE -> {
                if (isBoardInitialized) {
                    viewState.drawShapes(event.second)
                } else {
                    boardShapes.addAll(event.second)
                }
            }
            BoardDataTypes.CLEAN_BOARD -> {
                boardShapes.clear()
                viewState.cleanBoard()
            }
        }
    }

    private fun findTeacher(participants: Set<LessonParticipate>) {
        val teacher = participants.find { it.userData.roles == Roles.TEACHER }
        if (teacher == null) {
            viewState.showBoard(false)
        }
    }
}