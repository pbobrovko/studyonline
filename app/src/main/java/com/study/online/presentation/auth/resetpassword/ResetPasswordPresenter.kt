package com.study.online.presentation.auth.resetpassword

import com.study.online.R
import com.study.online.core.featureflow.AuthScreens
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.ResourceManager
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.core.presentation.FlowRouter
import com.study.online.domain.entity.ResetPasswordRequest
import com.study.online.domain.exceptions.ServerException
import com.study.online.domain.interactor.AuthInteractor
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ResetPasswordPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val router: Router,
    private val authInteractor: AuthInteractor,
    private val errorHandler: ErrorHandler,
    private val resourceManager: ResourceManager
) : BasePresenter<ResetPasswordView>() {

    fun onSendRequestClicked(user: String) {
        viewState.showUserNotRegisteredMessage(false)
        if (user.isEmpty()) {
            viewState.showMessage(resourceManager.getString(R.string.obligatory_field))
            return
        }
        authInteractor
            .resetPassword(ResetPasswordRequest(user))
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                {
                    flowRouter.navigateTo(
                        AuthScreens.AuthActionConfirmedScreen(it.email, user, false)
                    )
                },
                {
                    if (it is ServerException && it.code == USER_NOT_FOUND) {
                        viewState.showMessage(resourceManager.getString(R.string.incorrect_login))
                    } else if (it is ServerException && it.code == USER_NOT_REGISTERED) {
                        viewState.showUserNotRegisteredMessage(true)
                    } else {
                        errorHandler.handleError(it) { message -> viewState.showMessage(message) }
                    }
                }
            )
            .connect()
    }

    fun onToSignUpClicked() {
        flowRouter.newRootChain(AuthScreens.OnboardingScreen, AuthScreens.SignUpScreen)
    }

    fun onSupportClicked() {
        router.navigateTo(Screens.SupportFlow)
    }

    fun onBackPressed() {
        flowRouter.exit()
    }

    companion object {
        private const val USER_NOT_FOUND = 404
        private const val USER_NOT_REGISTERED = 412
    }
}