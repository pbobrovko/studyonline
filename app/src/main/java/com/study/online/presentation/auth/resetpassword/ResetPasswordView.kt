package com.study.online.presentation.auth.resetpassword

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

interface ResetPasswordView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showUserNotRegisteredMessage(isShow: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showProgress(isShow: Boolean)
}