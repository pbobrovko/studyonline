package com.study.online.presentation.main.lesson

import android.content.Context
import com.study.online.core.global.FileLogger
import com.study.online.domain.entity.TimetableLesson
import com.study.online.domain.entity.socket.Roles
import com.study.online.domain.entity.socket.lesson.HandUpResponse
import com.study.online.domain.entity.socket.lesson.LessonParticipate
import com.study.online.domain.entity.socket.lesson.PresenterStatus
import com.study.online.domain.interactor.LessonInteractor
import com.study.online.domain.interactor.ProfileInteractor
import fi.vtt.nubomedia.webrtcpeerandroid.NBMMediaConfiguration
import fi.vtt.nubomedia.webrtcpeerandroid.NBMPeerConnection
import fi.vtt.nubomedia.webrtcpeerandroid.NBMWebRTCPeer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.webrtc.*
import java.util.*
import javax.inject.Inject

// TODO: нодо отрефакторить этот класс и вынести всю логику работы с WebRtc в дата слой
class PeerConnectionDelegate @Inject constructor(
    private val context: Context,
    private val lessonInteractor: LessonInteractor,
    profileInteractor: ProfileInteractor,
    private val timetableLesson: TimetableLesson,
    private val logger: FileLogger
) {

    private val participants = mutableListOf<LessonParticipate>()
    private val user = profileInteractor.user!!
    private val compositeDisposable = CompositeDisposable()
    private val mediaConfiguration = NBMMediaConfiguration()
    private val webRtcDelegate = WebRtcDelegate()
    private val userFrameListener = OnFrameListener()
    private val teacherFrameListener = OnFrameListener()
    private val frameListeners = mutableListOf<Pair<OnFrameListener, String?>>(
        Pair(OnFrameListener(), null),
        Pair(OnFrameListener(), null),
        Pair(OnFrameListener(), null),
        Pair(OnFrameListener(), null),
        Pair(OnFrameListener(), null)
    )
    private val awaitPresenters = mutableSetOf<String>()
    private var connectionListener: PeerConnectionListener? = null
    private var isVideoEnabled = true
    private var isAudioEnabled = true
    private var isUserInitialized = false
    private var teacher: LessonParticipate? = null
    private var nbmWebRTCPeer: NBMWebRTCPeer? = null
    private val iceServers =
        LinkedList<PeerConnection.IceServer>()

    init {
        iceServers.add(PeerConnection.IceServer(STUN_ONE))
        iceServers.add(PeerConnection.IceServer(STUN_TWO))

        lessonInteractor
            .observeIceCandidates()
            .subscribe(
                {
                    logger.addLog("PeerConnectionDelegate iceCandidateResponse: $it")
                    val candidate = IceCandidate(
                        it.candidate.sdpMid,
                        it.candidate.sdpMLineIndex,
                        it.candidate.candidate
                    )

                    nbmWebRTCPeer?.addRemoteIceCandidate(candidate, it.userName)
                },
                {
                    logger.addLog("PeerConnectionDelegate iceCandidateResponse error: $it")
                }
            )
            .connect()

        lessonInteractor
            .observeSdpAnswer()
            .subscribe(
                {
                    logger.addLog("PeerConnectionDelegate SDPAnswerResponse: $it")
                    nbmWebRTCPeer?.processAnswer(
                        SessionDescription(SessionDescription.Type.ANSWER, it.sdpAnswer),
                        it.username
                    )
                },
                { logger.addLog("PeerConnectionDelegate SDPAnswerResponse error: $it") }
            )
            .connect()
    }

    fun setPeerConnectionListener(listener: PeerConnectionListener) {
        connectionListener = listener
    }

    fun initLocalMedia() {
        logger.addLog("PeerConnectionDelegate init local media")

        nbmWebRTCPeer =
            NBMWebRTCPeer(mediaConfiguration, context, userFrameListener.renderer, webRtcDelegate)
        nbmWebRTCPeer?.iceServers = iceServers
        nbmWebRTCPeer?.initialize()
    }

    fun enableVideo(isEnable: Boolean) {
        isVideoEnabled = isEnable
        nbmWebRTCPeer?.enableVideo(isVideoEnabled)
    }

    fun enableAudio(isEnable: Boolean) {
        isAudioEnabled = isEnable
        nbmWebRTCPeer?.enableAudio(isAudioEnabled)
    }

    fun onParticipantsChange(participants: Set<LessonParticipate>) {
        logger.addLog("PeerConnectionDelegate onParticipantsChange: $participants")

        if (!isUserInitialized) {
            nbmWebRTCPeer?.generateOffer(user.userName, true)
        }

        if (participants.size < this.participants.size) {
            val leavedParticipant = this.participants.find { !participants.contains(it) }
            val frameListener = frameListeners.find { leavedParticipant?.userName == it.second }

            if (frameListener != null) {
                frameListener.first.setTarget(null)
                val position = frameListeners.indexOf(frameListener)
                val newPresenter = frameListener.copy(frameListener.first, null)
                frameListeners[position] = newPresenter
                handlePresentersList()
            }
        }

        this.participants.clear()
        this.participants.addAll(participants)

        handleTeacherStatus(participants)

        participants.forEach { participate ->
            if (
                participate.userData.roles != Roles.TEACHER &&
                participate.userData.presenterStatus == PresenterStatus.PRESENTER
            ) {
                val frameListener = frameListeners.find { it.second == participate.userName }

                if (frameListener == null) {
                    addToFrameListener(participate.userName)
                }
            }
        }
    }

    fun onHandUpEvent(handUp: HandUpResponse) {
        logger.addLog("PeerConnectionDelegate onHandUpEvent: $handUp")

        if (handUp.userName != user.userName && handUp.userName != teacher?.userName) {
            when (handUp.presenterStatus) {
                PresenterStatus.PRESENTER -> {
                    addToFrameListener(handUp.userName)
                }
                PresenterStatus.NOT_READY_TO_ANSWER -> {
                    removeFromFrameListener(handUp.userName)
                }
            }
        }
        handlePresentersList()
    }

    fun onAttachView() {
        logger.addLog("PeerConnectionDelegate onAttachView")
        connectionListener?.setTeacherFrameListener(teacherFrameListener)
        connectionListener?.setUserFrameListener(userFrameListener)
        handlePresentersList()
    }

    fun onDestroyView() {
        logger.addLog("PeerConnectionDelegate onDestroyView")
        userFrameListener.release()
        teacherFrameListener.release()
        frameListeners.forEach { it.first.release() }
    }

    fun onDestroy() {
        logger.addLog("PeerConnectionDelegate onDestroy")
        compositeDisposable.dispose()

        if (nbmWebRTCPeer?.isInitialized == true) {
            nbmWebRTCPeer?.close()
            nbmWebRTCPeer?.stopLocalMedia()
            nbmWebRTCPeer = null
            isUserInitialized = false
        }
    }

    private fun handleTeacherStatus(participants: Set<LessonParticipate>) {
        logger.addLog(
            "PeerConnectionDelegate handleTeacherStatus. " +
                    "Teacher: $teacher, participants: $participants"
        )
        if (teacher == null) {
            logger.addLog("PeerConnectionDelegate handleTeacherStatus teacher add")
            teacher = participants.find { it.userData.roles == Roles.TEACHER }
            teacher?.let {
                nbmWebRTCPeer?.generateOffer(it.userName, false)
                connectionListener?.showTeacherName(teacher)
                connectionListener?.setTeacherFrameListener(teacherFrameListener)
            }
        } else {
            val isTeacherOnRoom = participants.find { it.userName == teacher?.userName } != null
            if (!isTeacherOnRoom) {
                logger.addLog("PeerConnectionDelegate handleTeacherStatus teacher leaved")
                nbmWebRTCPeer?.closeConnection(teacher?.userName)
                teacherFrameListener.setTarget(null)
                connectionListener?.showTeacherName(null)
                teacher = null
            }
        }
    }

    private fun addToFrameListener(userName: String) {
        logger.addLog("PeerConnectionDelegate addToFrameListener userName: $userName")
        val emptyListener = frameListeners.find { it.second == null }
        if (emptyListener == null) {
            logger.addLog("PeerConnectionDelegate addToFrameListener add to await list")
            awaitPresenters.add(userName)
        } else {
            logger.addLog("PeerConnectionDelegate addToFrameListener add to frame list")
            nbmWebRTCPeer?.generateOffer(userName, false)
            val newPresenter = emptyListener.copy(emptyListener.first, userName)
            val position = frameListeners.indexOf(emptyListener)
            frameListeners[position] = newPresenter
        }
        handlePresentersList()
    }

    private fun removeFromFrameListener(userName: String) {
        logger.addLog(
            "PeerConnectionDelegate removeFromFrameListener userName: $userName, " +
                    "frameListeners: $frameListeners"
        )
        val oldPresenter = frameListeners.find { it.second == userName }

        oldPresenter?.let {
            nbmWebRTCPeer?.closeConnection(userName)

            val newPresenter = if (awaitPresenters.isNotEmpty()) {
                val awaitPresenterName = awaitPresenters.elementAt(0)
                awaitPresenters.remove(awaitPresenterName)
                nbmWebRTCPeer?.generateOffer(awaitPresenterName, false)
                it.copy(it.first, awaitPresenterName)
            } else {
                it.copy(it.first, null)
            }

            it.first.setTarget(null)
            val position = frameListeners.indexOf(it)
            frameListeners[position] = newPresenter
        }
    }

    private fun handlePresentersList() {
        logger.addLog(
            "PeerConnectionDelegate handlePresentersList participants: $participants, " +
                    "frameListeners: $frameListeners"
        )
        val availableListeners = frameListeners.filter { it.second != null }

        if (availableListeners.isNotEmpty()) {
            val availableParticipants = participants.filter { participate ->
                availableListeners.find { participate.userName == it.second } != null
            }

            val presenters = availableListeners.map { listener ->
                val participant = availableParticipants.find { listener.second == it.userName }
                Pair(listener.first, participant!!)
            }
            connectionListener?.showPresenters(presenters)
        } else {
            connectionListener?.showPresenters(emptyList())
        }
    }

    private fun sendSDPData(localSdpOffer: SessionDescription, userName: String) {
        lessonInteractor
            .sendSdpData(localSdpOffer, timetableLesson, userName)
            .subscribe(
                { logger.addLog("PeerConnectionDelegate sendSDPData success") },
                { logger.addLog("PeerConnectionDelegate sendSDPData failed: $it") }
            )
            .connect()
    }

    private fun Disposable.connect() {
        compositeDisposable.add(this)
    }

    inner class WebRtcDelegate : NBMWebRTCPeer.Observer {

        override fun onInitialize() {
            logger.addLog("PeerConnectionDelegate onInitialize")
        }

        override fun onLocalSdpOfferGenerated(
            localSdpOffer: SessionDescription,
            connection: NBMPeerConnection
        ) {
            logger.addLog(
                "PeerConnectionDelegate onLocalSdpOfferGenerated ${connection.connectionId}"
            )
            sendSDPData(localSdpOffer, connection.connectionId)
            isUserInitialized = connection.connectionId == user.userName
        }

        override fun onLocalSdpAnswerGenerated(
            localSdpAnswer: SessionDescription?,
            connection: NBMPeerConnection?
        ) {
        }

        override fun onIceCandidate(
            localIceCandidate: IceCandidate,
            connection: NBMPeerConnection
        ) {
            logger.addLog("PeerConnectionDelegate onIceCandidate ${connection.connectionId}")
            lessonInteractor
                .sendIceCandidate(localIceCandidate, timetableLesson, connection.connectionId)
                .subscribe({}, {})
                .connect()
        }

        override fun onIceStatusChanged(
            state: PeerConnection.IceConnectionState,
            connection: NBMPeerConnection
        ) {
            logger.addLog(
                "PeerConnectionDelegate onIceStatusChanged ${connection.connectionId}, " +
                        "status ${state.name}"
            )
        }

        override fun onRemoteStreamAdded(stream: MediaStream, connection: NBMPeerConnection) {
            logger.addLog(
                "PeerConnectionDelegate onRemoteStreamAdded ${connection.connectionId}"
            )
            if (connection.connectionId == teacher?.userName) {
                nbmWebRTCPeer?.attachRendererToRemoteStream(teacherFrameListener.renderer, stream)
            } else if (connection.connectionId != user.userName) {
                val frameListener = frameListeners.find { it.second == connection.connectionId }
                frameListener?.let {
                    nbmWebRTCPeer?.attachRendererToRemoteStream(it.first.renderer, stream)
                }
            }
        }

        override fun onRemoteStreamRemoved(stream: MediaStream, connection: NBMPeerConnection) {}

        override fun onPeerConnectionError(error: String?) {}

        override fun onDataChannel(dataChannel: DataChannel, connection: NBMPeerConnection) {}

        override fun onBufferedAmountChange(
            l: Long,
            connection: NBMPeerConnection?,
            channel: DataChannel?
        ) {
        }

        override fun onStateChange(connection: NBMPeerConnection, channel: DataChannel) {}

        override fun onMessage(
            buffer: DataChannel.Buffer?,
            connection: NBMPeerConnection?,
            channel: DataChannel?
        ) {
        }
    }

    companion object {
        private const val STUN_ONE = "stun:stun.l.google.com:19302"
        private const val STUN_TWO = "stun:stun1.l.google.com:19302"
    }
}