@file:JvmName("ActivityUtils")

package com.study.online.extension

import android.app.Activity
import android.content.Context
import android.os.PowerManager

fun Activity.turnScreenOn() {
    val wakeLock =
        (this.getSystemService(Context.POWER_SERVICE) as PowerManager)
            .newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK or
                        PowerManager.FULL_WAKE_LOCK or
                        PowerManager.ACQUIRE_CAUSES_WAKEUP,
                "MyApp::Tag"
            )
    wakeLock.acquire()
    wakeLock.release()
}

fun Activity.turnScreenOff() {
    val wakeLock =
        (this.getSystemService(Context.POWER_SERVICE) as PowerManager)
            .newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK or
                        PowerManager.FULL_WAKE_LOCK or
                        PowerManager.ACQUIRE_CAUSES_WAKEUP,
                "MyApp::Tag"
            )
    wakeLock.acquire(0L)
    wakeLock.release()
}