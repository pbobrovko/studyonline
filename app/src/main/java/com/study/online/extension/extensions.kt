package com.study.online.extension

import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace
import java.util.*

fun Navigator.setLaunchScreen(screen: SupportAppScreen) {

    applyCommands(
        arrayOf(
            BackTo(null),
            Replace(screen)
        )
    )
}

const val LANGUAGE_RUSSIAN = "ru"
const val LANGUAGE_KAZAKHSTAN = "kk"
val LOCALE_RUSSIAN = Locale(LANGUAGE_RUSSIAN)
val LOCALE_KAZAKHSTAN = Locale(LANGUAGE_KAZAKHSTAN)