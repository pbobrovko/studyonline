package com.study.online.extension

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.study.online.domain.entity.socket.lesson.LessonDataTypes
import com.study.online.domain.entity.socket.lesson.GeneralLessonDTO
import com.study.online.domain.entity.socket.lesson.*
import org.json.JSONObject
import java.lang.Exception
import java.lang.reflect.Type

// TODO: refactor to type adapter (use: https://futurestud.io/tutorials/gson-advanced-custom-serialization-part-1)
fun Gson.fromJsonToGeneralLessonDTO(
    json: String
): GeneralLessonDTO<LessonResponse> {
    val jsonObject = JSONObject(json)
    val id = this.fromJson(jsonObject.getString("id"), LessonDataTypes::class.java)
    val type = getResponseType(id)

    return this.fromJson(json, type)
}

private fun getResponseType(id: LessonDataTypes): Type {
    return when (id) {
        LessonDataTypes.NEW_CHAT_MESSAGE ->
            object : TypeToken<GeneralLessonDTO<ChatMessage>>() {}.type
        LessonDataTypes.EXISTING_CHAT_MESSAGES ->
            object : TypeToken<GeneralLessonDTO<List<ChatMessage>>>() {}.type
        LessonDataTypes.PARTICIPANT_LEFT,
        LessonDataTypes.NEW_PARTICIPANT_ARRIVED ->
            object : TypeToken<GeneralLessonDTO<LessonParticipate>>() {}.type
        LessonDataTypes.EXISTING_PARTICIPANTS ->
            object : TypeToken<GeneralLessonDTO<List<LessonParticipate>>>() {}.type
        LessonDataTypes.RECEIVE_VIDEO_ANSWER ->
            object : TypeToken<GeneralLessonDTO<ReceiveSDPAnswer>>() {}.type
        LessonDataTypes.ICE_CANDIDATE ->
            object : TypeToken<GeneralLessonDTO<IceCandidateResponse>>() {}.type
        LessonDataTypes.HAND_UP -> object : TypeToken<GeneralLessonDTO<HandUpResponse>>() {}.type
        LessonDataTypes.ERROR,
        LessonDataTypes.FINISH_LESSON -> object : TypeToken<GeneralLessonDTO<*>>() {}.type
        else -> {
            throw Exception()
        }
    }
}