package com.study.online.extension

import android.app.Activity
import android.os.Parcelable
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import java.io.Serializable

fun Fragment.tryToGetString(
    key: String,
    exceptionMessage: String = composeErrorMessage(key)
): String {
    return this.arguments?.getString(key) ?: throwArgumentException(exceptionMessage)
}

fun Fragment.tryToGetInt(
    key: String,
    exceptionMessage: String = composeErrorMessage(key)
): Int {
    return this.arguments?.getInt(key) ?: throwArgumentException(exceptionMessage)
}

fun Fragment.tryToGetBoolean(
    key: String,
    exceptionMessage: String = composeErrorMessage(key)
): Boolean {
    return this.arguments?.getBoolean(key) ?: throwArgumentException(exceptionMessage)
}

fun <T : Parcelable> Fragment.tryToGetParcelable(
    key: String,
    exceptionMessage: String = composeErrorMessage(key)
): T {
    return this.arguments?.getParcelable(key) ?: throwArgumentException(exceptionMessage)
}

fun <T : Parcelable> Fragment.tryToGetArrayList(
    key: String,
    exceptionMessage: String = composeErrorMessage(key)
): ArrayList<T> {
    return this.arguments?.getParcelableArrayList(key) ?: throwArgumentException(exceptionMessage)
}

fun <T : Serializable> Fragment.tryToGetSerializable(
    key: String,
    exceptionMessage: String = composeErrorMessage(key)
): T {
    return (this.arguments?.getSerializable(key) as? T) ?: throwArgumentException(exceptionMessage)
}

fun Fragment.getColor(@ColorRes id: Int) = ContextCompat.getColor(this.requireContext(), id)

fun Fragment.hideKeyboard() {
    val imm: InputMethodManager =
        requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view: View? = requireActivity().currentFocus
    if (view == null) {
        view = requireView()
    }
    view?.let {
        imm.hideSoftInputFromWindow(it.windowToken, 0)
    }
}

private fun <T> throwArgumentException(message: String): T = throw IllegalArgumentException(message)

private fun composeErrorMessage(key: String) = "Argument $key hasn't been provided"