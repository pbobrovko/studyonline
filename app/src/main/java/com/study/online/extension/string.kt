package com.study.online.extension

fun String.Companion.getName(firstName: String, lastName: String, patronymic: String): String {
    val firstNameCharacter = if (firstName.isNullOrEmpty()) {
        ""
    } else {
        firstName.substring(
            NAME_CHARS_START_POSITION,
            NAME_CHARS_END_POSITION
        )
    }
    val patronymicCharacter = if (patronymic.isNullOrEmpty()) {
        ""
    } else {
        patronymic.substring(
            NAME_CHARS_START_POSITION,
            NAME_CHARS_END_POSITION
        )
    }

    return String.format(
        PRESENTER_NAME_PATTERN,
        lastName,
        firstNameCharacter,
        patronymicCharacter
    )
}

private const val PRESENTER_NAME_PATTERN = "%s %s.%s"

private const val NAME_CHARS_START_POSITION = 0
private const val NAME_CHARS_END_POSITION = 1