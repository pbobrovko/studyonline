package com.study.online.data.server

import com.study.online.domain.entity.*
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {

    @POST("api/v1/accounts/send_register_link")
    fun registerEmail(@Body email: Email): Single<Email>

    @POST("api/v1/accounts/check_reset_passw_key")
    fun checkResetPassword(@Body emailCode: EmailCode): Single<CreatePasswordInfo>

    @POST("api/v1/accounts/reset_passw")
    fun createPassword(@Body createPassword: CreatePassword): Single<AuthData>

    @POST("api/v1/auth")
    fun login(@Body loginRequest: LoginRequest): Single<AuthData>

    @POST("api/v1/accounts/send_reset_passw_link")
    fun resetPassword(@Body resetPassword: ResetPasswordRequest): Single<Email>

    @POST("api/v1/auth/refresh-tokens")
    fun refreshToken(@Body refreshTokensRequest: RefreshTokensRequest): Single<AuthData>

    @POST("api/v1/auth/logout")
    fun logout(@Body logoutRequest: LogoutRequest): Completable
}