package com.study.online.data.storage

import android.content.Context
import com.google.gson.Gson
import com.study.online.domain.entity.School
import com.study.online.domain.entity.User
import com.study.online.extension.LANGUAGE_RUSSIAN
import java.util.*
import javax.inject.Inject

class Prefs @Inject constructor(
    private val context: Context,
    private val gson: Gson
) {

    private fun getSharedPreferences(prefsName: String) =
        context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

    private val appPrefs by lazy { getSharedPreferences(APP_DATA) }

    var authToken: String
        get() = appPrefs.getString(KEY_AUTH_TOKEN, "") ?: ""
        set(value) {
            appPrefs.edit().putString(KEY_AUTH_TOKEN, value).apply()
        }

    var currentSchool: School?
        get() {
            val value = appPrefs.getString(KEY_CURRENT_SCHOOL, null)
            return if (value != null) {
                gson.fromJson(value, School::class.java)
            } else {
                null
            }
        }
        set(value) {
            appPrefs.edit().putString(KEY_CURRENT_SCHOOL, gson.toJson(value)).apply()
        }

    var currentLocale: Locale
        get() {
            val language = appPrefs.getString(CURRENT_LOCALE, LANGUAGE_RUSSIAN) ?: LANGUAGE_RUSSIAN
            return Locale(language)
        }
        set(value) {
            appPrefs.edit().putString(CURRENT_LOCALE, value?.language).apply()
        }

    var refreshToken: String
        get() = appPrefs.getString(KEY_REFRESH_TOKEN, "") ?: ""
        set(value) = appPrefs.edit().putString(KEY_REFRESH_TOKEN, value).apply()

    // This variable can be call only from ProfileInteractor!!
    var user: User?
        get() {
            val value = appPrefs.getString(KEY_USER, null)
            return if (value != null) {
                gson.fromJson(value, User::class.java)
            } else {
                null
            }
        }
        set(value) {
            appPrefs.edit().putString(KEY_USER, gson.toJson(value)).apply()
        }

    companion object {
        private const val APP_DATA = "APP_DATA"
        private const val KEY_AUTH_TOKEN = "KEY_AUTH_TOKEN"
        private const val KEY_CURRENT_SCHOOL = "KEY_CURRENT_SCHOOL"
        private const val KEY_USER = "KEY_USER"
        private const val KEY_REFRESH_TOKEN = "KEY_REFRESH_TOKEN"
        private const val CURRENT_LOCALE = "CURRENT_LOCALE"
    }
}