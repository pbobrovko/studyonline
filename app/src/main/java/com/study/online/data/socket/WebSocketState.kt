package com.study.online.data.socket

enum class WebSocketState {
    CONNECTING,
    CONNECTED,
    DISCONNECTING,
    DISCONNECTED
}