package com.study.online.data

import android.net.Uri
import com.study.online.core.global.FileLogger
import com.study.online.core.global.scheduler.SchedulersProvider
import com.study.online.data.server.LessonApi
import com.study.online.data.server.RoutingApi
import com.study.online.data.socket.LessonSocket
import com.study.online.data.socket.WebSocketState
import com.study.online.domain.entity.TimetableLesson
import com.study.online.domain.entity.User
import com.study.online.domain.entity.socket.*
import com.study.online.domain.entity.socket.lesson.*
import com.study.online.domain.exceptions.LessonOverflowException
import com.study.online.domain.exceptions.InvalidUserDataException
import com.study.online.domain.interactor.ProfileInteractor
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import org.webrtc.IceCandidate
import org.webrtc.SessionDescription
import javax.inject.Inject

class LessonRepository @Inject constructor(
    private val routingApi: RoutingApi,
    private val lessonSocket: LessonSocket,
    private val lessonApi: LessonApi,
    private val defaultMediaState: MediaState,
    schedulers: SchedulersProvider,
    private val logger: FileLogger,
    private val profileInteractor: ProfileInteractor
) {

    private var user: User? = null
        get() = profileInteractor.user

    private val participates = mutableSetOf<LessonParticipate>()
    private val chatMessages = mutableListOf<ChatMessage>()

    val sdpAnswer = PublishSubject.create<ReceiveSDPAnswer>()
    private val _sdpAnswer = sdpAnswer
    private val _existingParticipants =
        BehaviorSubject.createDefault<Set<LessonParticipate>>(participates)
    val existingParticipants: Observable<Set<LessonParticipate>> = _existingParticipants.hide()
    private val _existingChatMessages =
        BehaviorSubject.createDefault<List<ChatMessage>>(chatMessages)
    val exitingChatMessages: Observable<List<ChatMessage>> = _existingChatMessages.hide()
    private val _iceCandidateObservable =
        PublishSubject.create<IceCandidateResponse>()
    val iceCandidateObservable: Observable<IceCandidateResponse> = _iceCandidateObservable.hide()
    private val _handUpObservable = PublishSubject.create<HandUpResponse>()
    val handUpObservable: Observable<HandUpResponse> = _handUpObservable.hide()
    private val _finishLessonObservable = PublishSubject.create<Unit>()
    val finishLessonCompletable: Observable<Unit> = _finishLessonObservable.hide()

    init {
        val disposable = lessonSocket
            .observeSocketMessages()
            .subscribeOn(schedulers.io())
            .subscribe(
                { handleSocketMessage(it) },
                { logger.addLog("Lesson Repository socket response error $it") }
            )
    }

    fun connectToLesson(lessonId: String): Observable<WebSocketState> {
        participates.clear()
        chatMessages.clear()
        _existingParticipants.onNext(participates)
        _existingChatMessages.onNext(chatMessages)
        var nodeUri = Uri.parse("")
        return routingApi
            .getLessonNode(lessonId)
            .flatMap {
                nodeUri = Uri.parse(it.string())
                val url = String.format(URL_LESSON_USER_COUNT, nodeUri.encodedAuthority, lessonId)

                logger.addLog("Lesson Repository node url $nodeUri")
                lessonApi.getLessonUserCount(url)
            }
            .doOnSuccess {
                logger.addLog("Lesson Repository lesson participants count $it")
                if (it >= PUPILS_MAX_COUNT) {
                    throw LessonOverflowException()
                }
            }
            .flatMapObservable {
                logger.addLog("Lesson Repository connect to socket")
                lessonSocket.connect(nodeUri.toString())
            }
            .filter { it == WebSocketState.CONNECTED }
            .take(1)
    }

    fun joinLesson(lessonId: String): Completable {
        val message = user?.let {
            val joinLesson = JoinLesson(
                it.firstName,
                it.lastName,
                it.patronymic,
                it.role,
                PresenterStatus.NOT_READY_TO_ANSWER,
                defaultMediaState
            )
            GeneralLessonDTO(
                LessonDataTypes.JOIN_LESSON,
                lessonId,
                it.userName,
                joinLesson
            )
        }

        logger.addLog(
            "Lesson Repository send join lesson  message: ${message ?: "message is null"}"
        )
        // TODO: Thread changing make in a LessonInteractor
        return Completable.fromAction {
            if (message != null) {
                lessonSocket.sendMessage(message)
            } else {
                throw InvalidUserDataException()
            }
        }
    }

    fun sendSdpData(
        localSdpOffer: SessionDescription,
        timetableLesson: TimetableLesson,
        userName: String
    ): Completable {
        // TODO: Move DTO mapping outside of this class
        val sendSDP =
            SendSDP(userName, localSdpOffer.description, mediaState = defaultMediaState)
        val message = GeneralLessonDTO(
            LessonDataTypes.RECEIVE_VIDEO,
            timetableLesson.id.toString(),
            userName,
            sendSDP
        )

        logger.addLog("Lesson Repository send SDP message: $message")
        return Completable
            .fromAction { lessonSocket.sendMessage(message) }
    }

    fun sendIceCandidate(
        iceCandidate: IceCandidate,
        timetableLesson: TimetableLesson,
        userName: String
    ): Completable {
        val iceCandidateRequest = IceCandidateRequest(
            iceCandidate.sdp,
            iceCandidate.sdpMLineIndex,
            iceCandidate.sdpMid
        )
        val message = GeneralLessonDTO(
            LessonDataTypes.ON_ICE_CANDIDATE,
            timetableLesson.id.toString(),
            userName,
            iceCandidateRequest
        )

        logger.addLog("Lesson Repository send IceCandidate message: $message")

        return Completable.fromAction { lessonSocket.sendMessage(message) }
    }

    fun leaveLesson(): Completable {
        val message = user?.let {
            GeneralLessonDTO<LessonRequest>(
                LessonDataTypes.LEAVE_LESSON,
                null,
                it.userName,
                null
            )
        }

        logger.addLog(
            "Lesson Repository send leave lesson message: ${message ?: "message is null"}"
        )
        return Completable
            .fromAction {
                if (message != null) {
                    lessonSocket.sendMessage(message)
                } else {
                    throw InvalidUserDataException()
                }
            }
            .doOnTerminate {
                lessonSocket.close()
            }
    }

    fun handUp(presenterStatus: PresenterStatus, timetableLesson: TimetableLesson): Completable {
        val message = user?.let {
            val handUpRequest = HandUpRequest(
                it.userName,
                presenterStatus
            )
            GeneralLessonDTO(
                LessonDataTypes.HAND_UP,
                timetableLesson.id.toString(),
                it.userName,
                handUpRequest
            )
        }

        logger.addLog(
            "Lesson Repository send hand up message: ${message ?: "message is null"}"
        )
        return Completable.fromAction {
            if (message != null) {
                lessonSocket.sendMessage(message)
            } else {
                throw InvalidUserDataException()
            }
        }
    }

    // TODO: create ChatRepository for this logic
    fun sendChatMessage(chatMessage: SendMessageRequest, lessonId: String): Completable {
        val message = user?.let {
            GeneralLessonDTO(
                LessonDataTypes.SEND_CHAT_MESSAGE,
                lessonId,
                it.userName,
                chatMessage
            )
        }

        logger.addLog(
            "Lesson Repository send new chat message: ${message ?: "message is null"}"
        )
        return Completable.fromAction {
            if (message != null) {
                lessonSocket.sendMessage(message)
            } else {
                throw InvalidUserDataException()
            }
        }
    }

    private fun handleSocketMessage(response: GeneralLessonDTO<LessonResponse>) {
        try {
            when (response.dataType) {
                LessonDataTypes.EXISTING_PARTICIPANTS -> {
                    participates.addAll(response.data as List<LessonParticipate>)
                    _existingParticipants.onNext(participates)
                }
                LessonDataTypes.NEW_PARTICIPANT_ARRIVED -> {
                    participates.add(response.data as LessonParticipate)
                    _existingParticipants.onNext(participates)
                }
                LessonDataTypes.PARTICIPANT_LEFT -> {
                    val participate = response.data as LessonParticipate
                    participates.remove(participates.find { it.userName == participate.userName })
                    _existingParticipants.onNext(participates)
                }
                LessonDataTypes.EXISTING_CHAT_MESSAGES -> {
                    chatMessages.addAll(response.data as List<ChatMessage>)
                    _existingChatMessages.onNext(chatMessages)
                }
                LessonDataTypes.NEW_CHAT_MESSAGE -> {
                    chatMessages.add(response.data as ChatMessage)
                    _existingChatMessages.onNext(chatMessages)
                }
                LessonDataTypes.RECEIVE_VIDEO_ANSWER -> {
                    _sdpAnswer.onNext(response.data as ReceiveSDPAnswer)
                }
                LessonDataTypes.ICE_CANDIDATE -> {
                    _iceCandidateObservable.onNext(response.data as IceCandidateResponse)
                }
                LessonDataTypes.HAND_UP -> {
                    _handUpObservable.onNext(response.data as HandUpResponse)
                }
                LessonDataTypes.FINISH_LESSON -> {
                    _finishLessonObservable.onNext(Unit)
                }
                else -> {
                    logger.addLog("Lesson Repository unknown message: ${response.data}")
                }
            }
        } catch (e: ClassCastException) {
            logger.addLog("Lesson Repository response cast failed: $response")
        }
    }

    companion object {
        private const val URL_LESSON_USER_COUNT = "https://%s/api/lesson/%s/user-count"

        private const val PUPILS_MAX_COUNT = 55
    }
}