package com.study.online.data.socket

import com.google.gson.Gson
import com.study.online.core.global.FileLogger
import com.study.online.data.storage.Prefs
import com.study.online.domain.entity.socket.lessonboard.BoardRequest
import com.study.online.domain.entity.socket.lessonboard.GeneralBoardDTO
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import okhttp3.*
import okio.ByteString
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BoardSocket @Inject constructor(
    private val prefs: Prefs,
    private val gson: Gson,
    private val logger: FileLogger
) : WebSocketListener() {

    private var webSocket: WebSocket? = null

    private val webSocketStateRelay = BehaviorRelay.createDefault(WebSocketState.DISCONNECTED)
    private val webSocketMessage = PublishSubject.create<GeneralBoardDTO<*>>()

    override fun onOpen(webSocket: WebSocket, response: Response) {
        super.onOpen(webSocket, response)

        logger.addLog("Board socket connected")
        webSocketStateRelay.accept(WebSocketState.CONNECTED)
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        super.onMessage(webSocket, text)

        logger.addLog("Board socket message: $text")
        try {
            val message = gson.fromJson(text, GeneralBoardDTO::class.java)
            webSocketMessage.onNext(message)
        } catch (e: Exception) {
            logger.addLog("Board socket deserialization failed: $e")
        }
    }

    override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
        super.onMessage(webSocket, bytes)
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        super.onFailure(webSocket, t, response)

        logger.addLog(
            "Board socket failure. Throwable: ${t.message ?: "null"} \n Response: ${response?.message}"
        )
    }

    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
        super.onClosed(webSocket, code, reason)

        logger.addLog("Board socket closed")
        webSocketStateRelay.accept(WebSocketState.DISCONNECTED)
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
        super.onClosing(webSocket, code, reason)

        logger.addLog("Board socket closing")
        webSocketStateRelay.accept(WebSocketState.DISCONNECTING)
    }

    fun connect(nodeUrl: String): Observable<WebSocketState> {
        val client = OkHttpClient.Builder()
            .readTimeout(1, TimeUnit.MINUTES)
            .build()

        val url = String.format(SOCKET_CONNECTION_STRING, nodeUrl, prefs.authToken)
        val request = Request.Builder()
            .url(url)
            .build()

        logger.addLog("Board socket connect, url: $url")

        webSocket = client.newWebSocket(request, this)
        webSocketStateRelay.accept(WebSocketState.CONNECTING)

        return webSocketStateRelay.hide()
    }

    fun sendMessage(message: GeneralBoardDTO<out BoardRequest>) {
        logger.addLog("Board socket send message: $message")

        val text = gson.toJson(message)
        webSocket?.send(text)
    }

    fun observeSocketMessages(): Observable<GeneralBoardDTO<*>> =
        webSocketMessage.hide()

    fun close() {
        logger.addLog("Board socket close, code: $CODE_NORMAL_CLOSING")
        webSocket?.close(CODE_NORMAL_CLOSING, "")
        webSocket = null
    }

    companion object {
        private const val SOCKET_CONNECTION_STRING = "%s?token= %s"

        private const val CODE_NORMAL_CLOSING = 1000
    }
}