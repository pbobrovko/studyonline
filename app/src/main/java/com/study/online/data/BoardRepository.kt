package com.study.online.data

import com.study.online.core.global.FileLogger
import com.study.online.core.global.scheduler.SchedulersProvider
import com.study.online.data.server.RoutingApi
import com.study.online.data.socket.BoardSocket
import com.study.online.data.socket.WebSocketState
import com.study.online.domain.entity.socket.lessonboard.*
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class BoardRepository @Inject constructor(
    private val boardSocket: BoardSocket,
    private val logger: FileLogger,
    private val routingApi: RoutingApi,
    schedulers: SchedulersProvider
) {

    private val shapesData = mutableListOf<ShapeData>()
    private val shapesDataUndo = mutableListOf<ShapeData>()

    private val _boardState = PublishSubject.create<Boolean>()
    val boardState = _boardState.hide()
    private val _shapeData = BehaviorSubject.create<Pair<BoardDataTypes, List<ShapeData>>>()
    val shapeData = _shapeData.hide()
    private val _accessType = BehaviorSubject.create<AccessTypes>()
    val accessType = _accessType.hide()
    private val _accessState = BehaviorSubject.create<Boolean>()
    val accessState = _accessState.hide()
    private val _boardSize = BehaviorSubject.create<BoardSize>()
    val boardSize = _boardSize.hide()

    private var disposable: Disposable? = null

    init {
        disposable = boardSocket
            .observeSocketMessages()
            .subscribeOn(schedulers.io())
            .subscribe(
                { handleSocketMessage(it) },
                { logger.addLog("Board Repository socket response error $it") }
            )
    }

    fun connectToBoard(lessonId: String): Observable<WebSocketState> {
        _shapeData.onNext(Pair(BoardDataTypes.JOIN_BOARD, emptyList()))
        _boardSize.onNext(BoardSize(0, 0))
        shapesData.clear()
        shapesDataUndo.clear()
        return routingApi
            .getBoardNode(lessonId)
            .flatMapObservable {
                logger.addLog("Board Repository connect to socket")
                boardSocket.connect(it.string())
            }
            .filter { it == WebSocketState.CONNECTED }
            .take(1)
    }

    fun joinBoard(lessonId: String): Completable =
        Completable.fromAction {
            val message = GeneralBoardDTO(BoardDataTypes.JOIN_BOARD, BoardId(lessonId))
            boardSocket.sendMessage(message)
            logger.addLog("Board Repository joinBoard")
        }

    fun leaveBoard(lessonId: String): Completable =
        Completable.fromAction {
            boardSocket.close()
            logger.addLog("Board Repository leaveBoard")
        }

    protected fun finalize() {
        disposable?.dispose()
    }

    private fun handleSocketMessage(response: GeneralBoardDTO<*>) {
        try {
            when (response.id) {
                BoardDataTypes.JOIN_BOARD -> {
                    shapesData.clear()
                    shapesData.addAll(response.data as List<ShapeData>)
                    _shapeData.onNext(Pair(response.id, shapesData))
                    logger.addLog(
                        "Board Repository handleSocketMessage JOIN_BOARD ${shapesData.size}"
                    )
                }
                BoardDataTypes.ADD_CHANGE -> {
                    val shapeData = response.data as ShapeData
                    shapesData.add(shapeData)
                    _shapeData.onNext(Pair(response.id, listOf(shapeData)))

                    val undoShape = shapesDataUndo.find {
                        it.changeId.userName == shapeData.changeId.userName
                    }
                    shapesDataUndo.remove(undoShape)

                    logger.addLog("Board Repository handleSocketMessage ADD_CHANGE $shapeData")
                }
                BoardDataTypes.BOARD_STATE -> {
                    val boardState = response.data as BoardState
                    _boardState.onNext(boardState.actual)
                    if (boardState.boardSize != null) {
                        _boardSize.onNext(boardState.boardSize)
                    }
                    logger.addLog("Board Repository handleSocketMessage BOARD_STATE $boardState")
                }
                BoardDataTypes.OPEN_BOARD -> {
                    _boardState.onNext(true)
                    if (response.data != null) {
                        _boardSize.onNext(response.data as BoardSize)
                    }
                    logger.addLog(
                        "Board Repository handleSocketMessage OPEN_BOARD ${response.data}"
                    )
                }
                BoardDataTypes.CLOSE_BOARD -> {
                    _boardState.onNext(false)
                    logger.addLog(
                        "Board Repository handleSocketMessage CLOSE_BOARD ${response.data}"
                    )
                }
                BoardDataTypes.ACCESS_TYPE -> {
                    _accessType.onNext(response.data as AccessTypes)
                    logger.addLog(
                        "Board Repository handleSocketMessage ACCESS_TYPE ${response.data}"
                    )
                }
                BoardDataTypes.ACCESS_STATE -> {
                    _accessState.onNext(response.data as Boolean)
                    logger.addLog(
                        "Board Repository handleSocketMessage ACCESS_STATE ${response.data}"
                    )
                }
                BoardDataTypes.UNDO_LAST_CHANGE -> {
                    val changeId = response.data as ChangeId
                    val undoElement = shapesData.find {
                        it.changeId.userName == changeId.userName &&
                                it.changeId.changeNumber == changeId.changeNumber
                    }
                    undoElement?.let {
                        shapesDataUndo.add(it)
                        shapesData.remove(it)
                        _shapeData.onNext(Pair(response.id, listOf(it)))
                    }
                    logger.addLog(
                        "Board Repository handleSocketMessage UNDO_LAST_CHANGE $undoElement"
                    )
                }
                BoardDataTypes.REDO_LAST_CHANGE -> {
                    val changeId = response.data as ChangeId
                    val redoElement = shapesDataUndo.find {
                        it.changeId.userName == changeId.userName &&
                                it.changeId.changeNumber == changeId.changeNumber
                    }
                    redoElement?.let {
                        shapesData.add(it)
                        shapesDataUndo.remove(it)
                        _shapeData.onNext(Pair(response.id, listOf(it)))
                    }
                    logger.addLog(
                        "Board Repository handleSocketMessage REDO_LAST_CHANGE $redoElement"
                    )
                }
                BoardDataTypes.CLEAN_BOARD -> {
                    shapesData.clear()
                    shapesDataUndo.clear()
                    _shapeData.onNext(Pair(response.id, emptyList()))
                }
                else -> {
                    logger.addLog("Lesson Repository unknown message: ${response.data}")
                }
            }
        } catch (e: ClassCastException) {
            logger.addLog("Lesson Repository response cast failed: $response")
        }
    }
}