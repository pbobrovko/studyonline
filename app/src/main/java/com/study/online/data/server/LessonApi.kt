package com.study.online.data.server

import com.study.online.domain.entity.*
import io.reactivex.Single
import retrofit2.http.*

interface LessonApi {

    @GET("api/v1/profile")
    fun getUserProfile(): Single<User>

    @GET("api/v1/schools/{schoolId}/timetables")
    fun getLessons(@Path("schoolId") schoolId: Int): Single<TimetableData>

    @GET
    fun getLessonUserCount(@Url url: String): Single<Int>

    @POST("api/v1/accounts/create_guest")
    fun registerGuest(@Body registerGuestRequest: RegisterGuestRequest): Single<AccessToken>

    @GET("api/v1/lessons/{id}")
    fun getLessonById(@Path("id") lessonId: String): Single<Lesson>
}