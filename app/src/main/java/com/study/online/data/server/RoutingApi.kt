package com.study.online.data.server

import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path

interface RoutingApi {

    @GET("api/streaming/lesson/{lesson}/ip")
    fun getLessonNode(@Path("lesson") lessonId: String): Single<ResponseBody>

    @GET("api/board/lesson/{lesson}/ip")
    fun getBoardNode(@Path("lesson") lessonId: String): Single<ResponseBody>
}