package com.study.online.domain.interactor

import com.study.online.core.global.FileLogger
import com.study.online.core.global.scheduler.SchedulersProvider
import com.study.online.data.LessonRepository
import com.study.online.data.server.LessonApi
import com.study.online.data.socket.WebSocketState
import com.study.online.data.storage.Prefs
import com.study.online.domain.entity.*
import com.study.online.domain.entity.socket.Roles
import com.study.online.domain.entity.socket.lesson.*
import com.study.online.domain.exceptions.InvalidTokenException
import com.study.online.domain.exceptions.LessonEndedException
import com.study.online.domain.exceptions.LessonOverflowException
import com.study.online.domain.exceptions.ServerException
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.webrtc.IceCandidate
import org.webrtc.SessionDescription
import retrofit2.HttpException
import javax.inject.Inject

class LessonInteractor @Inject constructor(
    private val lessonApi: LessonApi,
    private val schedulers: SchedulersProvider,
    private val lessonRepository: LessonRepository,
    private val prefs: Prefs,
    private val profileInteractor: ProfileInteractor,
    private val authInteractor: AuthInteractor,
    private val logger: FileLogger,
    private val boardInteractor: BoardInteractor
) {

    fun getUser(): Single<User> =
        lessonApi
            .getUserProfile()
            .doOnSuccess {
                profileInteractor.user = it
                logger.addLog("LessonInteractor getUser new user $it")
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun getLessons(): Single<TimetableData> =
        lessonApi
            .getLessons(prefs.currentSchool?.id ?: 0)
            .onErrorResumeNext {
                // TODO: refactor Authenticator to Interceptor and handle this logic there
                val exception = if (
                    it is HttpException &&
                    it.code() == FORIBEN_ERROR_CODE &&
                    !authInteractor.isUserAuthorized()
                ) {
                    InvalidTokenException()
                } else {
                    it
                }
                Single.fromCallable { throw exception }
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun connectToLesson(lessonId: String): Observable<WebSocketState> =
        lessonRepository
            .connectToLesson(lessonId)
            .flatMap { boardInteractor.connectToBoard(lessonId) }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun joinLesson(lessonId: String): Completable =
        lessonRepository
            .joinLesson(lessonId)
            .andThen(boardInteractor.joinBoard(lessonId))
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun observeLessonParticipates(): Observable<Set<LessonParticipate>> =
        lessonRepository
            .existingParticipants
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun observeChatMessages(): Observable<List<ChatMessage>> =
        lessonRepository
            .exitingChatMessages
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun observeIceCandidates(): Observable<IceCandidateResponse> =
        lessonRepository
            .iceCandidateObservable
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun observeSdpAnswer(): Observable<ReceiveSDPAnswer> =
        lessonRepository
            .sdpAnswer
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun sendSdpData(
        localSdpOffer: SessionDescription,
        timetableLesson: TimetableLesson,
        userName: String
    ) = lessonRepository
        .sendSdpData(localSdpOffer, timetableLesson, userName)
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun leaveLesson(lessonId: String) =
        lessonRepository
            .leaveLesson()
            .andThen(boardInteractor.leaveBoard(lessonId))
            .doFinally {
                if (profileInteractor.user?.role == Roles.GUEST) {
                    logger.addLog("LessonInteractor leaveLesson guest logout")
                    authInteractor.clearTokens()
                }
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun sendIceCandidate(
        iceCandidate: IceCandidate,
        timetableLesson: TimetableLesson,
        userName: String
    ) = lessonRepository
        .sendIceCandidate(iceCandidate, timetableLesson, userName)
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun observeHandUpEvents(): Observable<HandUpResponse> =
        lessonRepository
            .handUpObservable
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun handUp(presenterStatus: PresenterStatus, timetableLesson: TimetableLesson) =
        lessonRepository
            .handUp(presenterStatus, timetableLesson)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun sendChatMessage(chatMessage: String, lessonId: String) =
        lessonRepository
            .sendChatMessage(SendMessageRequest(chatMessage), lessonId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun observeFinishLesson(): Observable<Unit> =
        lessonRepository
            .finishLessonCompletable
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun joinAsGuest(
        refLessonId: String,
        firstName: String,
        lastName: String,
        patronymic: String,
        lessonId: String
    ): Single<TimetableLesson> =
        lessonApi
            .registerGuest(
                RegisterGuestRequest(
                    refLessonId,
                    firstName,
                    lastName,
                    patronymic
                )
            )
            .onErrorResumeNext { Single.fromCallable { handleGuestLoginException(it) } }
            .doOnSuccess {
                logger.addLog("LessonInteractor leaveLesson guest authorized")
                prefs.authToken = it.accessToken
                prefs.refreshToken = ""
            }
            .flatMap { getUser() }
            .flatMapObservable { connectToLesson(lessonId) }
            .singleOrError()
            .flatMap {
                logger.addLog("LessonInteractor leaveLesson guest connect to lesson")
                lessonApi
                    .getLessonById(lessonId)
                    .subscribeOn(schedulers.io())
            }
            .map { it.toTimeTableLesson() }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnError { authInteractor.clearTokens() }

    private fun handleGuestLoginException(exception: Throwable): AccessToken {
        if (exception is ServerException && exception.code == LESSON_ERROR_CODE) {

            throw when (exception.serverExceptionResponse.message) {
                RESPONSE_LESSON_WAS_COMPLETED -> {
                    logger.addLog("LessonInteractor leaveLesson guest flow lesson ended")
                    LessonEndedException()
                }
                RESPONSE_PARTICIPANTS_LIMIT_EXCEEDED -> {
                    logger.addLog("LessonInteractor leaveLesson guest flow lesson overflow")
                    LessonOverflowException()
                }
                else -> exception
            }
        } else {
            logger.addLog("LessonInteractor leaveLesson guest flow other exception $exception")
            throw exception
        }
    }

    companion object {
        private const val RESPONSE_LESSON_WAS_COMPLETED = "LESSON_WAS_COMPLETED"
        private const val RESPONSE_PARTICIPANTS_LIMIT_EXCEEDED = "PARTICIPANTS_LIMIT_EXCEEDED"

        private const val LESSON_ERROR_CODE = 412
        private const val FORIBEN_ERROR_CODE = 403
    }
}