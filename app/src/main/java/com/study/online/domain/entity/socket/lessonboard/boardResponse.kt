package com.study.online.domain.entity.socket.lessonboard

import com.google.gson.annotations.SerializedName

sealed class BoardResponse

data class BoardState(
    @SerializedName("actual")
    val actual: Boolean,
    @SerializedName("size")
    val boardSize: BoardSize?
) : BoardResponse()

data class BoardSize(
    @SerializedName("width")
    val width: Int,
    @SerializedName("height")
    val height: Int
)

data class ShapeData(
    @SerializedName("changeId")
    val changeId: ChangeId,
    @SerializedName("updateTime")
    val updateTime: Long,
    @SerializedName("isActual")
    val isActual: Boolean,
    @SerializedName("shape")
    val shape: Shape
) : BoardResponse()

data class ChangeId(
    @SerializedName("username")
    val userName: String,
    @SerializedName("changeNumber")
    val changeNumber: Int
) : BoardResponse()

data class Shape(
    @SerializedName("color")
    val color: String,
    @SerializedName("thickness")
    val thickness: Int,
    @SerializedName("data")
    val coordinates: List<Line>,
    @SerializedName("actual")
    val actual: Boolean,
    @SerializedName("type")
    val type: String,
    @SerializedName("changeID")
    val changeId: ChangeId
)

data class Line(
    @SerializedName("from")
    val startPos: Position,
    @SerializedName("to")
    val endPos: Position
)

data class Position(
    @SerializedName("x")
    val x: Float,
    @SerializedName("y")
    val y: Float
)