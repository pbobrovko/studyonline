package com.study.online.domain.entity

import com.google.gson.annotations.SerializedName

data class RegisterGuestRequest(
    @SerializedName("refLessonId")
    val refLessonId: String,
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("lastName")
    val lastName: String,
    @SerializedName("patronymic")
    val patronymic: String
)