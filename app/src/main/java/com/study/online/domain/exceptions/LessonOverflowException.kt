package com.study.online.domain.exceptions

class LessonOverflowException(message: String = "") : RuntimeException(message)