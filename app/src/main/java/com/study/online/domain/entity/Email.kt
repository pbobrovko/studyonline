package com.study.online.domain.entity

data class Email(
    val email: String
)