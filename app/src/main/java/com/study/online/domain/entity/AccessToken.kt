package com.study.online.domain.entity

import com.google.gson.annotations.SerializedName

class AccessToken(
    @SerializedName("accessToken")
    val accessToken: String
)