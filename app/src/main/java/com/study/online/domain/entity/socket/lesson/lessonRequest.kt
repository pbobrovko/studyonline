package com.study.online.domain.entity.socket.lesson

import com.google.gson.annotations.SerializedName
import com.study.online.domain.entity.socket.MediaState
import com.study.online.domain.entity.socket.Roles

sealed class LessonRequest

data class JoinLesson(
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("lastName")
    val lastName: String,
    @SerializedName("patronymic")
    val patronymic: String,
    @SerializedName("role")
    val role: Roles,
    @SerializedName("presenterStatus")
    val presenterStatus: PresenterStatus,
    @SerializedName("mediaState")
    val mediaState: MediaState
) : LessonRequest()

data class SendSDP(
    @SerializedName("sender")
    val userName: String,
    @SerializedName("sdpOffer")
    val sdpOffer: String,
    @SerializedName("streamType")
    val streamType: String = "camera",
    @SerializedName("mediaState")
    val mediaState: MediaState
) : LessonRequest()

data class IceCandidateRequest(
    @SerializedName("candidate")
    val candidate: String,
    @SerializedName("sdpMLineIndex")
    val sdpMLineIndex: Int,
    @SerializedName("sdpMid")
    val sdpMid: String
) : LessonRequest()

data class HandUpRequest(
    @SerializedName("username")
    val userName: String,
    @SerializedName("presenterStatus")
    val presenterStatus: PresenterStatus
) : LessonRequest()

data class SendMessageRequest(
    val text: String
) : LessonRequest()