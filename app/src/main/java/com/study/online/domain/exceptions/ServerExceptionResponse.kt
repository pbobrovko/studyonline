package com.study.online.domain.exceptions

import com.google.gson.annotations.SerializedName

data class ServerExceptionResponse(
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val user: User,
    @SerializedName("status")
    val status: String
)

data class User(
    val user: String
)