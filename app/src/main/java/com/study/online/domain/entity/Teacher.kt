package com.study.online.domain.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Teacher(
    @SerializedName("id")
    val id: Int,
    @SerializedName("username")
    val userName: String,
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("lastName")
    val lastName: String,
    @SerializedName("patronymic")
    val patronymic: String,
    @SerializedName("title")
    val title: String
) : Serializable