package com.study.online.domain.entity

data class LogoutRequest(
    val refreshToken: String,
    val fingerprint: String,
    val fullLogout: Boolean = false
)