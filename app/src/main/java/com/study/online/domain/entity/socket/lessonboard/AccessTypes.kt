package com.study.online.domain.entity.socket.lessonboard

import com.google.gson.annotations.SerializedName

enum class AccessTypes(
    private val serializedName: String
) {
    @SerializedName("onlyAsked")
    ONLY_ASKED("onlyAsked"),
    @SerializedName("drawAll")
    DRAW_ALL("drawAll"),
    @SerializedName("disableDraw")
    DISABLE_DRAW("disableDraw");

    override fun toString(): String = serializedName
}