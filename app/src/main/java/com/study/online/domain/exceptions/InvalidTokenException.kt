package com.study.online.domain.exceptions

import java.io.IOException

class InvalidTokenException : IOException()