package com.study.online.domain.interactor

import com.study.online.core.global.FileLogger
import com.study.online.core.global.scheduler.SchedulersProvider
import com.study.online.data.BoardRepository
import com.study.online.data.socket.WebSocketState
import com.study.online.domain.entity.socket.lessonboard.BoardDataTypes
import com.study.online.domain.entity.socket.lessonboard.BoardSize
import com.study.online.domain.entity.socket.lessonboard.ShapeData
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class BoardInteractor @Inject constructor(
    private val schedulers: SchedulersProvider,
    private val boardRepository: BoardRepository,
    private val logger: FileLogger
) {

    fun connectToBoard(lessonId: String): Observable<WebSocketState> =
        boardRepository
            .connectToBoard(lessonId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun joinBoard(lessonId: String): Completable =
        boardRepository
            .joinBoard(lessonId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun observeBoardData(): Observable<Pair<BoardDataTypes, List<ShapeData>>> =
        boardRepository
            .shapeData
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun leaveBoard(lessonId: String) =
        boardRepository
            .leaveBoard(lessonId)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun observeBoardState(): Observable<Boolean> =
        boardRepository
            .boardState
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun observeBoardSize(): Observable<BoardSize> =
        boardRepository
            .boardSize
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
}