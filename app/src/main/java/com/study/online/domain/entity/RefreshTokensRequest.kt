package com.study.online.domain.entity

data class RefreshTokensRequest(
    val refreshToken: String,
    val fingerprint: String
)