package com.study.online.domain.exceptions

class ServerException(
    override val message: String,
    val code: Int,
    val serverExceptionResponse: ServerExceptionResponse
) : RuntimeException(message)