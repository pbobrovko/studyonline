package com.study.online.domain.entity

data class ResetPasswordRequest(
    val user: String
)