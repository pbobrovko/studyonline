package com.study.online.domain.entity.socket.lessonboard

import com.google.gson.annotations.SerializedName

enum class BoardDataTypes constructor(
    private val serializedName: String
) {
    @SerializedName("joinBoard")
    JOIN_BOARD("joinBoard"),
    @SerializedName("boardState")
    BOARD_STATE("boardState"),
    @SerializedName("accessState")
    ACCESS_STATE("accessState"),
    @SerializedName("accessType")
    ACCESS_TYPE("accessType"),
    @SerializedName("leaveBoard")
    LEAVE_BOARD("leaveBoard"),
    @SerializedName("closeBoard")
    CLOSE_BOARD("closeBoard"),
    @SerializedName("openBoard")
    OPEN_BOARD("openBoard"),
    @SerializedName("addChange")
    ADD_CHANGE("addChange"),
    @SerializedName("forbidDraw")
    FORBID_DRAW("forbidDraw"),
    @SerializedName("undoLastChange")
    UNDO_LAST_CHANGE("undoLastChange"),
    @SerializedName("redoLastChange")
    REDO_LAST_CHANGE("redoLastChange"),
    @SerializedName("cleanBoard")
    CLEAN_BOARD("cleanBoard");

    override fun toString(): String = serializedName
}