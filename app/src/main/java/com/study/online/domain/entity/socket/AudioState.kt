package com.study.online.domain.entity.socket

import com.google.gson.annotations.SerializedName

data class AudioState(
    @SerializedName("deviceId")
    val deviceId: DeviceId
)