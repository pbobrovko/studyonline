package com.study.online.domain.interactor

import com.study.online.core.global.FileLogger
import com.study.online.core.global.scheduler.SchedulersProvider
import com.study.online.data.server.AuthApi
import com.study.online.data.storage.Prefs
import com.study.online.domain.entity.*
import io.reactivex.Completable
import io.reactivex.Single
import java.net.UnknownHostException
import javax.inject.Inject

class AuthInteractor @Inject constructor(
    private val authApi: AuthApi,
    private val schedulers: SchedulersProvider,
    private val prefs: Prefs,
    private val metaData: MetaData,
    private val logger: FileLogger,
    private val profileInteractor: ProfileInteractor
) {

    fun registerEmail(email: Email): Single<Email> =
        authApi
            .registerEmail(email)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun requestUserInfoByEmailCode(emailCode: String): Single<CreatePasswordInfo> =
        authApi
            .checkResetPassword(EmailCode(emailCode))
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun createPassword(createPassword: CreatePassword): Completable =
        authApi
            .createPassword(createPassword)
            .doOnSuccess {
                logger.addLog("AuthInteractor createPassword success $it")
                prefs.authToken = it.authToken
                prefs.refreshToken = it.refreshToken
            }
            .ignoreElement()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun login(user: String, password: String): Completable =
        authApi
            .login(LoginRequest(user, password, metaData))
            .doOnSuccess {
                logger.addLog("AuthInteractor login success $it")
                prefs.authToken = it.authToken
                prefs.refreshToken = it.refreshToken
            }
            .ignoreElement()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun logout(): Completable =
        authApi
            .logout(LogoutRequest(prefs.refreshToken, metaData.deviceId))
            .onErrorComplete {
                logger.addLog("AuthInteractor logout onErrorComplete $it")
                it !is UnknownHostException
            }
            .doOnTerminate {
                logger.addLog("AuthInteractor logout doOnTerminate")
                clearTokens()
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun resetPassword(resetPassword: ResetPasswordRequest): Single<Email> =
        authApi
            .resetPassword(resetPassword)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    fun isUserAuthorized() =
        prefs.authToken.isNotEmpty() &&
                profileInteractor.user != null &&
                prefs.refreshToken.isNotEmpty()

    fun clearTokens() {
        logger.addLog("AuthInteractor clearTokens")
        prefs.authToken = ""
        prefs.refreshToken = ""
        prefs.currentSchool = null
        profileInteractor.user = null
    }
}