package com.study.online.domain.exceptions

class LessonEndedException(message: String = "") : RuntimeException(message)