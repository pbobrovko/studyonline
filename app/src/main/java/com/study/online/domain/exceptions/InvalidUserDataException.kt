package com.study.online.domain.exceptions

import java.lang.RuntimeException

class InvalidUserDataException : RuntimeException()