package com.study.online.domain.entity.socket

enum class Roles {
    STUDENT,
    GUEST,
    TEACHER
}