package com.study.online.domain.entity.socket

import com.google.gson.annotations.SerializedName

data class VideoState(
    @SerializedName("width")
    val width: Int = 640,
    @SerializedName("framerate")
    val framerate: Int = 30,
    @SerializedName("deviceId")
    val deviceId: DeviceId
)