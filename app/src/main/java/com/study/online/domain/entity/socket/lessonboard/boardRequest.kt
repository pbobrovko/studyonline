package com.study.online.domain.entity.socket.lessonboard

import com.google.gson.annotations.SerializedName

sealed class BoardRequest

data class BoardId(
    @SerializedName("boardId")
    val boardId: String
) : BoardRequest()