package com.study.online.domain.entity

import com.google.gson.annotations.SerializedName

data class MetaData(
    @SerializedName("fingerprint")
    val deviceId: String,
    @SerializedName("userAgent")
    val userAgent: String = ANDROID,
    @SerializedName("ip")
    val ip: String = ""
) {

    companion object {
        private const val ANDROID = "Android"
    }
}