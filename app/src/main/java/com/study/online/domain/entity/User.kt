package com.study.online.domain.entity

import com.google.gson.annotations.SerializedName
import com.study.online.domain.entity.socket.Roles

data class User(
    @SerializedName("username")
    val userName: String,
    @SerializedName("role")
    val role: Roles,
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("lastName")
    val lastName: String,
    @SerializedName("patronymic")
    val patronymic: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("secondPhone")
    val secondPhone: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("lessonId")
    val lessonId: Int,
    @SerializedName("schools")
    val schools: List<School>
)