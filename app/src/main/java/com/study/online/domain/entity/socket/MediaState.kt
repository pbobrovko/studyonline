package com.study.online.domain.entity.socket

import com.google.gson.annotations.SerializedName

// TODO: Отрефакторить этот класс под возвращаемый из сокета JSON
data class MediaState(
    @SerializedName("video")
    val video: VideoState,
    @SerializedName("audio")
    val audioState: AudioState?
)