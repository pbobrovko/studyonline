package com.study.online.domain.entity

data class LoginRequest(
    val user: String,
    val password: String,
    val metaData: MetaData
)