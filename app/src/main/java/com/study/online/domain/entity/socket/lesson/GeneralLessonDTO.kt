package com.study.online.domain.entity.socket.lesson

import com.google.gson.annotations.SerializedName

data class GeneralLessonDTO<T>(
    @SerializedName("id")
    val dataType: LessonDataTypes,
    @SerializedName("lesson")
    val lesson: String?,
    @SerializedName("username")
    val userName: String?,
    @SerializedName("data")
    val data: T?
)