package com.study.online.domain.entity.socket.lesson

import com.google.gson.annotations.SerializedName
import com.study.online.domain.entity.socket.Roles

sealed class LessonResponse

data class ChatMessage(
    @SerializedName("id")
    val id: Int,
    @SerializedName("text")
    val text: String,
    @SerializedName("userdata")
    val userFullName: UserFullName,
    @SerializedName("username")
    val userName: String,
    @SerializedName("time")
    val time: String
) : LessonResponse()

data class UserFullName(
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("lastName")
    val lastName: String,
    @SerializedName("patronymic")
    val patronymic: String
)

data class LessonParticipate(
    @SerializedName("username")
    val userName: String,
    @SerializedName("userdata")
    val userData: UserData
) : LessonResponse()

data class UserData(
    @SerializedName("firstName")
    val firstName: String,
    @SerializedName("lastName")
    val lastName: String,
    @SerializedName("patronymic")
    val patronymic: String,
    @SerializedName("presenterStatus")
    val presenterStatus: PresenterStatus,
    @SerializedName("role")
    val roles: Roles,
    @SerializedName("mediaState")
    val mediaState: Any,
    @SerializedName("audio")
    val isAudioEnabled: Boolean
)

data class ReceiveSDPAnswer(
    @SerializedName("username")
    val username: String,
    @SerializedName("sdpAnswer")
    val sdpAnswer: String
) : LessonResponse()

data class IceCandidateResponse(
    @SerializedName("username")
    val userName: String,
    @SerializedName("candidate")
    val candidate: Candidate
) : LessonResponse()

data class Candidate(
    @SerializedName("candidate")
    val candidate: String,
    @SerializedName("sdpMLineIndex")
    val sdpMLineIndex: Int,
    @SerializedName("sdpMid")
    val sdpMid: String
)

data class HandUpResponse(
    @SerializedName("username")
    val userName: String,
    @SerializedName("presenterStatus")
    val presenterStatus: PresenterStatus,
    @SerializedName("handUpTime")
    val handUpTime: Long
) : LessonResponse()

data class UserName(
    @SerializedName("username")
    val userName: String
)