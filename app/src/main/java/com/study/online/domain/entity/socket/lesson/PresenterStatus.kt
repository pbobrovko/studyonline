package com.study.online.domain.entity.socket.lesson

enum class PresenterStatus {
    PRESENTER,
    NOT_READY_TO_ANSWER,
    READY_TO_ANSWER
}