package com.study.online.domain.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class StudentGroup(
    @SerializedName("groupName")
    val groupName: String,
    @SerializedName("studentsCount")
    val studentsCount: Int
) : Serializable