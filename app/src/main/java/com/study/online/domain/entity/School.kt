package com.study.online.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class School(
    val id: Int,
    val name: String
) : Parcelable