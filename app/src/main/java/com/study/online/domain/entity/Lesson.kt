package com.study.online.domain.entity

import com.google.gson.annotations.SerializedName
import com.study.online.ui.main.timetable.adapter.LessonStatus

data class Lesson(
    @SerializedName("id")
    val id: Int,
    @SerializedName("refId")
    val refId: String,
    @SerializedName("dateStart")
    val dateStart: String,
    @SerializedName("dateEnd")
    val dateEnd: String,
    @SerializedName("topic")
    val topic: String,
    @SerializedName("subject")
    val subject: School,
    @SerializedName("description")
    val description: String,
    @SerializedName("status")
    val status: LessonStatus,
    @SerializedName("teacher")
    val teacher: Teacher,
    @SerializedName("schoolId")
    val schoolId: Int,
    @SerializedName("recurrentType")
    val recurrentType: String,
    @SerializedName("studentGroups")
    val studentGroups: List<StudentGroup>
) {

    fun toTimeTableLesson() =
        TimetableLesson(
            id,
            dateStart,
            topic,
            subject.name,
            description,
            status,
            teacher,
            studentGroups
        )
}