package com.study.online.domain.entity.socket.lessonboard

import com.google.gson.annotations.SerializedName

data class GeneralBoardDTO<T>(
    @SerializedName("id")
    val id: BoardDataTypes,
    @SerializedName("data")
    val data: T?
)