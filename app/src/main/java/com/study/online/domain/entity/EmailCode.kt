package com.study.online.domain.entity

data class EmailCode(
    val emailCode: String
)