package com.study.online.domain.interactor

import com.study.online.data.storage.Prefs
import com.study.online.domain.entity.User
import javax.inject.Inject

class ProfileInteractor @Inject constructor(
    private val prefs: Prefs
) {

    @Volatile
    var user: User? = prefs.user
        set(value) {
            field = value
            prefs.user = value
        }
}