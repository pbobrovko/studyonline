package com.study.online.domain.entity

data class CreatePassword(
    val user: String,
    val password: String,
    val emailCode: String
)