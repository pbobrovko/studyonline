package com.study.online.domain.entity

import com.google.gson.annotations.SerializedName
import com.study.online.ui.main.timetable.adapter.LessonStatus
import java.io.Serializable

data class TimetableData(
    @SerializedName("totalPages")
    val totalPages: Int,
    @SerializedName("totalElements")
    val totalElements: Int,
    @SerializedName("pageable")
    val pageable: String,
    @SerializedName("sort")
    val sort: SortData,
    @SerializedName("numberOfElements")
    val numberOfElements: Int,
    @SerializedName("first")
    val isFirst: Boolean,
    @SerializedName("last")
    val isLast: Boolean,
    @SerializedName("size")
    val size: Int,
    @SerializedName("content")
    val content: List<Content>,
    @SerializedName("number")
    val number: Int,
    @SerializedName("empty")
    val isEmpty: Boolean
)

data class SortData(
    @SerializedName("sorted")
    val isSorted: Boolean,
    @SerializedName("unsorted")
    val isUnsorted: Boolean,
    @SerializedName("empty")
    val isEmpty: Boolean
)

data class Content(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("lessons")
    val timetableLessons: List<TimetableLesson>
)

sealed class Timetable

data class TimetableLesson(
    @SerializedName("id")
    val id: Int,
    @SerializedName("time")
    val time: String,
    @SerializedName("topic")
    val topic: String,
    @SerializedName("subject")
    val subject: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("status")
    val status: LessonStatus,
    @SerializedName("teacher")
    val teacher: Teacher,
    @SerializedName("studentGroups")
    val studentGroups: List<StudentGroup>
) : Timetable(), Serializable

data class TimetableDate(
    val date: String
) : Timetable(), Serializable

fun Content.toTimetableDate() = TimetableDate(this.date)