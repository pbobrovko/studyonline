package com.study.online.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreatePasswordInfo(
    val user: String,
    val email: String
) : Parcelable