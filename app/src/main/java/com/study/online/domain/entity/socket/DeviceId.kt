package com.study.online.domain.entity.socket

import com.google.gson.annotations.SerializedName

data class DeviceId(
    @SerializedName("exact")
    val exact: String
)