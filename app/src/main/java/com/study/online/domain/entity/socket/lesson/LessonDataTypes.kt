package com.study.online.domain.entity.socket.lesson

import com.google.gson.annotations.SerializedName

enum class LessonDataTypes {
    @SerializedName("joinLesson")
    JOIN_LESSON,
    @SerializedName("newParticipantArrived")
    NEW_PARTICIPANT_ARRIVED,
    @SerializedName("participantLeft")
    PARTICIPANT_LEFT,
    @SerializedName("finishLesson")
    FINISH_LESSON,
    @SerializedName("iceCandidate")
    ICE_CANDIDATE,
    @SerializedName("switchScreen")
    SWITCH_SCREEN,
    @SerializedName("existingParticipants")
    EXISTING_PARTICIPANTS,
    @SerializedName("receiveVideoAnswer")
    RECEIVE_VIDEO_ANSWER,
    @SerializedName("Error")
    ERROR,
    @SerializedName("handUp")
    HAND_UP,
    @SerializedName("changeUserMediaState")
    CHANGE_USER_MEDIA_STATE,
    @SerializedName("newChatMessage")
    NEW_CHAT_MESSAGE,
    @SerializedName("existingChatMessages")
    EXISTING_CHAT_MESSAGES,
    @SerializedName("userListIsChanged")
    USER_LIST_CHANGED,
    @SerializedName("receiveVideo")
    RECEIVE_VIDEO,
    @SerializedName("leaveLesson")
    LEAVE_LESSON,
    @SerializedName("onIceCandidate")
    ON_ICE_CANDIDATE,
    @SerializedName("sendChatMessage")
    SEND_CHAT_MESSAGE
}