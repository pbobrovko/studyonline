package com.study.online.domain.interactor

import android.content.Context
import com.study.online.domain.entity.socket.Roles
import io.reactivex.Single
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class FileInteractor @Inject constructor(
    private val context: Context
) {

    private val assetManager = context.resources.assets

    fun getRolePdf(role: Roles): Single<File> =
        Single.fromCallable {
            val fileName = getFileName(role)
            val inputStream = assetManager.open(fileName)

            val file = File.createTempFile(fileName, "")
            val fileOutputStream = FileOutputStream(file)
            fileOutputStream.write(inputStream.readBytes())
            file
        }

    private fun getFileName(role: Roles) = when (role) {
        Roles.GUEST -> GUEST_INSTRUCTIONS
        Roles.TEACHER -> TEACHER_INSTRUCTIONS
        Roles.STUDENT -> STUDENT_INSTRUCTIONS
    }

    companion object {
        private const val GUEST_INSTRUCTIONS = "guest_instructions.pdf"
        private const val STUDENT_INSTRUCTIONS = "student_instructions.pdf"
        private const val TEACHER_INSTRUCTIONS = "teacher_instructions.pdf"
    }
}