package com.study.online.domain.entity

import com.google.gson.annotations.SerializedName

data class AuthData(
    @SerializedName("accessToken")
    val authToken: String,
    @SerializedName("refreshToken")
    val refreshToken: String
)