package com.study.online.app.di.provider

import com.google.gson.Gson
import com.study.online.app.di.OkHttpLabel
import com.study.online.app.di.RoutingEndpointLabel
import com.study.online.data.server.RoutingApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class RoutingApiProvider @Inject constructor(
    @OkHttpLabel private val okHttpClient: OkHttpClient,
    private val gson: Gson,
    @RoutingEndpointLabel private val routingEndpoint: String
) : Provider<RoutingApi> {

    override fun get(): RoutingApi =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .baseUrl(routingEndpoint)
            .build()
            .create(RoutingApi::class.java)
}