package com.study.online.app.di.provider

import com.study.online.domain.entity.MetaData
import com.study.online.domain.entity.socket.AudioState
import com.study.online.domain.entity.socket.DeviceId
import com.study.online.domain.entity.socket.MediaState
import com.study.online.domain.entity.socket.VideoState
import javax.inject.Inject
import javax.inject.Provider

class MediaStateProvider @Inject constructor(
    private val metaData: MetaData
) : Provider<MediaState> {

    override fun get(): MediaState {
        val deviceId = DeviceId(metaData.deviceId)
        val videoState = VideoState(deviceId = deviceId)
        val audioState = AudioState(deviceId)

        return MediaState(videoState, audioState)
    }
}