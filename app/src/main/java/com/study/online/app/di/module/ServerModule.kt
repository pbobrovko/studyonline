package com.study.online.app.di.module

import com.google.gson.Gson
import com.study.online.BuildConfig
import com.study.online.app.di.*
import com.study.online.app.di.provider.*
import com.study.online.data.BoardRepository
import com.study.online.data.LessonRepository
import com.study.online.data.server.AuthApi
import com.study.online.data.server.LessonApi
import com.study.online.data.server.RoutingApi
import com.study.online.data.socket.BoardSocket
import com.study.online.data.socket.LessonSocket
import com.study.online.domain.entity.socket.MediaState
import com.study.online.domain.interactor.AuthInteractor
import com.study.online.domain.interactor.FileInteractor
import com.study.online.domain.interactor.LessonInteractor
import com.study.online.domain.interactor.ProfileInteractor
import okhttp3.OkHttpClient
import toothpick.config.Module

class ServerModule : Module() {

    init {
        // Network
        bind(Gson::class.java)
            .toProvider(GsonProvider::class.java)
            .providesSingletonInScope()
        bind(OkHttpClient::class.java)
            .withName(AuthOkHttpLabel::class.java)
            .toProvider(AuthOkHttpProvider::class.java)
        bind(OkHttpClient::class.java)
            .withName(OkHttpLabel::class.java)
            .toProvider(OkHttpProvider::class.java)
        bind(String::class.java)
            .withName(AuthEndpointLabel::class.java)
            .toInstance(BuildConfig.AUTH_ENDPOINT)
        bind(String::class.java)
            .withName(LessonEndpointLabel::class.java)
            .toInstance(BuildConfig.LESSON_ENDPOINT)
        bind(String::class.java)
            .withName(RoutingEndpointLabel::class.java)
            .toInstance(BuildConfig.ROUTING_ENDPOINT)
        bind(AuthApi::class.java)
            .toProvider(AuthApiProvider::class.java)
            .providesSingletonInScope()
        bind(LessonApi::class.java)
            .toProvider(LessonApiProvider::class.java)
            .providesSingletonInScope()
        bind(RoutingApi::class.java)
            .toProvider(RoutingApiProvider::class.java)
            .providesSingletonInScope()

        bind(LessonSocket::class.java)
            .singletonInScope()
        bind(BoardSocket::class.java)
            .singletonInScope()

        bind(LessonRepository::class.java)
            .singletonInScope()
        bind(BoardRepository::class.java)
            .singletonInScope()

        bind(AuthInteractor::class.java)
            .singletonInScope()
        bind(LessonInteractor::class.java)
            .singletonInScope()
        bind(ProfileInteractor::class.java)
            .singletonInScope()
        bind(FileInteractor::class.java)
            .singletonInScope()

        // Socket default DTO-s
        bind(MediaState::class.java)
            .toProvider(MediaStateProvider::class.java)
            .providesSingletonInScope()
    }
}