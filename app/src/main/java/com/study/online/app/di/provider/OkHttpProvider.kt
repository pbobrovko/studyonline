package com.study.online.app.di.provider

import com.study.online.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider

class OkHttpProvider @Inject constructor(
    private val exceptionsInterceptor: ExceptionsInterceptor,
    private val serverInterceptor: ServerInterceptor,
    private val authenticator: StudyOnlineAuthenticator
) : Provider<OkHttpClient> {

    override fun get(): OkHttpClient {
        return with(OkHttpClient.Builder()) {
            connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            readTimeout(TIMEOUT, TimeUnit.SECONDS)
            if (BuildConfig.DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }

                authenticator(authenticator)
                addInterceptor(exceptionsInterceptor)
                addInterceptor(serverInterceptor)
                addNetworkInterceptor(loggingInterceptor)
            }
            build()
        }
    }

    companion object {
        private const val TIMEOUT = 30L
    }
}