package com.study.online.app.di.provider

import com.google.gson.Gson
import com.study.online.app.di.AuthOkHttpLabel
import com.study.online.app.di.AuthEndpointLabel
import com.study.online.data.server.AuthApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class AuthApiProvider @Inject constructor(
    @AuthOkHttpLabel private val okHttpClient: OkHttpClient,
    private val gson: Gson,
    @AuthEndpointLabel private val serverBaseUrl: String
) : Provider<AuthApi> {

    override fun get(): AuthApi =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .baseUrl(serverBaseUrl)
            .build()
            .create(AuthApi::class.java)
}