package com.study.online.app.di.provider

import com.google.gson.Gson
import com.study.online.app.di.OkHttpLabel
import com.study.online.app.di.LessonEndpointLabel
import com.study.online.data.server.LessonApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class LessonApiProvider @Inject constructor(
    @OkHttpLabel private val okHttpClient: OkHttpClient,
    private val gson: Gson,
    @LessonEndpointLabel private val lessonEndpoint: String
) : Provider<LessonApi> {

    override fun get(): LessonApi =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .baseUrl(lessonEndpoint)
            .build()
            .create(LessonApi::class.java)
}