package com.study.online.app.di.provider

import android.content.Context
import com.study.online.R
import com.study.online.data.storage.Prefs
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class ServerInterceptor @Inject constructor(
    private val prefs: Prefs,
    private val context: Context
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val bearer = context.getString(R.string.auth_token_prefix)
        val token = String.format(bearer, prefs.authToken)
        val request = chain.request()
        val newRequest = request
            .newBuilder()
            .addHeader(AUTHORIZATION, token)
            .build()

        return chain.proceed(newRequest)
    }

    companion object {
        private const val AUTHORIZATION = "Authorization"
    }
}