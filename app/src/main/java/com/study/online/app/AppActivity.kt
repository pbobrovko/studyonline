package com.study.online.app

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import com.google.gson.Gson
import com.study.online.R
import com.study.online.app.di.DI
import com.study.online.app.presentation.AppPresenter
import com.study.online.app.presentation.AppView
import com.study.online.core.presentation.FlowFragment
import com.study.online.coreui.dialog.errordialog.ErrorDialog
import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import com.study.online.data.storage.Prefs
import com.study.online.extension.invisibleUnless
import kotlinx.android.synthetic.main.activity_app.*
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import toothpick.Toothpick
import java.util.*
import javax.inject.Inject

class AppActivity : MvpAppCompatActivity(), AppView {

    @InjectPresenter
    lateinit var presenter: AppPresenter

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    private val navigator: Navigator =
        SupportAppNavigator(this, supportFragmentManager, R.id.appContainer_fl)

    private val currentFlowFragment: FlowFragment?
        get() = supportFragmentManager.findFragmentById(R.id.appContainer_fl) as? FlowFragment

    @ProvidePresenter
    fun providePresenter(): AppPresenter =
        Toothpick.openScope(DI.SERVER_SCOPE).getInstance(AppPresenter::class.java)

    override fun attachBaseContext(newBase: Context) {
        val newContext = applySelectedLanguage(newBase)
        super.attachBaseContext(newContext)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScope(DI.SERVER_SCOPE))

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_app)

        presenter.onAppStarted(intent, savedInstanceState != null)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        presenter.onNewIntent(intent)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()

        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()

        super.onPause()
    }

    override fun onBackPressed() {
        currentFlowFragment?.onBackPressed() ?: presenter.onBackPressed()
    }

    override fun showErrorDialog(typeErrorDialog: ErrorDialogType) {
        if (supportFragmentManager.findFragmentByTag(TAG_ERROR_DIALOG) == null) {
            ErrorDialog
                .newInstance(typeErrorDialog)
                .show(supportFragmentManager, TAG_ERROR_DIALOG)
        }
    }

    override fun showProgress(isShow: Boolean) {
        appProgress.invisibleUnless(isShow)
    }

    override fun checkPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                this, arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                KEY_PERMISSION_REQUEST_CODE
            )
        } else {
            presenter.onPermissionResult(true, intent)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == KEY_PERMISSION_REQUEST_CODE) {
            presenter.onPermissionResult(
                grantResults[0] == PackageManager.PERMISSION_GRANTED,
                intent
            )
        }
    }

    private fun applySelectedLanguage(context: Context): Context {
        // Prefs cannot be injected because at the time of calling this method
        // scope is not open yet.
        val gson = Gson()
        val prefs = Prefs(context, gson)

        // If selected local not set, then use the provided from system.
        // Evaluation is postponed to first access.
        // There is no need to set default English, because it will be done by system.
        val savedLocale = prefs.currentLocale ?: return context

        val newConfig = Configuration(context.resources.configuration)
        Locale.setDefault(savedLocale)
        newConfig.setLocale(savedLocale)
        return context.createConfigurationContext(newConfig)
    }

    companion object {
        private const val TAG_ERROR_DIALOG = "TAG_ERROR_DIALOG"

        private const val KEY_PERMISSION_REQUEST_CODE = 1001
    }
}