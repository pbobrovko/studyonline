package com.study.online.app.di.provider

import android.annotation.SuppressLint
import android.content.Context
import com.study.online.R
import com.study.online.core.global.FileLogger
import com.study.online.data.server.AuthApi
import com.study.online.data.storage.Prefs
import com.study.online.domain.entity.MetaData
import com.study.online.domain.entity.RefreshTokensRequest
import com.study.online.domain.exceptions.InvalidTokenException
import okhttp3.Authenticator
import okhttp3.Route
import okhttp3.Response
import okhttp3.Request
import javax.inject.Inject

class StudyOnlineAuthenticator @Inject constructor(
    private val authApi: AuthApi,
    private val prefs: Prefs,
    private val context: Context,
    private val metadata: MetaData,
    private val logger: FileLogger
) : Authenticator {

    @SuppressLint("CheckResult")
    @Synchronized
    override fun authenticate(route: Route?, response: Response): Request? {
        logger.addLog("StudyOnlineAuthenticator authenticate failed response $response")
        val storedToken = prefs.authToken
        val requestedToken = response
            .request
            .header(AUTHORIZATION)
            ?.removePrefix(BEARER)
        val requestBuilder = response.request.newBuilder()

        logger.addLog(
            "StudyOnlineAuthenticator authenticate is " +
                    "tokens identical ${storedToken == requestedToken}"
        )
        if (storedToken == requestedToken) {
            val refreshToken = prefs.refreshToken
            if (refreshToken.isNotEmpty()) {
                try {
                    authApi
                        .refreshToken(RefreshTokensRequest(refreshToken, metadata.deviceId))
                        .doOnSuccess {
                            logger.addLog("StudyOnlineAuthenticator authenticate refresh success")
                            prefs.authToken = it.authToken
                            prefs.refreshToken = it.refreshToken
                        }
                        .blockingGet()
                } catch (exception: RuntimeException) {
                    logger.addLog("StudyOnlineAuthenticator authenticate refresh failed")
                    throw InvalidTokenException()
                }
            } else {
                logger.addLog("StudyOnlineAuthenticator authenticate refresh token is empty")
                throw InvalidTokenException()
            }
        }
        val bearer = context.getString(R.string.auth_token_prefix)
        val token = String.format(bearer, prefs.authToken)
        requestBuilder.header(AUTHORIZATION, token)

        return requestBuilder.build()
    }

    companion object {
        private const val AUTHORIZATION = "Authorization"
        private const val BEARER = "Bearer "
    }
}