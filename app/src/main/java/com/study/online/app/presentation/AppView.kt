package com.study.online.app.presentation

import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface AppView : MvpView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showErrorDialog(typeErrorDialog: ErrorDialogType)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showProgress(isShow: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun checkPermission()
}