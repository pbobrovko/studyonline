package com.study.online.app.di.provider

import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Provider

class ExpiredSessionSubjectProvider @Inject constructor() : Provider<PublishSubject<Boolean>> {

    override fun get(): PublishSubject<Boolean> = PublishSubject.create()
}