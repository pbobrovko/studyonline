package com.study.online.app.di.provider

import com.google.gson.Gson
import com.study.online.domain.exceptions.ServerException
import com.study.online.domain.exceptions.ServerExceptionResponse
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class ExceptionsInterceptor @Inject constructor(
    private val gson: Gson
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        return if (response.isSuccessful) {
            response
        } else {
            val serverExceptionResponse = gson.fromJson<ServerExceptionResponse>(
                response.body?.string(),
                ServerExceptionResponse::class.java
            )

            throw ServerException(response.message, response.code, serverExceptionResponse)
        }
    }
}