package com.study.online.app.di.module

import android.content.Context
import com.google.gson.Gson
import com.study.online.app.App
import com.study.online.app.di.ExpirySessionLabel
import com.study.online.app.di.PrivacyPolicyLabel
import com.study.online.app.di.provider.ExpiredSessionSubjectProvider
import com.study.online.app.di.provider.GsonProvider
import com.study.online.app.di.provider.MetaDataProvider
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.FileLogger
import com.study.online.core.global.ResourceManager
import com.study.online.core.global.scheduler.AppSchedulers
import com.study.online.core.global.scheduler.SchedulersProvider
import com.study.online.domain.entity.MetaData
import io.reactivex.subjects.PublishSubject
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class AppModule(
    app: App
) : Module() {

    init {
        bind(Context::class.java)
            .toInstance(app)
        bind(SchedulersProvider::class.java)
            .toInstance(AppSchedulers())
        bind(ResourceManager::class.java)
            .singletonInScope()
        bind(ErrorHandler::class.java)
            .singletonInScope()
        bind(Gson::class.java)
            .toProvider(GsonProvider::class.java)
            .providesSingletonInScope()
        bind(PublishSubject::class.java)
            .withName(ExpirySessionLabel::class.java)
            .toProvider(ExpiredSessionSubjectProvider::class.java)
            .providesSingletonInScope()
        bind(PublishSubject::class.java)
            .withName(PrivacyPolicyLabel::class.java)
            .toProvider(ExpiredSessionSubjectProvider::class.java)
            .providesSingletonInScope()

        // Navigation
        val cicerone = Cicerone.create()
        bind(Router::class.java)
            .toInstance(cicerone.router)
        bind(NavigatorHolder::class.java)
            .toInstance(cicerone.navigatorHolder)

        bind(MetaData::class.java)
            .toProvider(MetaDataProvider::class.java)
            .providesSingletonInScope()

        bind(FileLogger::class.java)
            .singletonInScope()
    }
}