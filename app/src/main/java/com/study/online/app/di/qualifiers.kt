package com.study.online.app.di

import javax.inject.Qualifier

@Qualifier
annotation class AuthEndpointLabel

@Qualifier
annotation class LessonEndpointLabel

@Qualifier
annotation class RoutingEndpointLabel

@Qualifier
annotation class MaskedEmailLabel

@Qualifier
annotation class ResendDataLabel

@Qualifier
annotation class AuthOkHttpLabel

@Qualifier
annotation class OkHttpLabel

@Qualifier
annotation class ExpirySessionLabel

@Qualifier
annotation class PrivacyPolicyLabel