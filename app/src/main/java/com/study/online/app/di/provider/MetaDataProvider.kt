package com.study.online.app.di.provider

import android.content.Context
import android.provider.Settings
import com.study.online.domain.entity.MetaData
import javax.inject.Inject
import javax.inject.Provider

class MetaDataProvider @Inject constructor(
    private val context: Context
) : Provider<MetaData> {

    override fun get(): MetaData {
        val deviceId = Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        )

        return MetaData(deviceId)
    }
}