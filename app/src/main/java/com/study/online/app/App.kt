package com.study.online.app

import android.app.Application
import com.study.online.BuildConfig
import com.study.online.app.di.DI
import com.study.online.app.di.module.AppModule
import com.study.online.app.di.module.ServerModule
import toothpick.Toothpick
import toothpick.configuration.Configuration

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initDI()
    }

    private fun initDI() {
        val configuration = if (BuildConfig.DEBUG) {
            Configuration.forDevelopment().preventMultipleRootScopes()
        } else {
            Configuration.forProduction()
        }
        Toothpick.setConfiguration(configuration)

        val appScope = Toothpick.openScope(DI.APP_SCOPE)
        appScope.installModules(AppModule(this))
        Toothpick.openScopes(DI.APP_SCOPE, DI.SERVER_SCOPE)
            .installModules(ServerModule())
    }
}