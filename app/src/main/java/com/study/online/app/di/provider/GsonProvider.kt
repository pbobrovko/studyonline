package com.study.online.app.di.provider

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.study.online.core.global.FileLogger
import com.study.online.core.global.deserializers.LessonBoardDeserializer
import com.study.online.domain.entity.socket.lessonboard.GeneralBoardDTO
import javax.inject.Inject
import javax.inject.Provider

class GsonProvider @Inject constructor(
    private val logger: FileLogger
) : Provider<Gson> {

    override fun get(): Gson =
        GsonBuilder()
            .registerTypeAdapter(GeneralBoardDTO::class.java, LessonBoardDeserializer(logger))
            .create()
}