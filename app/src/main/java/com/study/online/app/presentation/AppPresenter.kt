package com.study.online.app.presentation

import android.content.Intent
import android.net.Uri
import com.study.online.BuildConfig
import com.study.online.app.di.ExpirySessionLabel
import com.study.online.core.featureflow.AuthScreens
import com.study.online.core.global.ErrorHandler
import com.study.online.core.global.Screens
import com.study.online.core.presentation.BasePresenter
import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import com.study.online.domain.interactor.AuthInteractor
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class AppPresenter @Inject constructor(
    private val router: Router,
    private val errorHandler: ErrorHandler,
    private val authInteractor: AuthInteractor,
    @ExpirySessionLabel private val expiredSessionSubject: PublishSubject<Boolean>
) : BasePresenter<AppView>() {

    private var isAppRunning = false

    override fun onDestroy() {
        super.onDestroy()

        errorHandler.onDestroy()
    }

    fun onAppStarted(intent: Intent, isRestart: Boolean) {
        expiredSessionSubject
            .flatMap {
                authInteractor
                    .logout()
                    .andThen(Observable.just(Unit))
            }
            .subscribe {
                router.newRootScreen(Screens.AuthFlow)
            }
            .connect()

        if (BuildConfig.DEBUG) {
            viewState.checkPermission()
        } else {
            handleIntent(intent, isRestart)
        }
    }

    fun onNewIntent(intent: Intent) {
        if (isAppRunning) {
            handleIntent(intent, true)
        }
    }

    fun onBackPressed() {
        router.exit()
    }

    fun onPermissionResult(isGranted: Boolean, intent: Intent) {
        if (isGranted) {
            handleIntent(intent, false)
        } else {
            viewState.checkPermission()
        }
    }

    private fun handleIntent(intent: Intent, isRestart: Boolean) {
        val action = intent.action ?: ""

        if (action == Intent.ACTION_MAIN) {
            handleNormalFlow(isRestart)
        } else {
            intent.data?.let {
                if (it.pathSegments.size >= 1) {
                    when (it.pathSegments[1]) {
                        RESET_PASSWORD_FLOW -> startCreatePasswordFlow(it)
                        REGISTER_GUEST_FLOW -> startConnectLessonGuestFlow(it)
                    }
                } else {
                    viewState.showErrorDialog(ErrorDialogType.UNKNOWN_ERROR)
                    handleNormalFlow(isRestart)
                }
            }
        }
        isAppRunning = true
    }

    private fun handleNormalFlow(isRestart: Boolean) {
        if (!isRestart) {
            if (authInteractor.isUserAuthorized()) {
                router.newRootScreen(Screens.MainFlow)
            } else {
                authInteractor
                    .logout()
                    .doOnTerminate { router.newRootScreen(Screens.AuthFlow) }
                    .subscribe({}, {})
                    .connect()
            }
        }
    }

    private fun startCreatePasswordFlow(data: Uri) {
        val emailCode = data.lastPathSegment ?: ""

        authInteractor
            .requestUserInfoByEmailCode(emailCode)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                {
                    router.newRootScreen(Screens.CreatePasswordScreen(it, emailCode))
                },
                {
                    errorHandler.handleErrorDialog(it) { type -> viewState.showErrorDialog(type) }
                    router.exit()
                }
            )
            .connect()
    }

    private fun startConnectLessonGuestFlow(data: Uri) {
        val inviteLink = data.path ?: ""

        router.newRootChain(Screens.AuthFlow, AuthScreens.GuestLoginScreen(inviteLink))
    }

    companion object {
        private const val RESET_PASSWORD_FLOW = "reset-password"
        private const val REGISTER_GUEST_FLOW = "guest-register"
    }
}