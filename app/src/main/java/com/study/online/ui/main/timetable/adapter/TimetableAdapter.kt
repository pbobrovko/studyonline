package com.study.online.ui.main.timetable.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import com.study.online.R
import com.study.online.domain.entity.Timetable
import com.study.online.domain.entity.TimetableDate
import com.study.online.domain.entity.TimetableLesson

class TimetableAdapter(
    private val onClickListener: (Int) -> Unit,
    private val onEnterLessonClickListener: (Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val lessons = mutableListOf<Timetable>()

    override fun getItemViewType(position: Int): Int = when (lessons[position]) {
        is TimetableLesson -> TYPE_TIMETABLE_LESSON
        is TimetableDate -> TYPE_TIMETABLE_DATE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view: View
        val holder: RecyclerView.ViewHolder
        if (viewType == TYPE_TIMETABLE_LESSON) {
            view = inflater.inflate(R.layout.item_lesson, parent, false)
            holder = LessonViewHolder(view, onEnterLessonClickListener)

            view.setOnClickListener {
                val position = holder.adapterPosition
                if (position != NO_POSITION) {
                    onClickListener.invoke(position)
                }
            }
        } else {
            view = inflater.inflate(R.layout.item_timetable_date, parent, false)
            holder = DateViewHolder(view)
        }

        return holder
    }

    override fun getItemCount(): Int = lessons.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = lessons[position]
        when {
            holder is LessonViewHolder && item is TimetableLesson -> holder.bind(item)
            holder is DateViewHolder && item is TimetableDate -> holder.bind(item)
        }
    }

    fun setItems(items: List<Timetable>) {
        lessons.clear()
        lessons.addAll(items)
        notifyDataSetChanged()
    }

    companion object {
        private const val TYPE_TIMETABLE_LESSON = 1
        private const val TYPE_TIMETABLE_DATE = 2
    }
}