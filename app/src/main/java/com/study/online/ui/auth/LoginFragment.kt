package com.study.online.ui.auth

import android.graphics.Typeface
import android.os.Bundle
import android.text.InputFilter
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.study.online.R
import com.study.online.core.presentation.BaseFragment
import com.study.online.extension.invisibleUnless
import com.study.online.presentation.auth.login.LoginPresenter
import com.study.online.presentation.auth.login.LoginView
import kotlinx.android.synthetic.main.fragment_login.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope

class LoginFragment : BaseFragment(), LoginView {

    override val layoutRes: Int = R.layout.fragment_login

    @InjectPresenter
    lateinit var presenter: LoginPresenter

    @ProvidePresenter
    fun providePresenter(): LoginPresenter = scope.getInstance(LoginPresenter::class.java)

    override fun installScopeModules(scope: Scope) {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToSignUpButton()

        loginButton.setOnClickListener {
            presenter.onLoginClicked(
                loginInput.text.toString(),
                passwordInput.text.toString()
            )
        }
        toSignUpButton.setOnClickListener { presenter.onGoToSignUpClicked() }
        supportButton.setOnClickListener { presenter.onSupportClicked() }
        forgotPasswordButton.setOnClickListener { presenter.onForgotPasswordClicked() }
        loginBack.setOnClickListener { presenter.onBackPressed() }

        loginInput.filters = arrayOf(
            InputFilter.AllCaps(),
            InputFilter.LengthFilter(LOGIN_MAX_LENGTH)
        )
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    override fun showLoginError(message: String) {
        loginLayout.error = message
        loginLayout.isErrorEnabled = message.isNotEmpty()
    }

    override fun showPasswordError(message: String) {
        passwordLayout.error = message
        passwordLayout.isErrorEnabled = message.isNotEmpty()
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun enableLoginButton(isEnable: Boolean) {
        loginButton.isEnabled = isEnable
    }

    override fun showProgress(isShow: Boolean) {
        loginProgress.invisibleUnless(isShow)
    }

    private fun initToSignUpButton() {
        val span = SpannableString(getString(R.string.to_sign_up))
        span.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.blue)),
            SPAN_START_POSITION,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        span.setSpan(
            StyleSpan(Typeface.create("sans-serif-medium", Typeface.NORMAL).style),
            SPAN_START_POSITION,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        toSignUpButton.text = span
    }

    companion object {
        private const val SPAN_START_POSITION = 14
        private const val LOGIN_MAX_LENGTH = 30
    }
}