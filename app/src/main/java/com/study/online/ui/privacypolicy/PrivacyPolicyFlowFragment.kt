package com.study.online.ui.privacypolicy

import android.os.Bundle
import com.study.online.core.featureflow.PrivacyPolicyScreens
import com.study.online.core.presentation.FlowFragment
import com.study.online.extension.setLaunchScreen
import com.study.online.extension.tryToGetBoolean

class PrivacyPolicyFlowFragment : FlowFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val isFromRegistration = tryToGetBoolean(KEY_IS_FROM_REGISTRATION)
        val isTermsAndCondition = tryToGetBoolean(KEY_IS_TERMS_AND_CONDITIONS)

        if (savedInstanceState == null) {
            val screen = if (isTermsAndCondition) {
                PrivacyPolicyScreens.TermsAndConditionsScreen(isFromRegistration)
            } else {
                PrivacyPolicyScreens.PrivacyPolicyScreen(isFromRegistration)
            }

            navigator.setLaunchScreen(screen)
        }
    }

    companion object {
        private const val KEY_IS_FROM_REGISTRATION = "KEY_IS_FROM_REGISTRATION"
        private const val KEY_IS_TERMS_AND_CONDITIONS = "KEY_IS_TERMS_AND_CONDITIONS"

        fun newInstance(isFromRegistration: Boolean, isTermsAndCondition: Boolean) =
            PrivacyPolicyFlowFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(KEY_IS_FROM_REGISTRATION, isFromRegistration)
                    putBoolean(KEY_IS_TERMS_AND_CONDITIONS, isTermsAndCondition)
                }
            }
    }
}