package com.study.online.ui.main.timetable.adapter

import java.io.Serializable

enum class LessonStatus : Serializable {
    COMPLETED,
    IN_PROGRESS,
    PLANNED,
    CANCELED
}