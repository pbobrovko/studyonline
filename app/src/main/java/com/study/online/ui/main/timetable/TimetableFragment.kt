package com.study.online.ui.main.timetable

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.study.online.R
import com.study.online.core.presentation.BaseFragment
import com.study.online.coreui.dialog.LessonOverflowDialog
import com.study.online.domain.entity.Timetable
import com.study.online.extension.invisibleUnless
import com.study.online.presentation.main.timetable.TimetableLabelState
import com.study.online.presentation.main.timetable.TimetablePresenter
import com.study.online.presentation.main.timetable.TimetableView
import com.study.online.ui.main.timetable.adapter.TimetableAdapter
import com.study.online.ui.main.timetable.adapter.TimetableDecorator
import kotlinx.android.synthetic.main.fragment_timetable.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope

class TimetableFragment : BaseFragment(), TimetableView {

    private lateinit var adapter: TimetableAdapter
    private var timetableDayBackground: Drawable? = null

    override val layoutRes: Int = R.layout.fragment_timetable

    @InjectPresenter
    lateinit var presenter: TimetablePresenter

    @ProvidePresenter
    fun providePresenter(): TimetablePresenter = scope.getInstance(TimetablePresenter::class.java)

    override fun installScopeModules(scope: Scope) {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = TimetableAdapter(presenter::onLessonClicked, presenter::onEnterLessonClicked)
        timetableDayBackground =
            ContextCompat.getDrawable(requireContext(), R.drawable.day_view_background)

        timetableScrollButton.setOnClickListener { presenter.onScrollClicked() }

        timetableList.let {
            it.addItemDecoration(TimetableDecorator())
            it.layoutManager = LinearLayoutManager(requireContext())
            it.adapter = adapter
            it.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val position = (recyclerView.layoutManager as LinearLayoutManager)
                        .findFirstVisibleItemPosition()
                    presenter.onAdapterPositionChanged(position)
                }
            })
        }
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun showTimetable(timetables: List<Timetable>, atPosition: Int?) {
        adapter.setItems(timetables)
        atPosition?.let { timetableList.scrollToPosition(atPosition) }
        emptyTimetableImage.invisibleUnless(timetables.isEmpty())
        emptyTimetableMessage.invisibleUnless(timetables.isEmpty())
    }

    override fun showAdapterDate(date: String) {
        timetableDate.text = date
    }

    override fun showTimetableLabel(labelState: TimetableLabelState) {
        val backgroundTint: Int
        val textColor: Int
        val text: String
        var isVisible: Boolean
        when (labelState) {
            TimetableLabelState.YESTERDAY -> {
                text = getString(R.string.yesterday)
                textColor = ContextCompat.getColor(requireContext(), R.color.regent_gray)
                backgroundTint = ContextCompat.getColor(requireContext(), R.color.light_gray)
                isVisible = true
            }
            TimetableLabelState.TODAY -> {
                text = getString(R.string.today)
                textColor = ContextCompat.getColor(requireContext(), R.color.blue)
                backgroundTint =
                    ContextCompat.getColor(requireContext(), R.color.blue_transparent_15)
                isVisible = true
            }
            TimetableLabelState.TOMORROW -> {
                text = getString(R.string.tomorrow)
                textColor = ContextCompat.getColor(requireContext(), R.color.blue)
                backgroundTint =
                    ContextCompat.getColor(requireContext(), R.color.blue_transparent_15)
                isVisible = true
            }
            TimetableLabelState.HIDE -> {
                text = ""
                textColor = 0
                backgroundTint = 0
                isVisible = false
            }
        }

        timetableDay.setTextColor(textColor)
        timetableDay.text = text
        timetableDayBackground?.setTint(backgroundTint)
        timetableDay.background = timetableDayBackground
        timetableDay.invisibleUnless(isVisible)
    }

    override fun showScrollButton(isShow: Boolean, isScrollUp: Boolean) {
        timetableScrollButton.invisibleUnless(isShow)

        if (isShow) {
            val imageId = if (isScrollUp) {
                R.drawable.ic_up
            } else {
                R.drawable.ic_down
            }

            timetableScrollButton.setImageDrawable(
                ContextCompat.getDrawable(requireContext(), imageId)
            )
        }
    }

    override fun scrollToCurrentDate(position: Int) {
        timetableList.smoothScrollToPosition(position)
    }

    override fun showLessonOverflowDialog() {
        // TODO: Can be merged with error dialog
        if (childFragmentManager.findFragmentByTag(TAG_LESSON_OVERFLOW_DIALOG) == null) {
            LessonOverflowDialog()
                .show(childFragmentManager, TAG_LESSON_OVERFLOW_DIALOG)
        }
    }

    override fun showProgress(isShow: Boolean) {
        timetableProgress.invisibleUnless(isShow)
    }

    companion object {
        private const val TAG_LESSON_OVERFLOW_DIALOG = "TAG_LESSON_OVERFLOW_DIALOG"
    }
}