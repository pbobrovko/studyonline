package com.study.online.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import com.study.online.R
import com.study.online.core.presentation.FlowFragment
import com.study.online.presentation.main.MainFlowPresenter
import com.study.online.presentation.main.MainFlowView
import com.study.online.ui.main.timetable.TimetableFragment
import kotlinx.android.synthetic.main.fragment_main_flow.*
import kotlinx.android.synthetic.main.item_main_flow.*
import kotlinx.android.synthetic.main.item_navigation_drawer.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

class MainFlowFragment : FlowFragment(), MainFlowView, View.OnClickListener {

    private var currentTabTag = TAG_TIMETABLE_SCREEN

    private lateinit var languagePopupWindow: PopupWindow
    private lateinit var userPopupWindow: PopupWindow

    override val layoutRes = R.layout.fragment_main_flow

    @InjectPresenter
    lateinit var presenter: MainFlowPresenter

    @ProvidePresenter
    fun providePresenter(): MainFlowPresenter =
        scope.getInstance(MainFlowPresenter::class.java)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (childFragmentManager.fragments.isEmpty()) {
            presenter.onFlowStarted()
        }

        currentTabTag = savedInstanceState?.getString(TAG_CURRENT_SCREEN) ?: TAG_TIMETABLE_SCREEN

        burgerButton.setOnClickListener { presenter.onBurgerClicked() }
        drawerBurgerButton.setOnClickListener { presenter.onBurgerClicked() }
        languageMenuButton.setOnClickListener { presenter.onLanguageMenuClicked() }
        notificationButton.setOnClickListener { presenter.onNotificationsClicked() }
        userMenuButton.setOnClickListener { presenter.onUserMenuClicked() }
        supportButton.setOnClickListener { presenter.onSupportClicked() }
        mainTermsAndConditions.setOnClickListener { presenter.onTermsAndConditionsClicked() }
        mainPrivacyPolicy.setOnClickListener { presenter.onPrivacyPolicyClicked() }

        initMenus()
        timetableButton.setOnClickListener(this)
    }

    override fun openDrawer() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawer(navigationView)
        } else {
            drawerLayout.openDrawer(navigationView)
        }
    }

    override fun showLanguageMenu() {
        languagePopupWindow.showAsDropDown(languageMenuButton)
    }

    override fun showUserMenu() {
        userPopupWindow.showAsDropDown(userMenuButton)
    }

    override fun showUserInitials(initials: String) {
        userAvatar.text = initials
    }

    override fun initDrawer() {
        val timetableFragment = TimetableFragment()

        childFragmentManager.beginTransaction()
            .add(R.id.flowContainer_fl, timetableFragment, TAG_TIMETABLE_SCREEN)
            .commitNow()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString(TAG_CURRENT_SCREEN, currentTabTag)
    }

    override fun recreateActivity() {
        requireActivity().recreate()
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun showCurrentLanguage(language: String) {
        languageMenuButton.text = language
    }

    // This click listener only for drawer buttons
    override fun onClick(view: View) {
        drawerLayout.closeDrawer(navigationView)
        val tag = when (view) {
            timetableButton -> TAG_TIMETABLE_SCREEN
            else -> throw UnsupportedOperationException("ItemId = ${view.id} not found!")
        }

        onDrawerMenuItemSelected(tag)
    }

    private fun initMenus() {
        val inflater = LayoutInflater.from(requireContext())

        val languageMenu = inflater.inflate(R.layout.item_language_menu, null)
        val languageClickListener = View.OnClickListener {
            presenter.onLanguageItemClicked(it.id)
            languagePopupWindow.dismiss()
        }
        val rusButton = languageMenu.findViewById<TextView>(R.id.languageRus)
        rusButton.setOnClickListener(languageClickListener)
        val kazButton = languageMenu.findViewById<TextView>(R.id.languageKaz)
        kazButton.setOnClickListener(languageClickListener)

        languageMenuButton.post {
            languagePopupWindow = PopupWindow(
                languageMenu,
                languageMenuButton?.measuredWidth ?: 0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                true
            )
        }

        val userMenu = inflater.inflate(R.layout.item_user_menu, null)
        val userClickListener = View.OnClickListener {
            presenter.onUserItemClicked(it.id)
            userPopupWindow.dismiss()
        }
        val instructionsButton = userMenu.findViewById<TextView>(R.id.userInstructions)
        instructionsButton.setOnClickListener(userClickListener)
        val logoutButton = userMenu.findViewById<TextView>(R.id.userLogout)
        logoutButton.setOnClickListener(userClickListener)
        userPopupWindow = PopupWindow(
            userMenu,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            true
        )
    }

    private fun onDrawerMenuItemSelected(tag: String) {
        childFragmentManager.apply {
            findFragmentByTag(currentTabTag)?.let { currentFragment ->
                findFragmentByTag(tag)?.let { selectedFragment ->
                    beginTransaction()
                        .hide(currentFragment)
                        .show(selectedFragment)
                        .commit()
                    currentTabTag = tag
                }
            }
        }
    }

    companion object {
        private const val TAG_TIMETABLE_SCREEN = "TAG_TIMETABLE_SCREEN"
        private const val TAG_CURRENT_SCREEN = "TAG_CURRENT_SCREEN"
    }
}