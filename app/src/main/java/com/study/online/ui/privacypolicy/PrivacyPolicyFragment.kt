package com.study.online.ui.privacypolicy

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.study.online.R
import com.study.online.app.di.PrimitiveWrapper
import com.study.online.core.presentation.BaseFragment
import com.study.online.extension.gone
import com.study.online.extension.goneUnless
import com.study.online.extension.tryToGetBoolean
import com.study.online.extension.visible
import com.study.online.presentation.privacypolicy.PrivacyPolicyPresenter
import com.study.online.presentation.privacypolicy.PrivacyPolicyView
import kotlinx.android.synthetic.main.fragment_privacy_policy.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope
import toothpick.config.Module
import java.net.UnknownHostException

class PrivacyPolicyFragment : BaseFragment(), PrivacyPolicyView {

    override val layoutRes: Int = R.layout.fragment_privacy_policy

    @InjectPresenter
    lateinit var presenter: PrivacyPolicyPresenter

    @ProvidePresenter
    fun providePresenter(): PrivacyPolicyPresenter =
        scope.getInstance(PrivacyPolicyPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        val isFromRegistration = tryToGetBoolean(KEY_IS_FROM_REGISTRATION)

        scope.installModules(object : Module() {

            init {
                bind(PrimitiveWrapper::class.java)
                    .toInstance(PrimitiveWrapper(isFromRegistration))
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupWebView()
        privacyPolicyButton.setOnClickListener { presenter.onConfirmClicked() }
        privacyPolicyBack.setOnClickListener { presenter.onBackPressed() }
    }

    override fun showConfirmButton(isShow: Boolean) {
        privacyPolicyButton.goneUnless(isShow)
    }

    override fun setLoadUrl(url: String) {
        privacyPolicyWebView.loadUrl(url)
    }

    override fun onResume() {
        super.onResume()

        privacyPolicyWebView.onResume()
    }

    override fun onPause() {
        super.onPause()

        privacyPolicyWebView.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        privacyPolicyWebView.destroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    private fun setupWebView() {

        val webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                privacyPolicyProgress.visible()
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                privacyPolicyProgress.gone()
                super.onPageFinished(view, url)
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    if (error?.errorCode == ERROR_HOST_LOOKUP) {
                        presenter.onErrorReceived(UnknownHostException()) { type ->
                            showErrorDialog(type)
                        }
                    } else {
                        presenter.onErrorReceived(UnknownError()) { type -> showErrorDialog(type) }
                    }
                }

                super.onReceivedError(view, request, error)
            }

            override fun onReceivedError(
                view: WebView?,
                errorCode: Int,
                description: String?,
                failingUrl: String?
            ) {
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
                    if (errorCode == ERROR_HOST_LOOKUP) {
                        presenter.onErrorReceived(UnknownHostException()) { type ->
                            showErrorDialog(type)
                        }
                    } else {
                        presenter.onErrorReceived(UnknownError()) { type -> showErrorDialog(type) }
                    }
                }
                super.onReceivedError(view, errorCode, description, failingUrl)
            }
        }

        privacyPolicyWebView.apply {
            privacyPolicyWebView.webViewClient = webViewClient
            settings.javaScriptEnabled = true
            settings.defaultTextEncodingName = "utf-8"
        }
    }

    companion object {
        private const val KEY_IS_FROM_REGISTRATION = "KEY_IS_FROM_REGISTRATION"

        fun newInstance(isFromRegistration: Boolean) =
            PrivacyPolicyFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(KEY_IS_FROM_REGISTRATION, isFromRegistration)
                }
            }
    }
}