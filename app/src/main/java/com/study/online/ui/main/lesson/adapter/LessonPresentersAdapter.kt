package com.study.online.ui.main.lesson.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.study.online.R
import com.study.online.coreui.view.TextureViewRenderer
import com.study.online.domain.entity.socket.lesson.LessonParticipate
import com.study.online.presentation.main.lesson.OnFrameListener
import org.webrtc.EglBase
import org.webrtc.RendererCommon

class LessonPresentersAdapter : RecyclerView.Adapter<LessonPresentersViewHolder>() {

    private var screenWidth = 0
    private val items = mutableListOf<Pair<OnFrameListener, LessonParticipate>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LessonPresentersViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_lesson_presenter, parent, false)
        val presenterSurface = view.findViewById<TextureViewRenderer>(R.id.presenterSurface)
        presenterSurface.apply {
            init(EglBase.create().eglBaseContext, null)
            setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)
        }
        return LessonPresentersViewHolder(screenWidth, view)
    }

    override fun onBindViewHolder(holder: LessonPresentersViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item.first, item.second, position, itemCount)
    }

    override fun getItemCount() = items.size

    fun setItems(items: List<Pair<OnFrameListener, LessonParticipate>>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun setWidth(width: Int) {
        screenWidth = width
    }
}