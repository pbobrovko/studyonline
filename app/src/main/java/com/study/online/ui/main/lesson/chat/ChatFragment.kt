package com.study.online.ui.main.lesson.chat

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.study.online.R
import com.study.online.core.presentation.BaseFragment
import com.study.online.domain.entity.TimetableLesson
import com.study.online.domain.entity.socket.lesson.ChatMessage
import com.study.online.extension.hideKeyboard
import com.study.online.extension.tryToGetSerializable
import com.study.online.presentation.main.lesson.chat.ChatPresenter
import com.study.online.presentation.main.lesson.chat.ChatView
import com.study.online.ui.main.lesson.chat.adapter.ChatDecorator
import com.study.online.ui.main.lesson.chat.adapter.ChatAdapter
import kotlinx.android.synthetic.main.fragment_lesson_chat.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope
import toothpick.config.Module

class ChatFragment : BaseFragment(), ChatView {

    private lateinit var adapter: ChatAdapter

    override val layoutRes: Int = R.layout.fragment_lesson_chat

    @InjectPresenter
    lateinit var presenter: ChatPresenter

    @ProvidePresenter
    fun providePresenter(): ChatPresenter = scope.getInstance(ChatPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        val timetableLesson = tryToGetSerializable<TimetableLesson>(KEY_TIMETABLE_LESSON)

        scope.installModules(object : Module() {

            init {
                bind(TimetableLesson::class.java)
                    .toInstance(timetableLesson)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lessonChatSendButton.setOnClickListener {
            presenter.onSendMessageClicked(lessonChatMessageView.text.toString())
            hideKeyboard()
            lessonChatMessageView.clearFocus()
            lessonChatMessageView.setText("")
        }
        lessonChatBackButton.setOnClickListener { presenter.onBackPressed() }
    }

    override fun initChatAdapter(userName: String) {
        adapter = ChatAdapter(userName)
        lessonChatList.also {
            it.layoutManager = LinearLayoutManager(requireContext())
            it.addItemDecoration(ChatDecorator(userName))
            it.adapter = adapter
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    override fun setItems(items: List<ChatMessage>) {
        adapter.setItems(items)
    }

    override fun scrollToEndOfChat(position: Int) {
        lessonChatList.scrollToPosition(position)
    }

    companion object {
        private const val KEY_TIMETABLE_LESSON = "KEY_TIMETABLE_LESSON"

        fun newInstance(timetableLesson: TimetableLesson) =
            ChatFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(KEY_TIMETABLE_LESSON, timetableLesson)
                }
            }
    }
}