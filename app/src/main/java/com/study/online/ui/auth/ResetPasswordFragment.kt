package com.study.online.ui.auth

import android.graphics.Typeface
import android.os.Bundle
import android.text.InputFilter
import android.text.Spannable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import androidx.core.content.ContextCompat
import com.study.online.R
import com.study.online.core.presentation.BaseFragment
import com.study.online.extension.invisibleUnless
import com.study.online.presentation.auth.resetpassword.ResetPasswordPresenter
import com.study.online.presentation.auth.resetpassword.ResetPasswordView
import kotlinx.android.synthetic.main.fragment_reset_password.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope

class ResetPasswordFragment : BaseFragment(), ResetPasswordView {

    override val layoutRes: Int = R.layout.fragment_reset_password

    @InjectPresenter
    lateinit var presenter: ResetPasswordPresenter

    @ProvidePresenter
    fun providePresenter(): ResetPasswordPresenter =
        scope.getInstance(ResetPasswordPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToSignUpButton()
        initUserNotRegisteredText()

        sendRequestButton.setOnClickListener {
            presenter.onSendRequestClicked(loginInput.text.toString())
        }
        toSignUpButton.setOnClickListener { presenter.onToSignUpClicked() }
        supportButton.setOnClickListener { presenter.onSupportClicked() }
        resetPasswordBack.setOnClickListener { presenter.onBackPressed() }

        loginInput.filters = arrayOf(InputFilter.AllCaps())
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    override fun showMessage(message: String) {
        loginLayout.error = message
    }

    override fun showUserNotRegisteredMessage(isShow: Boolean) {
        userNotRegisteredMessage.invisibleUnless(isShow)
    }

    override fun showProgress(isShow: Boolean) {
        resetPasswordProgress.invisibleUnless(isShow)
    }

    private fun initToSignUpButton() {
        val span = SpannableString(getString(R.string.to_sign_up))
        span.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.blue)),
            SPAN_START_POSITION,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        span.setSpan(
            StyleSpan(Typeface.create("sans-serif-medium", Typeface.NORMAL).style),
            SPAN_START_POSITION,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        toSignUpButton.text = span
    }

    private fun initUserNotRegisteredText() {
        val span = SpannableString(getString(R.string.user_not_registered_error))
        span.setSpan(
            object : ClickableSpan() {
                override fun onClick(view: View) {
                    presenter.onToSignUpClicked()
                }
            },
            SPAN_USER_NOT_REGISTERED_START_POSITION,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        span.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.red)),
            SPAN_USER_NOT_REGISTERED_START_POSITION,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        userNotRegisteredMessage.movementMethod = LinkMovementMethod.getInstance()
        userNotRegisteredMessage.text = span
    }

    companion object {
        private const val SPAN_START_POSITION = 14
        private const val SPAN_USER_NOT_REGISTERED_START_POSITION = 85
    }
}