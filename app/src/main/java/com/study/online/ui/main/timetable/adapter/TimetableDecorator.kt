package com.study.online.ui.main.timetable.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.study.online.extension.dpToPx

class TimetableDecorator : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.bottom = parent.context.dpToPx(VERTICAL_PADDING)
        outRect.left = parent.context.dpToPx(HORIZONTAL_PADDING)
        outRect.right = parent.context.dpToPx(HORIZONTAL_PADDING)
    }

    companion object {
        private const val VERTICAL_PADDING = 20f
        private const val HORIZONTAL_PADDING = 35F
    }
}