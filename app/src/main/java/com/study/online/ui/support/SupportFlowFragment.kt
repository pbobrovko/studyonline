package com.study.online.ui.support

import android.os.Bundle
import com.study.online.core.featureflow.SupportScreens
import com.study.online.core.presentation.FlowFragment
import com.study.online.extension.setLaunchScreen

class SupportFlowFragment : FlowFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            navigator.setLaunchScreen(SupportScreens.SupportScreen)
        }
    }
}