package com.study.online.ui.auth

import android.os.Bundle
import android.view.View
import com.study.online.core.presentation.BaseFragment
import toothpick.Scope
import com.study.online.R
import com.study.online.presentation.auth.onboarding.OnboardingPresenter
import com.study.online.presentation.auth.onboarding.OnboardingView
import kotlinx.android.synthetic.main.fragment_onboarding.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

class OnboardingFragment : BaseFragment(), OnboardingView {

    override val layoutRes: Int = R.layout.fragment_onboarding

    @InjectPresenter
    lateinit var presenter: OnboardingPresenter

    @ProvidePresenter
    fun providePresenter(): OnboardingPresenter = scope.getInstance(OnboardingPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loginButton.setOnClickListener { presenter.onLoginClicked() }
        signUpButton.setOnClickListener { presenter.onSignUpClicked() }
        supportButton.setOnClickListener { presenter.onSupportClicked() }
        loginGuestButton.setOnClickListener { presenter.onGuestLoginClicked() }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }
}