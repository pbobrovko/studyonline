package com.study.online.ui.main.lesson.chat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.study.online.R
import com.study.online.domain.entity.socket.lesson.ChatMessage

class ChatAdapter(
    private val userName: String
) : RecyclerView.Adapter<ChatViewHolder>() {

    private val messages = mutableListOf<ChatMessage>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_lesson_chat_message, parent, false)

        return ChatViewHolder(userName, view)
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.bind(messages[position])
    }

    override fun getItemCount(): Int = messages.size

        // TODO: refactor logic of chat rendering
    // notifyDataSetChanged() using for update message time
    fun setItems(items: List<ChatMessage>) {
        messages.clear()
        messages.addAll(items)
        notifyDataSetChanged()
    }

    fun getItem(position: Int) = messages[position]
}