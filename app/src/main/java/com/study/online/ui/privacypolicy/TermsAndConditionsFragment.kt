package com.study.online.ui.privacypolicy

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.study.online.R
import com.study.online.app.di.PrimitiveWrapper
import com.study.online.core.presentation.BaseFragment
import com.study.online.extension.*
import com.study.online.presentation.privacypolicy.TermsAndConditionsPresenter
import com.study.online.presentation.privacypolicy.TermsAndConditionsView
import kotlinx.android.synthetic.main.fragment_terms_and_conditions.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope
import toothpick.config.Module
import java.net.UnknownHostException

class TermsAndConditionsFragment : BaseFragment(), TermsAndConditionsView {

    override val layoutRes: Int = R.layout.fragment_terms_and_conditions

    @InjectPresenter
    lateinit var presenter: TermsAndConditionsPresenter

    @ProvidePresenter
    fun providePresenter(): TermsAndConditionsPresenter =
        scope.getInstance(TermsAndConditionsPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        val isFromRegistration = tryToGetBoolean(KEY_IS_FROM_REGISTRATION)

        scope.installModules(object : Module() {

            init {
                bind(PrimitiveWrapper::class.java)
                    .toInstance(PrimitiveWrapper(isFromRegistration))
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupWebView()
        termsAndConditionsBack.setOnClickListener { presenter.onBackPressed() }
        termsAndConditionsButton.setOnClickListener { presenter.onConfirmClicked() }
    }

    override fun showConfirmButton(isShow: Boolean) {
        termsAndConditionsButton.goneUnless(isShow)
    }

    override fun setLoadUrl(url: String) {
        termsAndConditionsWebView.loadUrl(url)
    }

    override fun onResume() {
        super.onResume()

        termsAndConditionsWebView.onResume()
    }

    override fun onPause() {
        super.onPause()

        termsAndConditionsWebView.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        termsAndConditionsWebView.destroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    private fun setupWebView() {

        val webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                termsAndConditionsProgress.visible()
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                termsAndConditionsProgress.gone()
                super.onPageFinished(view, url)
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    if (error?.errorCode == ERROR_HOST_LOOKUP) {
                        presenter.onErrorReceived(UnknownHostException()) { type ->
                            showErrorDialog(type)
                        }
                    } else {
                        presenter.onErrorReceived(UnknownError()) { type -> showErrorDialog(type) }
                    }
                }

                super.onReceivedError(view, request, error)
            }

            override fun onReceivedError(
                view: WebView?,
                errorCode: Int,
                description: String?,
                failingUrl: String?
            ) {
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
                    if (errorCode == ERROR_HOST_LOOKUP) {
                        presenter.onErrorReceived(UnknownHostException()) { type ->
                            showErrorDialog(type)
                        }
                    } else {
                        presenter.onErrorReceived(UnknownError()) { type -> showErrorDialog(type) }
                    }
                }
                super.onReceivedError(view, errorCode, description, failingUrl)
            }
        }

        termsAndConditionsWebView.apply {
            termsAndConditionsWebView.webViewClient = webViewClient
            settings.javaScriptEnabled = true
            settings.defaultTextEncodingName = "utf-8"
        }
    }

    companion object {
        private const val KEY_IS_FROM_REGISTRATION = "KEY_IS_FROM_REGISTRATION"

        fun newInstance(isFromRegistration: Boolean) =
            TermsAndConditionsFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(KEY_IS_FROM_REGISTRATION, isFromRegistration)
                }
            }
    }
}