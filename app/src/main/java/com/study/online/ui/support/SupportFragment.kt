package com.study.online.ui.support

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import com.study.online.R
import com.study.online.core.presentation.BaseFragment
import com.study.online.presentation.support.SupportPresenter
import com.study.online.presentation.support.SupportView
import kotlinx.android.synthetic.main.fragment_support.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope

class SupportFragment : BaseFragment(), SupportView {

    override val layoutRes: Int = R.layout.fragment_support

    @InjectPresenter
    lateinit var presenter: SupportPresenter

    @ProvidePresenter
    fun providePresenter(): SupportPresenter = scope.getInstance(SupportPresenter::class.java)

    override fun installScopeModules(scope: Scope) {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        supportContactOne.setOnClickListener {
            presenter.onPhoneNumberClicked(supportContactOne.text.toString())
        }
        supportContactTwo.setOnClickListener {
            presenter.onPhoneNumberClicked(supportContactTwo.text.toString())
        }
        supportContactThree.setOnClickListener {
            presenter.onPhoneNumberClicked(supportContactThree.text.toString())
        }
        supportEmail.setOnClickListener { presenter.onEmailClicked(supportEmail.text.toString()) }
        supportBack.setOnClickListener { presenter.onBackPressed() }
        downloadTeacherInstructions.setOnClickListener { presenter.onDownloadTeacherClicked() }
        downloadStudentInstructions.setOnClickListener { presenter.onDownloadStudentClicked() }
        downloadGuestInstructions.setOnClickListener { presenter.onDownloadGuestClicked() }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    override fun startContactApplication(intent: Intent) {
        requireActivity().startActivity(intent)
    }

    override fun checkPermission() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                KEY_PERMISSION_REQUEST_CODE
            )
        } else {
            presenter.onPermissionResult(true)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == KEY_PERMISSION_REQUEST_CODE) {
            presenter.onPermissionResult(
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            )
        }
    }

    companion object {
        private const val KEY_PERMISSION_REQUEST_CODE = 1001
    }
}