package com.study.online.ui.main.lesson.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.study.online.extension.dpToPx

class LessonPresentersDecorator : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        if (parent.getChildAdapterPosition(view) % 2 == 0) {
            outRect.right = parent.context.dpToPx(CENTRAL_PADDING)
        } else {
            outRect.left = parent.context.dpToPx(CENTRAL_PADDING)
        }

        if (parent.getChildAdapterPosition(view) > 2) {
            outRect.top = parent.context.dpToPx(VERTICAL_PADDING)
        }
    }

    companion object {
        private const val CENTRAL_PADDING = 5f
        private const val VERTICAL_PADDING = 10f
    }
}