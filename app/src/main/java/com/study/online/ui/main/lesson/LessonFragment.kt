package com.study.online.ui.main.lesson

import android.content.pm.PackageManager.*
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.study.online.R
import com.study.online.core.presentation.BaseFragment
import com.study.online.coreui.dialog.LessonEndDialog
import com.study.online.coreui.dialog.OnDialogDismissListener
import com.study.online.coreui.view.BoardView
import com.study.online.domain.entity.TimetableLesson
import com.study.online.domain.entity.socket.lesson.*
import com.study.online.domain.entity.socket.lessonboard.BoardSize
import com.study.online.domain.entity.socket.lessonboard.ShapeData
import com.study.online.extension.*
import com.study.online.presentation.main.lesson.LessonPresenter
import com.study.online.presentation.main.lesson.LessonView
import com.study.online.presentation.main.lesson.OnFrameListener
import com.study.online.ui.main.lesson.adapter.LessonPresentersAdapter
import com.study.online.ui.main.lesson.adapter.LessonPresentersDecorator
import kotlinx.android.synthetic.main.fragment_lesson.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.webrtc.EglBase
import org.webrtc.RendererCommon
import toothpick.Scope
import toothpick.config.Module

class LessonFragment : BaseFragment(), LessonView, OnDialogDismissListener {

    private lateinit var adapter: LessonPresentersAdapter

    override val layoutRes: Int = R.layout.fragment_lesson

    @InjectPresenter
    lateinit var presenter: LessonPresenter

    @ProvidePresenter
    fun providePresenter(): LessonPresenter = scope.getInstance(LessonPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        val timetableLesson = tryToGetSerializable<TimetableLesson>(KEY_TIMETABLE_LESSON)

        scope.installModules(object : Module() {

            init {
                bind(TimetableLesson::class.java)
                    .toInstance(timetableLesson)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().turnScreenOn()

        participantsButton.setOnClickListener { presenter.onParticipantsClicked() }
        chatButton.setOnClickListener { presenter.onChatClicked() }
        lessonLeave.setOnClickListener { presenter.onBackPressed() }
        lessonHandUp.setOnClickListener { presenter.onHandUpClicked() }
        lessonSettings.setOnClickListener { presenter.onSettingsClicked() }

        lessonUserView.viewTreeObserver.addOnGlobalLayoutListener(
            object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    lessonUserView.viewTreeObserver.removeOnGlobalLayoutListener(this)

                    val location = IntArray(2)
                    lessonUserView.getLocationOnScreen(location)
                    val surfaceHeight =
                        view.height - (location[1] / 2) - requireContext().dpToPx(10f)
                    val userParams = lessonUserView.layoutParams
                    userParams.height = (surfaceHeight / 2)
                    lessonUserView.layoutParams = userParams
                    val teacherParams = lessonTeacherView.layoutParams
                    teacherParams.height = (surfaceHeight / 2)
                    lessonTeacherView.layoutParams = teacherParams

                    adapter?.setWidth(view.width)
                    lessonPresentersList.requestLayout()
                }
            }
        )

        adapter = LessonPresentersAdapter()
        lessonPresentersList.let {
            val layoutManager = GridLayoutManager(requireContext(), 2)
            it.addItemDecoration(LessonPresentersDecorator())
            it.layoutManager = layoutManager
            it.adapter = adapter
            it.recycledViewPool.clear()
        }

        lessonUserView.apply {
            init(EglBase.create().eglBaseContext, null)
            setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)
        }
        lessonTeacherView.apply {
            init(EglBase.create().eglBaseContext, null)
            setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)
        }

        lessonBoard.setOnSizeChangedListener(object : BoardView.OnSizeChangedListener {

            override fun onSizeChanged(view: BoardView) {
                presenter.onBoardSizeChanged()
            }
        })
    }

    override fun onResume() {
        super.onResume()

        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun onPause() {
        super.onPause()

        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        lessonTeacherView.release()
        lessonUserView.release()
        requireActivity().turnScreenOff()
    }

    override fun showLessonName(name: String) {
        lessonName.text = name
    }

    override fun showLessonTheme(theme: String) {
        lessonTheme.text = theme
    }

    override fun checkPermissions(permissions: List<String>) {
        val notGranted = permissions.find {
            ActivityCompat.checkSelfPermission(requireContext(), it) != PERMISSION_GRANTED
        }

        presenter.onPermissionsGranted(notGranted.isNullOrEmpty())
    }

    override fun requestPermissions(permissions: List<String>) {
        super.requestPermissions(permissions.toTypedArray(), REQUEST_CODE_LESSON_PERMISSIONS)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE_LESSON_PERMISSIONS) {
            val notGranted = grantResults.find { it != PERMISSION_GRANTED }
            presenter.onPermissionsGranted(notGranted == null)
        }
    }

    override fun setUserFrameListener(listener: OnFrameListener) {
        listener.setTarget(lessonUserView)
        listener.onTargetRemoved = {
            userNoVideo?.invisibleUnless(it)
            userBlackScreen?.invisibleUnless(it)
        }
    }

    override fun setTeacherFrameListener(listener: OnFrameListener) {
        listener.setTarget(lessonTeacherView)
        listener.onTargetRemoved = {
            teacherNoVideo?.invisibleUnless(it)
            teacherBlackScreen?.invisibleUnless(it)
        }
    }

    override fun showPresenters(presenters: List<Pair<OnFrameListener, LessonParticipate>>) {
        adapter?.setItems(presenters)
    }

    override fun showUserStatus(text: String, iconId: Int, colorId: Int) {
        val drawable = ContextCompat.getDrawable(requireContext(), iconId)
        lessonHandUp.text = text
        lessonHandUp.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
        lessonHandUp.setTextColor(ContextCompat.getColor(requireContext(), colorId))
    }

    override fun showTeacherName(name: String) {
        teacherName.text = name
    }

    override fun showUserName(name: String) {
        userName.text = name
    }

    override fun showBoard(isShow: Boolean) {
        lessonBoard.invisibleUnless(isShow)
    }

    override fun setBoardSize(size: BoardSize) {
        lessonBoard.setBoardSize(size.width, size.height)
    }

    override fun drawShapes(shapesData: List<ShapeData>) {
        lessonBoard.drawShapes(shapesData)
    }

    override fun undoShape(shapeData: ShapeData) {
        lessonBoard.undoShape(shapeData)
    }

    override fun cleanBoard() {
        lessonBoard.cleanBoard()
    }

    override fun showLessonEndedDialog() {
        if (childFragmentManager.findFragmentByTag(TAG_LESSON_ENDED_DIALOG) == null) {
            LessonEndDialog()
                .show(childFragmentManager, TAG_LESSON_ENDED_DIALOG)
        }
    }

    override fun onDismiss() {
        presenter.onLessonEndDialogDismissed()
    }

    companion object {
        private const val KEY_TIMETABLE_LESSON = "KEY_TIMETABLE_LESSON"
        private const val TAG_LESSON_ENDED_DIALOG = "TAG_LESSON_ENDED_DIALOG"

        private const val REQUEST_CODE_LESSON_PERMISSIONS = 1000

        fun newInstance(timetableLesson: TimetableLesson) =
            LessonFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(KEY_TIMETABLE_LESSON, timetableLesson)
                }
            }
    }
}