package com.study.online.ui.main.lesson

import android.os.Bundle
import com.study.online.core.featureflow.LessonScreens
import com.study.online.core.presentation.FlowFragment
import com.study.online.domain.entity.TimetableLesson
import com.study.online.extension.setLaunchScreen
import com.study.online.extension.tryToGetSerializable

class LessonFlowFragment : FlowFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            val timetableLesson = tryToGetSerializable<TimetableLesson>(KEY_TIMETABLE_LESSON)
            navigator.setLaunchScreen(LessonScreens.LessonScreen(timetableLesson))
        }
    }

    companion object {
        private const val KEY_TIMETABLE_LESSON = "KEY_TIMETABLE_LESSON"

        fun newInstance(timetableLesson: TimetableLesson) =
            LessonFlowFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(KEY_TIMETABLE_LESSON, timetableLesson)
                }
            }
    }
}