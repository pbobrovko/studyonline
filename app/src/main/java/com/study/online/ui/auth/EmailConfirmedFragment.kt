package com.study.online.ui.auth

import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.study.online.R
import com.study.online.app.di.MaskedEmailLabel
import com.study.online.app.di.PrimitiveWrapper
import com.study.online.app.di.ResendDataLabel
import com.study.online.core.presentation.BaseFragment
import com.study.online.extension.tryToGetBoolean
import com.study.online.extension.tryToGetString
import com.study.online.presentation.auth.emailconfirmed.EmailConfirmedPresenter
import com.study.online.presentation.auth.emailconfirmed.EmailConfirmedView
import kotlinx.android.synthetic.main.fragment_email_confirmed.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope
import toothpick.config.Module

class EmailConfirmedFragment : BaseFragment(), EmailConfirmedView {

    override val layoutRes: Int = R.layout.fragment_email_confirmed

    @InjectPresenter
    lateinit var presenter: EmailConfirmedPresenter

    @ProvidePresenter
    fun providePresenter(): EmailConfirmedPresenter =
        scope.getInstance(EmailConfirmedPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        val maskedEmail = tryToGetString(KEY_MASKED_EMAIL)
        val resendData = tryToGetString(KEY_RESEND_DATA)
        val isRegistration = PrimitiveWrapper(tryToGetBoolean(KEY_IS_REGISTRATION))
        scope.installModules(object : Module() {
            init {
                bind(String::class.java)
                    .withName(MaskedEmailLabel::class.java)
                    .toInstance(maskedEmail)
                bind(String::class.java)
                    .withName(ResendDataLabel::class.java)
                    .toInstance(resendData)
                bind(PrimitiveWrapper::class.java)
                    .toInstance(isRegistration)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initResendMailButton()

        resendMailButton.setOnClickListener { presenter.onResendMailClicked() }
        supportServiceButton.setOnClickListener { presenter.onSupportServiceClicked() }
        emailConfirmedBack.setOnClickListener { presenter.onBackPressed() }
    }

    override fun showMaskedEmail(email: String) {
        confirmedMaskedEmail.text = email
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    private fun initResendMailButton() {
        val span = SpannableString(getString(R.string.resend_mail))
        span.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.blue)),
            12,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        span.setSpan(
            StyleSpan(Typeface.create("sans-serif-medium", Typeface.NORMAL).style),
            12,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        resendMailButton.text = span
    }

    companion object {
        private const val KEY_MASKED_EMAIL = "KEY_MASKED_EMAIL"
        private const val KEY_RESEND_DATA = "KEY_RESEND_DATA"
        private const val KEY_IS_REGISTRATION = "KEY_IS_REGISTRATION"

        fun newInstance(maskedEmail: String, resendData: String, isRegistration: Boolean) =
            EmailConfirmedFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_MASKED_EMAIL, maskedEmail)
                    putString(KEY_RESEND_DATA, resendData)
                    putBoolean(KEY_IS_REGISTRATION, isRegistration)
                }
            }
    }
}