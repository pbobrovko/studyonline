package com.study.online.ui.main.timetable.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.study.online.core.global.DateFormatter
import com.study.online.domain.entity.TimetableDate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_timetable_date.*

class DateViewHolder(view: View) : RecyclerView.ViewHolder(view), LayoutContainer {

    private val dateFormatter = DateFormatter()

    override val containerView: View = itemView

    fun bind(timetableDate: TimetableDate) {

        timetableDateView.text = dateFormatter.formatToTimetableDate(timetableDate.date)
    }
}