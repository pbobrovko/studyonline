package com.study.online.ui.main.lesson.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.study.online.domain.entity.socket.lesson.LessonParticipate
import com.study.online.extension.dpToPx
import com.study.online.extension.getName
import com.study.online.extension.invisibleUnless
import com.study.online.presentation.main.lesson.OnFrameListener
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_lesson_presenter.*

class LessonPresentersViewHolder(
    width: Int,
    item: View
) : RecyclerView.ViewHolder(item), LayoutContainer {

    private val rectangleWidth: Int
    private val squareSize: Int

    override val containerView = item

    init {
        rectangleWidth = width - itemView.context.dpToPx(TWO_MARGINS_DP)
        squareSize = (width - itemView.context.dpToPx(THREE_MARGINS_DP)) / 2
    }

    fun bind(
        onFrameListener: OnFrameListener,
        participate: LessonParticipate,
        position: Int,
        presentersSize: Int
    ) {
        presenterBlackScreen.visibility = View.VISIBLE
        presenterNoVideo.visibility = View.VISIBLE

        handleSurfaceWidth(position, presentersSize)
        onFrameListener.setTarget(presenterSurface)

        presenterName.text = String.getName(
            participate.userData.firstName,
            participate.userData.lastName,
            participate.userData.patronymic
        )

        onFrameListener.onTargetRemoved = {
            presenterNoVideo?.invisibleUnless(it)
            presenterBlackScreen?.invisibleUnless(it)
        }
    }

    private fun handleSurfaceWidth(position: Int, presentersSize: Int) {
        val params = itemView.layoutParams
        if (position + 1 == presentersSize && presentersSize % 2 != 0) {
            params.width = rectangleWidth
            params.height = squareSize
        } else {
            params.width = squareSize
            params.height = squareSize
        }
        itemView.layoutParams = params
    }

    companion object {
        private const val NAME_CHARS_START_POSITION = 0
        private const val NAME_CHARS_END_POSITION = 1

        private const val TWO_MARGINS_DP = 20f
        private const val THREE_MARGINS_DP = 35f
    }
}