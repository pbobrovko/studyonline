package com.study.online.ui.auth

import android.os.Bundle
import com.study.online.core.featureflow.AuthScreens
import com.study.online.core.presentation.FlowFragment
import com.study.online.extension.setLaunchScreen

class AuthFlowFragment : FlowFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            navigator.setLaunchScreen(AuthScreens.OnboardingScreen)
        }
    }
}