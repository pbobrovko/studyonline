package com.study.online.ui.main.timetable.adapter

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import com.study.online.R
import com.study.online.core.global.DateFormatter
import com.study.online.domain.entity.TimetableLesson
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_lesson.*

class LessonViewHolder(
    view: View,
    private val onEnterLessonClickListener: (Int) -> Unit
) : RecyclerView.ViewHolder(view), LayoutContainer {

    // Card colors
    private val seashell = getColor(R.color.seashell)
    private val lightGray = getColor(R.color.light_gray)
    private val regentGray = getColor(R.color.regent_gray)
    private val white = getColor(R.color.white)
    private val radicalRed = getColor(R.color.radical_red)
    private val radicalRedTransparent = getColor(R.color.radical_red_transparent_15)
    private val blueTransparent = getColor(R.color.blue_transparent_15)
    private val blue = getColor(R.color.blue)

    // Card icons
    private val clockGary =
        ContextCompat.getDrawable(itemView.context, R.drawable.ic_clock_gray)
    private val clockRed =
        ContextCompat.getDrawable(itemView.context, R.drawable.ic_clock_radical_red)
    private val clockBlue = ContextCompat.getDrawable(itemView.context, R.drawable.ic_clock_blue)

    private val lessonTimeDrawable =
        ContextCompat.getDrawable(itemView.context, R.drawable.item_timetable_card_time)

    private val lessonStart = itemView.context.getString(R.string.lesson_start)
    private val lessonCancelledTime = itemView.context.getString(R.string.lesson_cancelled_time)
    private val lessonCancelled = itemView.context.getString(R.string.lesson_cancelled)
    private val enterLesson = itemView.context.getString(R.string.enter_to_lesson)

    private val dateFormatter = DateFormatter()

    override val containerView: View = itemView

    init {
        enterLessonButton.setOnClickListener {
            if (adapterPosition != NO_POSITION) {
                onEnterLessonClickListener.invoke(adapterPosition)
            }
        }
    }

    fun bind(lesson: TimetableLesson) {
        val time = dateFormatter.formatToLessonTime(lesson.time)

        when (lesson.status) {
            LessonStatus.CANCELED -> setCancelledState(time)
            LessonStatus.PLANNED -> setPlannedState(time)
            LessonStatus.IN_PROGRESS -> setInProgressState(time)
            LessonStatus.COMPLETED -> setComplectedState(time)
        }

        lessonName.text = lesson.subject
        lessonTopic.text = lesson.topic
    }

    private fun setComplectedState(time: String) {
        lessonLayout.setBackgroundColor(seashell)
        lessonTimeDrawable?.setTint(lightGray)
        lessonTime.background = lessonTimeDrawable
        lessonTime.setTextColor(regentGray)
        lessonTime.setCompoundDrawablesWithIntrinsicBounds(clockGary, null, null, null)
        enterLessonButton.setBackgroundColor(lightGray)
        enterLessonButton.setTextColor(regentGray)
        enterLessonButton.isEnabled = false
        enterLessonButton.icon = null
        enterLessonButton.text = enterLesson
        lessonTime.text = time
    }

    private fun setInProgressState(time: String) {
        lessonLayout.setBackgroundColor(blueTransparent)
        lessonTimeDrawable?.setTint(radicalRedTransparent)
        lessonTime.background = lessonTimeDrawable
        lessonTime.setTextColor(radicalRed)
        lessonTime.setCompoundDrawablesWithIntrinsicBounds(clockRed, null, null, null)
        enterLessonButton.setBackgroundColor(blue)
        enterLessonButton.setTextColor(white)
        enterLessonButton.isEnabled = true
        enterLessonButton.icon = null
        enterLessonButton.text = enterLesson
        lessonTime.text = String.format(lessonStart, time)
    }

    private fun setPlannedState(time: String) {
        lessonLayout.setBackgroundColor(white)
        lessonTimeDrawable?.setTint(blueTransparent)
        lessonTime.background = lessonTimeDrawable
        lessonTime.setTextColor(blue)
        lessonTime.setCompoundDrawablesWithIntrinsicBounds(clockBlue, null, null, null)
        enterLessonButton.setBackgroundColor(lightGray)
        enterLessonButton.setTextColor(regentGray)
        enterLessonButton.isEnabled = false
        enterLessonButton.icon = null
        enterLessonButton.text = enterLesson
        lessonTime.text = time
    }

    private fun setCancelledState(time: String) {
        lessonLayout.setBackgroundColor(radicalRedTransparent)
        lessonTimeDrawable?.setTint(radicalRedTransparent)
        lessonTime.background = lessonTimeDrawable
        lessonTime.setTextColor(radicalRed)
        lessonTime.setCompoundDrawablesWithIntrinsicBounds(clockRed, null, null, null)
        enterLessonButton.setBackgroundColor(radicalRedTransparent)
        enterLessonButton.setTextColor(radicalRed)
        enterLessonButton.isEnabled = false
        enterLessonButton.icon = ContextCompat.getDrawable(itemView.context, R.drawable.ic_close)
        enterLessonButton.text = lessonCancelled
        lessonTime.text = String.format(lessonCancelledTime, time)
    }

    private fun getColor(colorId: Int): Int = ContextCompat.getColor(itemView.context, colorId)
}