package com.study.online.ui.main.lesson.participants

import com.study.online.R
import com.study.online.core.presentation.BaseFragment
import com.study.online.presentation.main.lesson.participates.ParticipatesPresenter
import com.study.online.presentation.main.lesson.participates.ParticipatesView
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope

class ParticipantsFragment : BaseFragment(), ParticipatesView {

    override val layoutRes: Int = R.layout.fragment_lesson_participants

    @InjectPresenter
    lateinit var presenter: ParticipatesPresenter

    @ProvidePresenter
    fun providePresenter(): ParticipatesPresenter =
        scope.getInstance(ParticipatesPresenter::class.java)

    override fun installScopeModules(scope: Scope) {}

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }
}