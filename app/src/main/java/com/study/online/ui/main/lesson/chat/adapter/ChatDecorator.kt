package com.study.online.ui.main.lesson.chat.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.study.online.extension.dpToPx

class ChatDecorator(
    private val currentUserName: String
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        parent.adapter?.let {
            if (it is ChatAdapter) {
                val position = parent.getChildAdapterPosition(view)
                val message = it.getItem(position)

                val startPaddingPix: Int
                val endPaddingPix: Int
                if (message.userName == currentUserName) {
                    startPaddingPix = parent.context.dpToPx(HORIZONTAL_PADDING_34DP)
                    endPaddingPix = parent.context.dpToPx(HORIZONTAL_PADDING_24DP)
                } else {
                    startPaddingPix = parent.context.dpToPx(HORIZONTAL_PADDING_24DP)
                    endPaddingPix = parent.context.dpToPx(HORIZONTAL_PADDING_34DP)
                }

                val verticalPaddingPix = parent.context.dpToPx(VERTICAL_PADDING)
                outRect.left = startPaddingPix
                outRect.right = endPaddingPix
                outRect.top = verticalPaddingPix
                outRect.bottom = verticalPaddingPix
            }
        }
    }

    companion object {
        private const val HORIZONTAL_PADDING_34DP = 34F
        private const val HORIZONTAL_PADDING_24DP = 24F
        private const val VERTICAL_PADDING = 10f
    }
}