package com.study.online.ui.main.choseschool

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import com.study.online.R
import com.study.online.app.di.DI.SERVER_SCOPE
import com.study.online.core.presentation.FlowFragment
import com.study.online.domain.entity.School
import com.study.online.extension.tryToGetArrayList
import com.study.online.presentation.main.choseschool.ChooseSchoolPresenter
import com.study.online.presentation.main.choseschool.ChoseSchoolView
import kotlinx.android.synthetic.main.fragment_chose_school.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope
import toothpick.config.Module

class ChoseSchoolFragment : FlowFragment(), ChoseSchoolView {

    override val layoutRes: Int = R.layout.fragment_chose_school
    override val parentFragmentScopeName: String = SERVER_SCOPE

    @InjectPresenter
    lateinit var presenter: ChooseSchoolPresenter

    @ProvidePresenter
    fun providePresenter(): ChooseSchoolPresenter =
        scope.getInstance(ChooseSchoolPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        super.installScopeModules(scope)

        val schools = tryToGetArrayList<School>(KEY_SCHOOLS)

        scope.installModules(object : Module() {

            init {
                bind(List::class.java)
                    .toInstance(schools)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        schoolMenu.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(
                schoolName: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                presenter.onSchoolChanged(schoolName.toString())
            }
        })

        choseSchoolButton.setOnClickListener { presenter.onChoseClicked() }
        choseSchoolBack.setOnClickListener { presenter.onBackPressed() }
        supportButton.setOnClickListener { presenter.onSupportClicked() }
    }

    override fun setSchools(schoolNames: List<String>) {
        val adapter = ArrayAdapter(requireContext(), R.layout.item_school, schoolNames)
        schoolMenu.setAdapter(adapter)
    }

    override fun showSelectedSchool(schoolName: String) {
        schoolMenu.setText(schoolName)
    }

    override fun showSchoolError(message: String) {
        schoolMenuLayout.error = message
        schoolMenuLayout.isErrorEnabled = message.isNotEmpty()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    companion object {
        private const val KEY_SCHOOLS = "KEY_SCHOOLS"

        fun newInstance(schools: ArrayList<School>) =
            ChoseSchoolFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(KEY_SCHOOLS, schools)
                }
            }
    }
}