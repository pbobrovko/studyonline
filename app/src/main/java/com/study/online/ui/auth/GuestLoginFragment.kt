package com.study.online.ui.auth

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.study.online.core.presentation.BaseFragment
import toothpick.Scope
import com.study.online.R
import com.study.online.app.di.DI
import com.study.online.coreui.dialog.LessonEndDialog
import com.study.online.coreui.dialog.LessonOverflowDialog
import com.study.online.extension.invisibleUnless
import com.study.online.extension.tryToGetString
import com.study.online.presentation.auth.guestlogin.GuestLoginPresenter
import com.study.online.presentation.auth.guestlogin.GuestLoginView
import kotlinx.android.synthetic.main.fragment_join_guest_lesson.*
import kotlinx.android.synthetic.main.fragment_join_guest_lesson.supportButton
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.config.Module

class GuestLoginFragment : BaseFragment(), GuestLoginView {

    override val layoutRes: Int = R.layout.fragment_join_guest_lesson
    override val parentFragmentScopeName: String = DI.SERVER_SCOPE

    @InjectPresenter
    lateinit var presenter: GuestLoginPresenter

    @ProvidePresenter
    fun providePresenter(): GuestLoginPresenter = scope.getInstance(GuestLoginPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        val inviteLink = tryToGetString(KEY_LESSON_INVITE_LINK)
        scope.installModules(
            object : Module() {

                init {
                    bind(String::class.java)
                        .toInstance(inviteLink)
                }
            }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.let {
            guestLinkInput.setText(it.getString(KEY_LESSON_INVITE_LINK))
            secondNameInput.setText(it.getString(KEY_LAST_NAME))
            firstNameInput.setText(it.getString(KEY_FIRST_NAME))
            thirdNameInput.setText(it.getString(KEY_PATRONYMIC))
        }

        initPrivacyPolicyWithTermsAndConditionsTextView()
        supportButton.setOnClickListener { presenter.onTechSupportClicked() }
        enterLessonButton.setOnClickListener {
            presenter.onRegisterGuestClicked(
                guestLinkInput.text.toString(),
                secondNameInput.text.toString(),
                firstNameInput.text.toString(),
                thirdNameInput.text.toString()
            )
        }
        guestBack.setOnClickListener { presenter.onBackPressed() }
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress(isShow: Boolean) {
        guestProgress.invisibleUnless(isShow)
    }

    override fun showInviteLink(link: String) {
        guestLinkInput.setText(link)
    }

    override fun showFirstNameError(message: String) {
        firstNameLayout.error = message
        firstNameLayout.isErrorEnabled = message.isNotEmpty()
    }

    override fun showLastNameError(message: String) {
        secondNameLayout.error = message
        secondNameLayout.isErrorEnabled = message.isNotEmpty()
    }

    override fun showPatronymicError(message: String) {
        thirdNameLayout.error = message
        thirdNameLayout.isErrorEnabled = message.isNotEmpty()
    }

    override fun showInviteLinkError(message: String) {
        guestLinkLayout.error = message
        guestLinkLayout.isErrorEnabled = message.isNotEmpty()
    }

    override fun showLessonOverflowDialog() {
        // TODO: Can be merged with error dialog
        if (childFragmentManager.findFragmentByTag(TAG_LESSON_OVERFLOW_DIALOG) == null) {
            LessonOverflowDialog()
                .show(childFragmentManager, TAG_LESSON_OVERFLOW_DIALOG)
        }
    }

    override fun showLessonEndedDialog() {
        if (childFragmentManager.findFragmentByTag(TAG_LESSON_ENDED_DIALOG) == null) {
            LessonEndDialog()
                .show(childFragmentManager, TAG_LESSON_ENDED_DIALOG)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.apply {
            putString(KEY_LESSON_INVITE_LINK, guestLinkInput.text.toString())
            putString(KEY_LAST_NAME, secondNameInput.text.toString())
            putString(KEY_FIRST_NAME, firstNameInput.text.toString())
            putString(KEY_PATRONYMIC, thirdNameInput.text.toString())
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    private fun initPrivacyPolicyWithTermsAndConditionsTextView() {
        val span =
            SpannableString(getString(R.string.guest_privacy_policy_and_terms_and_conditions))

        span.setSpan(
            object : ClickableSpan() {
                override fun onClick(view: View) {
                    presenter.onPrivacyPolicyClicked()
                }
            },
            0,
            SPAN_LENGTH_PRIVACY_POLICY_STRING,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        span.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.blue)),
            0,
            SPAN_LENGTH_PRIVACY_POLICY_STRING,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        span.setSpan(
            object : ClickableSpan() {
                override fun onClick(view: View) {
                    presenter.onTermsAndConditionsClicked()
                }
            },
            SPAN_START_GUEST_TERMS_AND_CONDITIONS,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        span.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.blue)),
            SPAN_START_GUEST_TERMS_AND_CONDITIONS,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        guestPrivacyPolicyWithTermsAndConditions.movementMethod = LinkMovementMethod.getInstance()
        guestPrivacyPolicyWithTermsAndConditions.text = span
    }

    companion object {
        private const val KEY_LESSON_INVITE_LINK = "KEY_LESSON_INVITE_LINK"
        private const val KEY_FIRST_NAME = "KEY_FIRST_NAME"
        private const val KEY_LAST_NAME = "KEY_LAST_NAME"
        private const val KEY_PATRONYMIC = "KEY_PATRONYMIC"
        private const val TAG_LESSON_OVERFLOW_DIALOG = "TAG_LESSON_OVERFLOW_DIALOG"
        private const val TAG_LESSON_ENDED_DIALOG = "TAG_LESSON_ENDED_DIALOG"
        private const val SPAN_LENGTH_PRIVACY_POLICY_STRING = 27
        private const val SPAN_START_GUEST_TERMS_AND_CONDITIONS = 30

        fun newInstance(lessonInviteLink: String): GuestLoginFragment {
            return GuestLoginFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_LESSON_INVITE_LINK, lessonInviteLink)
                }
            }
        }
    }
}