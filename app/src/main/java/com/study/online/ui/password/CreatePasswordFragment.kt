package com.study.online.ui.password

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.study.online.core.presentation.BaseFragment
import com.study.online.presentation.password.CreatePasswordView
import toothpick.Scope
import com.study.online.R
import com.study.online.app.di.DI
import com.study.online.domain.entity.CreatePasswordInfo
import com.study.online.extension.getColor
import com.study.online.extension.invisibleUnless
import com.study.online.extension.tryToGetParcelable
import com.study.online.extension.tryToGetString
import com.study.online.presentation.password.CreatePasswordPresenter
import kotlinx.android.synthetic.main.fragment_create_password.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.config.Module

class CreatePasswordFragment : BaseFragment(), CreatePasswordView {

    override val layoutRes: Int = R.layout.fragment_create_password
    override val parentFragmentScopeName: String = DI.SERVER_SCOPE

    @InjectPresenter
    lateinit var presenter: CreatePasswordPresenter

    @ProvidePresenter
    fun providePresenter(): CreatePasswordPresenter =
        scope.getInstance(CreatePasswordPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        val createPasswordInfo = tryToGetParcelable<CreatePasswordInfo>(KEY_CREATE_PASSWORD_INFO)
        val emailCode = tryToGetString(KEY_EMAIL_CODE)

        scope.installModules(object : Module() {
            init {
                bind(CreatePasswordInfo::class.java)
                    .toInstance(createPasswordInfo)
                bind(String::class.java)
                    .toInstance(emailCode)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        createPasswordBack.setOnClickListener { presenter.onBackPressed() }
        confirmButton.setOnClickListener {
            presenter.onConfirmClicked(
                passwordInput.text.toString(),
                confirmPasswordInput.text.toString()
            )
        }

        passwordInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
                presenter.onFieldsChanged(text.toString())
            }
        })
    }

    override fun showUserEmail(email: String) {
        createPasswordMessage.text =
            String.format(getString(R.string.create_password_message), email)
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun enableConfirmButton(isEnable: Boolean) {
        confirmButton.isEnabled = isEnable
    }

    override fun showPasswordError(message: String) {
        passwordLayout.error = message
        passwordLayout.isErrorEnabled = message.isNotEmpty()
    }

    override fun showConfirmPasswordError(message: String) {
        confirmPasswordLayout.error = message
        confirmPasswordLayout.isErrorEnabled = message.isNotEmpty()
    }

    override fun showProgress(isShow: Boolean) {
        createPasswordProgress.invisibleUnless(isShow)
    }

    override fun showBrokenPhoneNumberMessage() {
        createPasswordMessage.text = getString(R.string.broken_phone_number)
        createPasswordMessage.setTextColor(getColor(R.color.red))
    }

    companion object {
        private const val KEY_CREATE_PASSWORD_INFO = "KEY_CREATE_PASSWORD_INFO"
        private const val KEY_EMAIL_CODE = "KEY_EMAIL_CODE"

        fun newInstance(createPasswordInfo: CreatePasswordInfo, emailCode: String) =
            CreatePasswordFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(KEY_CREATE_PASSWORD_INFO, createPasswordInfo)
                    putString(KEY_EMAIL_CODE, emailCode)
                }
            }
    }
}