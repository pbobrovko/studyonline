package com.study.online.ui.support

import android.os.Bundle
import android.view.View
import com.github.barteksc.pdfviewer.link.DefaultLinkHandler
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import com.study.online.R
import com.study.online.core.presentation.BaseFragment
import com.study.online.domain.entity.socket.Roles
import com.study.online.extension.tryToGetString
import com.study.online.presentation.support.pdfviewer.PdfViewerPresenter
import com.study.online.presentation.support.pdfviewer.PdfViewerView
import kotlinx.android.synthetic.main.fragment_pdf_viewer.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope
import toothpick.config.Module
import java.io.File

class PdfViewerFragment : BaseFragment(), PdfViewerView {

    override val layoutRes = R.layout.fragment_pdf_viewer

    private var currentPage = 0

    @InjectPresenter
    lateinit var presenter: PdfViewerPresenter

    @ProvidePresenter
    fun providePresenter(): PdfViewerPresenter = scope.getInstance(PdfViewerPresenter::class.java)

    override fun installScopeModules(scope: Scope) {
        val role = Roles.valueOf(tryToGetString(KEY_ROLE))

        scope.installModules(object : Module() {
            init {
                bind(Roles::class.java)
                    .toInstance(role)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.let { currentPage = savedInstanceState.getInt(KEY_CURRENT_PAGE) }
    }

    override fun showPdf(file: File) {
        pdfView.fromFile(file)
            .defaultPage(currentPage)
            .enableAnnotationRendering(true)
            .enableDoubletap(true)
            .enableSwipe(true)
            .swipeHorizontal(false)
            .scrollHandle(DefaultScrollHandle(requireContext()))
            .linkHandler(DefaultLinkHandler(pdfView))
            .onPageChange { page, _ -> currentPage = page }
            .load()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(KEY_CURRENT_PAGE, currentPage)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    companion object {

        private const val KEY_ROLE = "KEY_ROLE"
        private const val KEY_CURRENT_PAGE = "KEY_CURRENT_PAGE"

        fun newInstance(role: Roles) =
            PdfViewerFragment().apply {
                arguments = Bundle().apply { putString(KEY_ROLE, role.name) }
            }
    }
}