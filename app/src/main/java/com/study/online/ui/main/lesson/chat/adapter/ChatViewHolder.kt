package com.study.online.ui.main.lesson.chat.adapter

import android.graphics.drawable.Drawable
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.study.online.R
import com.study.online.core.global.DateFormatter
import com.study.online.domain.entity.socket.lesson.ChatMessage
import com.study.online.extension.getName
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_lesson_chat_message.*

class ChatViewHolder(
    private val userName: String,
    item: View
) : RecyclerView.ViewHolder(item), LayoutContainer {

    private val context = item.context
    private val lessThanMinute = context.getString(R.string.less_than_minute)
    private val messageDrawableBlue =
        ContextCompat.getDrawable(context, R.drawable.chat_message_background_blue)
    private val messageDrawableGray =
        ContextCompat.getDrawable(context, R.drawable.chat_message_background_gray)
    private val dateFormatter = DateFormatter()

    override val containerView = item

    fun bind(message: ChatMessage) {
        val isMyMessage = isMyMessage(message.userName)
        lessonChatMessage.text = message.text

        val userFullName = message.userFullName
        lessonChatSender.text = String.getName(
            userFullName.firstName ?: "",
            userFullName.lastName ?: "",
            userFullName.patronymic ?: ""
        )
        val visibility = if (isMyMessage) {
            View.INVISIBLE
        } else {
            View.VISIBLE
        }
        lessonChatSender.visibility = visibility

        lessonChatTime.text = if (dateFormatter.isLessThanMinute(message.time)) {
            lessThanMinute
        } else {
            dateFormatter.formatToLessonTime(message.time)
        }
        handleMessageType(isMyMessage)
    }

    private fun handleMessageType(isMyMessage: Boolean) {
        val backgroundDrawable: Drawable?
        val gravity: Int

        if (isMyMessage) {
            backgroundDrawable = messageDrawableGray
            gravity = Gravity.END
        } else {
            backgroundDrawable = messageDrawableBlue
            gravity = Gravity.START
        }

        lessonChatMessage.background = backgroundDrawable
        val messageParams = lessonChatMessage.layoutParams as LinearLayout.LayoutParams
        messageParams.gravity = gravity
        lessonChatMessage.layoutParams = messageParams
        messageDataLayout.setHorizontalGravity(gravity)
    }

    private fun isMyMessage(currentUserName: String) = userName == currentUserName
}