package com.study.online.ui.auth

import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.study.online.R
import com.study.online.core.presentation.BaseFragment
import com.study.online.presentation.auth.signup.SignUpPresenter
import com.study.online.presentation.auth.signup.SignUpView
import kotlinx.android.synthetic.main.fragment_sign_up.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope

class SignUpFragment : BaseFragment(), SignUpView {

    override val layoutRes: Int = R.layout.fragment_sign_up

    @InjectPresenter
    lateinit var presenter: SignUpPresenter

    @ProvidePresenter
    fun providePresenter(): SignUpPresenter = scope.getInstance(SignUpPresenter::class.java)

    override fun installScopeModules(scope: Scope) {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToLoginButton()

        getLinkButton.setOnClickListener { presenter.onGetLinkClicked(emailInput.text.toString()) }
        toLoginButton.setOnClickListener { presenter.onGoToLoginClicked() }
        supportButton.setOnClickListener { presenter.onSupportClicked() }
        signUpBack.setOnClickListener { presenter.onBackPressed() }

        emailInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
                presenter.onEmailChanged(text.toString())
            }
        })
    }

    override fun enableLoginButton(isEnable: Boolean) {
        getLinkButton.isEnabled = isEnable
    }

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun showEmailNotFoundError(message: String) {
        emailLayout.error = message
        emailLayout.isErrorEnabled = true
    }

    override fun onBackPressed() {
        super.onBackPressed()

        presenter.onBackPressed()
    }

    private fun initToLoginButton() {
        val span = SpannableString(getString(R.string.go_to_login))
        span.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.blue)),
            14,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        span.setSpan(
            StyleSpan(Typeface.create("sans-serif-medium", Typeface.NORMAL).style),
            14,
            span.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        toLoginButton.text = span
    }
}