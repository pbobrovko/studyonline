package com.study.online.core.featureflow

import androidx.fragment.app.Fragment
import com.study.online.ui.auth.*
import ru.terrakok.cicerone.android.support.SupportAppScreen

object AuthScreens {

    object OnboardingScreen : SupportAppScreen() {

        override fun getFragment(): Fragment = OnboardingFragment()
    }

    object SignUpScreen : SupportAppScreen() {

        override fun getFragment(): Fragment =
            SignUpFragment()
    }

    object LoginScreen : SupportAppScreen() {

        override fun getFragment(): Fragment = LoginFragment()
    }

    data class AuthActionConfirmedScreen(
        private val maskedEmail: String,
        private val resendData: String,
        private val isRegistration: Boolean
    ) : SupportAppScreen() {

        override fun getFragment(): Fragment =
            EmailConfirmedFragment.newInstance(maskedEmail, resendData, isRegistration)
    }

    object ResetPasswordScreen : SupportAppScreen() {

        override fun getFragment() = ResetPasswordFragment()
    }

    data class GuestLoginScreen(
        private val lessonInviteLink: String
    ) : SupportAppScreen() {

        override fun getFragment() = GuestLoginFragment.newInstance(lessonInviteLink)
    }
}