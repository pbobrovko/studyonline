package com.study.online.core.global.deserializers

import com.google.gson.*
import com.study.online.core.global.FileLogger
import com.study.online.domain.entity.socket.lessonboard.*
import java.lang.reflect.Type

class LessonBoardDeserializer(
    private val logger: FileLogger
) : JsonDeserializer<GeneralBoardDTO<*>> {

    private val jsonParser = JsonParser()

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): GeneralBoardDTO<*> {
        val jsonObject = json.asJsonObject
        val type = BoardDataTypes.values().find { it.toString() == jsonObject.get("id").asString }

        return when (type) {
            BoardDataTypes.JOIN_BOARD -> deserializeBoardShapes(jsonObject, type)
            BoardDataTypes.BOARD_STATE -> deserializeBoardState(jsonObject, type)
            BoardDataTypes.ACCESS_STATE -> deserializeAccessState(jsonObject, type)
            BoardDataTypes.ACCESS_TYPE -> deserializeAccessType(jsonObject, type)
            BoardDataTypes.CLOSE_BOARD,
            BoardDataTypes.OPEN_BOARD -> deserializeOpenCloseBoard(jsonObject, type)
            BoardDataTypes.ADD_CHANGE -> deserializeAddChange(jsonObject, type)
            BoardDataTypes.UNDO_LAST_CHANGE,
            BoardDataTypes.REDO_LAST_CHANGE -> deserializeUndoRedo(jsonObject, type)
            BoardDataTypes.CLEAN_BOARD -> GeneralBoardDTO(type, null)
            else -> {
                logger.addLog("LessonBoardDeserializer unknown json type, json: $json")
                throw JsonParseException(jsonObject.asString)
            }
        }
    }

    private fun deserializeBoardState(
        json: JsonObject,
        id: BoardDataTypes
    ): GeneralBoardDTO<BoardState> {
        val data = getData(json)
        val actual = data.get("actual").asBoolean
        val isSizeAvailable = !data.get("size").isJsonNull
        val boardSize = if (isSizeAvailable) {
            val sizeString = data.get("size").asString
            val sizeObject = jsonParser.parse(sizeString).asJsonObject
            val width = sizeObject.get("width").asInt
            val height = sizeObject.get("height").asInt
            BoardSize(width, height)
        } else {
            null
        }
        val boardState = BoardState(actual, boardSize)
        return GeneralBoardDTO(id, boardState)
    }

    private fun deserializeBoardShapes(
        json: JsonObject,
        id: BoardDataTypes
    ): GeneralBoardDTO<List<ShapeData>> {
        val jsonArray = json.getAsJsonArray("data")
        val boardShapes = mutableListOf<ShapeData>()

        jsonArray.forEach { boardShape ->
            val jsonObject = boardShape.asJsonObject
            boardShapes.add(getShapeData(jsonObject))
        }
        return GeneralBoardDTO(id, boardShapes)
    }

    private fun deserializeAccessState(
        json: JsonObject,
        id: BoardDataTypes
    ): GeneralBoardDTO<Boolean> {
        val data = json.get("data").asBoolean
        return GeneralBoardDTO(id, data)
    }

    private fun deserializeAccessType(
        jsonObject: JsonObject,
        id: BoardDataTypes
    ): GeneralBoardDTO<AccessTypes> {
        val data = AccessTypes.values().find { it.toString() == jsonObject.get("data").asString }
        return GeneralBoardDTO(id, data)
    }

    private fun deserializeOpenCloseBoard(
        jsonObject: JsonObject,
        type: BoardDataTypes
    ): GeneralBoardDTO<BoardSize> {
        val boardSize = if (jsonObject.get("data").isJsonNull) {
            null
        } else {
            val dataString = jsonObject.get("data").asString
            val dataObject = jsonParser.parse(dataString).asJsonObject
            val width = dataObject.get("width").asInt
            val height = dataObject.get("height").asInt
            BoardSize(width, height)
        }
        return GeneralBoardDTO(type, boardSize)
    }

    private fun deserializeAddChange(
        jsonObject: JsonObject,
        type: BoardDataTypes
    ) = GeneralBoardDTO(type, getShapeData(getData(jsonObject)))

    private fun deserializeUndoRedo(
        jsonObject: JsonObject,
        type: BoardDataTypes
    ) = GeneralBoardDTO(type, getChangeId(getData(jsonObject)))

    private fun getData(json: JsonObject): JsonObject {
        return json.get("data").asJsonObject
    }

    private fun getShapeData(jsonObject: JsonObject): ShapeData {
        val changeIdObject = jsonObject.get("changeId").asJsonObject
        val changeId = getChangeId(changeIdObject)
        val updateTime = jsonObject.get("updateTime").asLong
        val actual = jsonObject.get("actual").asBoolean
        val shapeStr = jsonObject.get("shape").asString
        val shapeObject = jsonParser.parse(shapeStr).asJsonObject
        val shape = getShape(shapeObject)

        return ShapeData(changeId, updateTime, actual, shape)
    }

    private fun getShape(jsonObject: JsonObject): Shape {
        val color = jsonObject.get("color").asString
        val thickness = jsonObject.get("thickness").asInt
        val shapeActual = jsonObject.get("actual").asBoolean
        val coordinatesArray = jsonObject.get("data").asJsonArray
        val type = jsonObject.get("type").asString
        val changeIdObject = jsonObject.get("changeID").asJsonObject
        val changeId = getChangeId(changeIdObject)
        val coordinates = mutableListOf<Line>()

        coordinatesArray.forEach {
            coordinates.add(getLine(it.asJsonObject))
        }

        return Shape(color, thickness, coordinates, shapeActual, type, changeId)
    }

    private fun getLine(jsonObject: JsonObject): Line {
        val from = jsonObject.get("from").asJsonObject
        val to = jsonObject.get("to").asJsonObject

        return Line(getPosition(from), getPosition(to))
    }

    private fun getPosition(jsonObject: JsonObject): Position {
        val x = jsonObject.get("x").asFloat
        val y = jsonObject.get("y").asFloat

        return Position(x, y)
    }

    private fun getChangeId(jsonObject: JsonObject): ChangeId {
        val changeNumber = jsonObject.get("changeNumber").asInt
        val userName = jsonObject.get("username").asString

        return ChangeId(userName, changeNumber)
    }
}