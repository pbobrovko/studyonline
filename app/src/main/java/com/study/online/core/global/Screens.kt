package com.study.online.core.global

import com.study.online.domain.entity.CreatePasswordInfo
import com.study.online.domain.entity.TimetableLesson
import com.study.online.ui.auth.AuthFlowFragment
import com.study.online.ui.main.MainFlowFragment
import com.study.online.ui.main.lesson.LessonFlowFragment
import com.study.online.ui.password.CreatePasswordFragment
import com.study.online.ui.privacypolicy.PrivacyPolicyFlowFragment
import com.study.online.ui.support.SupportFlowFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    object AuthFlow : SupportAppScreen() {

        override fun getFragment() = AuthFlowFragment()
    }

    object SupportFlow : SupportAppScreen() {

        override fun getFragment() = SupportFlowFragment()
    }

    data class CreatePasswordScreen(
        val createPasswordInfo: CreatePasswordInfo,
        val emailCode: String
    ) : SupportAppScreen() {

        override fun getFragment() =
            CreatePasswordFragment.newInstance(createPasswordInfo, emailCode)
    }

    object MainFlow : SupportAppScreen() {

        override fun getFragment() = MainFlowFragment()
    }

    data class LessonFlow(
        private val timetableLesson: TimetableLesson
    ) : SupportAppScreen() {

        override fun getFragment() = LessonFlowFragment.newInstance(timetableLesson)
    }

    data class PrivacyPolicyFlow(
        private val isFromRegistration: Boolean,
        private val isTermsAndConditions: Boolean
    ) : SupportAppScreen() {

        override fun getFragment() =
            PrivacyPolicyFlowFragment.newInstance(isFromRegistration, isTermsAndConditions)
    }
}