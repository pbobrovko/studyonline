package com.study.online.core.global

import android.text.format.DateUtils
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DateFormatter @Inject constructor() {

    private val inputFormat = SimpleDateFormat(FULL_DATE_PATTERN, Locale.getDefault())

    init {
        inputFormat.timeZone = TimeZone.getTimeZone(GMT)
    }

    fun formatToTimetableDate(timetableDate: String): String {
        val date: Date? = stringToDate(timetableDate)
        val outputFormat = SimpleDateFormat(TIMETABLE_DATE_PATTERN, Locale.getDefault())

        return outputFormat.format(date) ?: ""
    }

    fun formatToLessonTime(dateString: String): String {
        val date: Date? = stringToDate(dateString)
        val outputFormat = SimpleDateFormat(HOURS_MINUTES_PATTERN, Locale.getDefault())

        return outputFormat.format(date) ?: ""
    }

    fun isYesterday(dateString: String): Boolean {
        val date = stringToDate(dateString)
        val timeMillis = (date?.time ?: 0) + DateUtils.DAY_IN_MILLIS
        return DateUtils.isToday(timeMillis)
    }

    fun isToday(dateString: String): Boolean {
        val date = stringToDate(dateString)

        return DateUtils.isToday(date?.time ?: 0)
    }

    fun isTomorrow(dateString: String): Boolean {
        val date = stringToDate(dateString)
        val timeMillis = (date?.time ?: 0) - DateUtils.DAY_IN_MILLIS
        return DateUtils.isToday(timeMillis)
    }

    fun isLessThanMinute(dateString: String): Boolean {
        val date = stringToDate(dateString)
        val dateMillis = date?.time ?: 0
        val currentMillis = System.currentTimeMillis()
        val difference = currentMillis - dateMillis

        return difference < DateUtils.MINUTE_IN_MILLIS
    }

    private fun stringToDate(date: String): Date? {
        return inputFormat.parse(date, ParsePosition(0))
    }

    companion object {
        private const val FULL_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        private const val TIMETABLE_DATE_PATTERN = "EEEE',' dd MMMM"
        private const val HOURS_MINUTES_PATTERN = "HH:mm"
        private const val GMT = "GMT"
    }
}