package com.study.online.core.featureflow

import com.study.online.domain.entity.TimetableLesson
import com.study.online.ui.main.lesson.LessonFragment
import com.study.online.ui.main.lesson.chat.ChatFragment
import com.study.online.ui.main.lesson.participants.ParticipantsFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object LessonScreens {

    data class LessonScreen(
        private val timetableLesson: TimetableLesson
    ) : SupportAppScreen() {

        override fun getFragment() = LessonFragment.newInstance(timetableLesson)
    }

    data class LessonChatScreen(
        private val timetableLesson: TimetableLesson
    ) : SupportAppScreen() {

        override fun getFragment() = ChatFragment.newInstance(timetableLesson)
    }

    object LessonParticipatesScreen : SupportAppScreen() {

        override fun getFragment() = ParticipantsFragment()
    }
}