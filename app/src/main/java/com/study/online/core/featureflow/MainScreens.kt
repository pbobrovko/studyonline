package com.study.online.core.featureflow

import com.study.online.domain.entity.School
import com.study.online.ui.main.choseschool.ChoseSchoolFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object MainScreens {

    data class ChoseSchoolScreen(
        private val schools: List<School>
    ) : SupportAppScreen() {

        override fun getFragment() = ChoseSchoolFragment.newInstance(schools as ArrayList<School>)
    }
}