package com.study.online.core.global

import com.study.online.BuildConfig
import com.orhanobut.logger.CsvFormatStrategy
import com.orhanobut.logger.DiskLogAdapter
import com.orhanobut.logger.FormatStrategy
import com.orhanobut.logger.Logger
import javax.inject.Inject

class FileLogger @Inject constructor() {

    init {
        createLogger()
    }

    fun addLog(log: String) {
        if (BuildConfig.DEBUG) {
            Logger.d(log, log)
        }
    }

    private fun createLogger() {
        if (BuildConfig.DEBUG) {
            val formatStrategy: FormatStrategy =
                CsvFormatStrategy
                    .newBuilder()
                    .tag(TAG_LOGGER_DEBUG)
                    .build()

            Logger.addLogAdapter(DiskLogAdapter(formatStrategy))
        }
    }

    companion object {
        private const val TAG_LOGGER_DEBUG = "LOGGER_DEBUG"
    }
}