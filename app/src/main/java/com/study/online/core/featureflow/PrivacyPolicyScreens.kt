package com.study.online.core.featureflow

import com.study.online.ui.privacypolicy.PrivacyPolicyFragment
import com.study.online.ui.privacypolicy.TermsAndConditionsFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object PrivacyPolicyScreens {

    data class PrivacyPolicyScreen(
        private val isFromRegistration: Boolean
    ) : SupportAppScreen() {

        override fun getFragment() = PrivacyPolicyFragment.newInstance(isFromRegistration)
    }

    data class TermsAndConditionsScreen(
        private val isFromRegistration: Boolean
    ) : SupportAppScreen() {

        override fun getFragment() = TermsAndConditionsFragment.newInstance(isFromRegistration)
    }
}