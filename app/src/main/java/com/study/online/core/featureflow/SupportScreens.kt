package com.study.online.core.featureflow

import com.study.online.domain.entity.socket.Roles
import com.study.online.ui.support.PdfViewerFragment
import com.study.online.ui.support.SupportFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object SupportScreens {

    object SupportScreen : SupportAppScreen() {

        override fun getFragment() = SupportFragment()
    }

    data class PdfViewerScreen(
        private val role: Roles
    ) : SupportAppScreen() {

        override fun getFragment() = PdfViewerFragment.newInstance(role)
    }
}