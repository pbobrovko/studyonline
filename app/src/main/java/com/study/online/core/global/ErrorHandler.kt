package com.study.online.core.global

import com.study.online.R
import com.study.online.app.di.ExpirySessionLabel
import com.study.online.domain.exceptions.InvalidTokenException
import com.study.online.domain.exceptions.InvalidUserDataException
import com.study.online.coreui.dialog.errordialog.ErrorDialogType
import io.reactivex.subjects.PublishSubject
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class ErrorHandler @Inject constructor(
    private val resourceManager: ResourceManager,
    @ExpirySessionLabel private val expiredSessionSubject: PublishSubject<Boolean>
) {

    @Deprecated("Use system dialogs")
    fun handleError(exception: Throwable, messageListener: (String) -> Unit = {}) {
        when (exception) {
            is TimeoutException -> {
                messageListener(resourceManager.getString(R.string.timeout_error))
            }
            is UnknownHostException -> {
                messageListener(resourceManager.getString(R.string.network_error))
            }
            is InvalidTokenException, is InvalidUserDataException -> {
                expiredSessionSubject.onNext(true)
            }
            else -> {
                messageListener(resourceManager.getString(R.string.unknown_error))
            }
        }
    }

    fun handleErrorDialog(
        exception: Throwable,
        messageListener: (ErrorDialogType) -> Unit = {}
    ) {
        when (exception) {
            is UnknownHostException -> {
                messageListener(ErrorDialogType.NETWORK_CONNECTION_ERROR)
            }
            is InvalidTokenException, is InvalidUserDataException -> {
                expiredSessionSubject.onNext(true)
            }
            else -> {
                messageListener(ErrorDialogType.UNKNOWN_ERROR)
            }
        }
    }

    fun onDestroy() {
    }
}