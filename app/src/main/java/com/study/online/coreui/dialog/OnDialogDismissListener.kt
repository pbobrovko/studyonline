package com.study.online.coreui.dialog

interface OnDialogDismissListener {

    fun onDismiss()
}