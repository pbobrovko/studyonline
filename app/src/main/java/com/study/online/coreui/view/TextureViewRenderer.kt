//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//
package com.study.online.coreui.view

import android.content.Context
import android.content.res.Resources.NotFoundException
import android.graphics.Point
import android.graphics.SurfaceTexture
import android.opengl.GLES20
import android.os.Handler
import android.os.HandlerThread
import android.util.AttributeSet
import android.view.Surface
import android.view.SurfaceHolder
import android.view.TextureView
import org.webrtc.*
import org.webrtc.RendererCommon.*
import org.webrtc.VideoRenderer.I420Frame
import java.util.concurrent.CountDownLatch

class TextureViewRenderer : TextureView, SurfaceHolder.Callback, VideoRenderer.Callbacks,
    TextureView.SurfaceTextureListener {
    private var renderThread: HandlerThread? = null
    private val handlerLock = Any()
    private var renderThreadHandler: Handler? = null
    private var eglBase: EglBase? = null
    private val yuvUploader = YuvUploader()
    private var drawer: GlDrawer? = null
    private var yuvTextures: IntArray? = null
    private val frameLock = Any()
    private var pendingFrame: I420Frame? = null
    private val layoutLock = Any()
    private var desiredLayoutSize = Point()
    private val layoutSize = Point()
    private val surfaceSize = Point()
    private var isSurfaceCreated = false
    private var frameWidth = 0
    private var frameHeight = 0
    private var frameRotation = 0
    private var scalingType: ScalingType
    private var mirror = false
    private var rendererEvents: RendererEvents? = null
    private val statisticsLock: Any
    private var framesReceived = 0
    private var framesDropped = 0
    private var framesRendered = 0
    private var firstFrameTimeNs: Long = 0
    private var renderTimeNs: Long = 0
    private val renderFrameRunnable = Runnable { renderFrameOnRenderThread() }
    private val makeBlackRunnable = Runnable { makeBlack() }

    constructor(context: Context?) : super(context) {
        scalingType = ScalingType.SCALE_ASPECT_BALANCED
        statisticsLock = Any()
        surfaceTextureListener = this
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        scalingType = ScalingType.SCALE_ASPECT_BALANCED
        statisticsLock = Any()
        surfaceTextureListener = this
    }

    @JvmOverloads
    fun init(
        sharedContext: EglBase.Context?,
        rendererEvents: RendererEvents?,
        configAttributes: IntArray? = EglBase.CONFIG_PLAIN,
        drawer: GlDrawer? = GlRectDrawer()
    ) {
        synchronized(handlerLock) {
            check(renderThreadHandler == null) { resourceName + "Already initialized" }
            Logging.d("SurfaceViewRenderer", resourceName + "Initializing.")
            this.rendererEvents = rendererEvents
            this.drawer = drawer
            renderThread = HandlerThread("SurfaceViewRenderer")
            renderThread!!.start()
            eglBase = EglBase.create(sharedContext, configAttributes)
            renderThreadHandler = Handler(renderThread!!.looper)
        }
        tryCreateEglSurface()
    }

    fun tryCreateEglSurface() {
        runOnRenderThread(Runnable {
            synchronized(layoutLock) {
                if (isAvailable && eglBase?.hasSurface() != true) {
                    eglBase?.createSurface(Surface(surfaceTexture))
                    eglBase?.makeCurrent()
                    GLES20.glPixelStorei(3317, 1)
                }
            }
        })
    }

    fun release() {
        val eglCleanupBarrier = CountDownLatch(1)
        synchronized(this.handlerLock) {
            if (this.renderThreadHandler == null) {
                Logging.d("SurfaceViewRenderer", this.resourceName + "Already released")
                return
            }
            this.renderThreadHandler?.postAtFrontOfQueue {
                this@TextureViewRenderer.drawer?.release()
                this@TextureViewRenderer.drawer = null
                if (this@TextureViewRenderer.yuvTextures != null) {
                    GLES20.glDeleteTextures(3, this@TextureViewRenderer.yuvTextures, 0)
                    this@TextureViewRenderer.yuvTextures = null
                }
                this@TextureViewRenderer.makeBlack()
                this@TextureViewRenderer.eglBase?.release()
                this@TextureViewRenderer.eglBase = null
                eglCleanupBarrier.countDown()
            }
            this.renderThreadHandler = null
        }
        ThreadUtils.awaitUninterruptibly(eglCleanupBarrier)
        this.renderThread!!.quit()
        synchronized(this.frameLock) {
            if (this.pendingFrame != null) {
                VideoRenderer.renderFrameDone(this.pendingFrame)
                this.pendingFrame = null
            }
        }
        ThreadUtils.joinUninterruptibly(this.renderThread)
        this.renderThread = null
        synchronized(this.layoutLock) {
            this.frameWidth = 0
            this.frameHeight = 0
            this.frameRotation = 0
            this.rendererEvents = null
        }
        this.resetStatistics()
    }

    fun resetStatistics() {
        synchronized(this.statisticsLock) {
            this.framesReceived = 0
            this.framesDropped = 0
            this.framesRendered = 0
            this.firstFrameTimeNs = 0L
            this.renderTimeNs = 0L
        }
    }

    fun setMirror(mirror: Boolean) {
        synchronized(this.layoutLock) { this.mirror = mirror }
    }

    fun setScalingType(scalingType: ScalingType) {
        synchronized(this.layoutLock) { this.scalingType = scalingType }
    }

    override fun renderFrame(frame: I420Frame) {
        synchronized(this.statisticsLock) { ++this.framesReceived }
        synchronized(this.handlerLock) {
            if (this.renderThreadHandler == null) {
                Logging.d(
                    "SurfaceViewRenderer",
                    this.resourceName + "Dropping frame - Not initialized or already released."
                )
                VideoRenderer.renderFrameDone(frame)
            } else {
                synchronized(this.frameLock) {
                    if (this.pendingFrame != null) {
                        synchronized(this.statisticsLock) { ++this.framesDropped }
                        VideoRenderer.renderFrameDone(this.pendingFrame)
                    }
                    this.pendingFrame = frame
                    this.updateFrameDimensionsAndReportEvents(frame)
                    this.renderThreadHandler!!.post(this.renderFrameRunnable)
                }
            }
        }
    }

    private fun getDesiredLayoutSize(widthSpec: Int, heightSpec: Int): Point {
        synchronized(this.layoutLock) {
            val maxWidth = getDefaultSize(2147483647, widthSpec)
            val maxHeight = getDefaultSize(2147483647, heightSpec)
            val size = getDisplaySize(
                this.scalingType,
                this.frameAspectRatio(),
                maxWidth,
                maxHeight
            )
            if (MeasureSpec.getMode(widthSpec) == (1073741824).toInt()) {
                size.x = maxWidth
            }
            if (MeasureSpec.getMode(heightSpec) == (1073741824).toInt()) {
                size.y = maxHeight
            }
            return size
        }
    }

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        synchronized(this.layoutLock) {
            if (this.frameWidth != 0 && this.frameHeight != 0) {
                this.desiredLayoutSize = this.getDesiredLayoutSize(widthSpec, heightSpec)
                if (this.desiredLayoutSize.x != this.measuredWidth || this.desiredLayoutSize.y != this.measuredHeight) {
                    synchronized(this.handlerLock) {
                        if (this.renderThreadHandler != null) {
                            this.renderThreadHandler!!.postAtFrontOfQueue(this.makeBlackRunnable)
                        }
                    }
                }
                this.setMeasuredDimension(this.desiredLayoutSize.x, this.desiredLayoutSize.y)
            } else {
                super.onMeasure(widthSpec, heightSpec)
            }
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        synchronized(this.layoutLock) {
            this.layoutSize.x = right - left
            this.layoutSize.y = bottom - top
        }
        this.runOnRenderThread(this.renderFrameRunnable)
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        Logging.d("SurfaceViewRenderer", this.resourceName + "Surface created.")
        synchronized(this.layoutLock) { this.isSurfaceCreated = true }
        this.tryCreateEglSurface()
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        Logging.d("SurfaceViewRenderer", this.resourceName + "Surface destroyed.")
        synchronized(this.layoutLock) {
            this.isSurfaceCreated = false
            this.surfaceSize.x = 0
            this.surfaceSize.y = 0
        }
        this.runOnRenderThread(Runnable { this@TextureViewRenderer.eglBase?.releaseSurface() })
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        Logging.d(
            "SurfaceViewRenderer",
            this.resourceName + "Surface changed: " + width + "x" + height
        )
        synchronized(this.layoutLock) {
            this.surfaceSize.x = width
            this.surfaceSize.y = height
        }
        this.runOnRenderThread(this.renderFrameRunnable)
    }

    private fun runOnRenderThread(runnable: Runnable) {
        synchronized(this.handlerLock) {
            if (this.renderThreadHandler != null) {
                this.renderThreadHandler!!.post(runnable)
            }
        }
    }

    private val resourceName: String
        private get() = try {
            this.resources.getResourceEntryName(this.id) + ": "
        } catch (var2: NotFoundException) {
            ""
        }

    private fun makeBlack() {
        check(!(Thread.currentThread() !== this.renderThread)) { this.resourceName + "Wrong thread." }
        if (this.eglBase != null && this.eglBase?.hasSurface() == true) {
            GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f)
            GLES20.glClear(16384)
            this.eglBase?.swapBuffers()
        }
    }

    private fun checkConsistentLayout(): Boolean {
        check(!(Thread.currentThread() !== this.renderThread)) { this.resourceName + "Wrong thread." }
        synchronized(this.layoutLock) { return this.layoutSize == this.desiredLayoutSize && this.surfaceSize == this.layoutSize }
    }

    private fun renderFrameOnRenderThread() {
        check(!(Thread.currentThread() !== this.renderThread)) { this.resourceName + "Wrong thread." }
        var frame: I420Frame?
        synchronized(this.frameLock) {
            if (this.pendingFrame == null) {
                return
            }
            frame = this.pendingFrame
            this.pendingFrame = null
        }
        if (this.eglBase != null && this.eglBase?.hasSurface() == true) {
            if (!this.checkConsistentLayout()) {
                this.makeBlack()
                VideoRenderer.renderFrameDone(frame)
            } else {
                synchronized(this.layoutLock) {
                    if (this.eglBase?.surfaceWidth() != this.surfaceSize.x || this.eglBase?.surfaceHeight() != this.surfaceSize.y) {
                        this.makeBlack()
                    }
                }
                val startTimeNs = System.nanoTime()
                var texMatrix: FloatArray?
                synchronized(this.layoutLock) {
                    val rotatedSamplingMatrix =
                        rotateTextureMatrix(
                            frame!!.samplingMatrix, frame!!.rotationDegree.toFloat()
                        )
                    val layoutMatrix = getLayoutMatrix(
                        this.mirror,
                        this.frameAspectRatio(),
                        this.layoutSize.x.toFloat() / this.layoutSize.y.toFloat()
                    )
                    texMatrix =
                        multiplyMatrices(rotatedSamplingMatrix, layoutMatrix)
                }
                GLES20.glClear(16384)
                if (frame!!.yuvFrame) {
                    if (this.yuvTextures == null) {
                        this.yuvTextures = IntArray(3)
                        for (i in 0..2) {
                            this.yuvTextures!![i] = GlUtil.generateTexture(3553)
                        }
                    }
                    this.yuvUploader.uploadYuvData(
                        this.yuvTextures,
                        frame!!.width,
                        frame!!.height,
                        frame!!.yuvStrides,
                        frame!!.yuvPlanes
                    )
                    this.drawer!!.drawYuv(
                        this.yuvTextures,
                        texMatrix,
                        0,
                        0,
                        this.surfaceSize.x,
                        this.surfaceSize.y
                    )
                } else {
                    this.drawer!!.drawOes(
                        frame!!.textureId,
                        texMatrix,
                        0,
                        0,
                        this.surfaceSize.x,
                        this.surfaceSize.y
                    )
                }
                this.eglBase?.swapBuffers()
                VideoRenderer.renderFrameDone(frame)
                synchronized(this.statisticsLock) {
                    if (this.framesRendered == 0) {
                        this.firstFrameTimeNs = startTimeNs
                        synchronized(this.layoutLock) {
                            Logging.d(
                                "SurfaceViewRenderer",
                                this.resourceName + "Reporting first rendered frame."
                            )
                            if (this.rendererEvents != null) {
                                this.rendererEvents!!.onFirstFrameRendered()
                            }
                        }
                    }
                    ++this.framesRendered
                    this.renderTimeNs += System.nanoTime() - startTimeNs
                    if (this.framesRendered % 300 == 0) {
                        this.logStatistics()
                    }
                }
            }
        } else {
            Logging.d("SurfaceViewRenderer", this.resourceName + "No surface to draw on")
            VideoRenderer.renderFrameDone(frame)
        }
    }

    private fun frameAspectRatio(): Float {
        synchronized(this.layoutLock) {
            return if (this.frameWidth != 0 && this.frameHeight != 0) {
                if (this.frameRotation % 180 == 0) this.frameWidth.toFloat() / this.frameHeight.toFloat() else this.frameHeight.toFloat() / this.frameWidth.toFloat()
            } else {
                0.0f
            }
        }
    }

    private fun updateFrameDimensionsAndReportEvents(frame: I420Frame) {
        synchronized(this.layoutLock) {
            if (this.frameWidth != frame.width || this.frameHeight != frame.height || this.frameRotation != frame.rotationDegree) {
                Logging.d(
                    "SurfaceViewRenderer",
                    this.resourceName + "Reporting frame resolution changed to " + frame.width + "x" + frame.height + " with rotation " + frame.rotationDegree
                )
                if (this.rendererEvents != null) {
                    this.rendererEvents!!.onFrameResolutionChanged(
                        frame.width,
                        frame.height,
                        frame.rotationDegree
                    )
                }
                this.frameWidth = frame.width
                this.frameHeight = frame.height
                this.frameRotation = frame.rotationDegree
                this.post { this@TextureViewRenderer.requestLayout() }
            }
        }
    }

    private fun logStatistics() {
        synchronized(this.statisticsLock) {
            Logging.d(
                "SurfaceViewRenderer",
                this.resourceName + "Frames received: " + this.framesReceived + ". Dropped: " + this.framesDropped + ". Rendered: " + this.framesRendered
            )
            if (this.framesReceived > 0 && this.framesRendered > 0) {
                val timeSinceFirstFrameNs =
                    System.nanoTime() - this.firstFrameTimeNs
                Logging.d(
                    "SurfaceViewRenderer",
                    this.resourceName + "Duration: " + (timeSinceFirstFrameNs.toDouble() / 1000000.0).toInt() + " ms. FPS: " + this.framesRendered.toDouble() * 1.0E9 / timeSinceFirstFrameNs.toDouble()
                )
                Logging.d(
                    "SurfaceViewRenderer",
                    this.resourceName + "Average render time: " + (this.renderTimeNs / (1000 * this.framesRendered).toLong()).toInt() + " us."
                )
            }
        }
    }

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
        Logging.d("SurfaceViewRenderer", this.resourceName + "Surface created.")
        synchronized(this.layoutLock) { this.isSurfaceCreated = true }
        this.tryCreateEglSurface()
    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture?, width: Int, height: Int) {
        Logging.d(
            "SurfaceViewRenderer",
            this.resourceName + "Surface changed: " + width + "x" + height
        )
        synchronized(this.layoutLock) {
            this.surfaceSize.x = width
            this.surfaceSize.y = height
        }
        this.runOnRenderThread(this.renderFrameRunnable)
    }

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture?): Boolean {
        Logging.d("SurfaceViewRenderer", this.resourceName + "Surface destroyed.")
        synchronized(this.layoutLock) {
            this.isSurfaceCreated = false
            this.surfaceSize.x = 0
            this.surfaceSize.y = 0
        }
        this.runOnRenderThread(Runnable { this@TextureViewRenderer.eglBase?.releaseSurface() })
        return true
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture?) {
        Logging.d(
            "SurfaceViewRenderer",
            this.resourceName + "Surface changed: " + width + "x" + height
        )
        synchronized(this.layoutLock) {
            this.surfaceSize.x = width
            this.surfaceSize.y = height
        }
        this.runOnRenderThread(this.renderFrameRunnable)
    }

    companion object {
        private const val TAG = "SurfaceViewRenderer"
    }
}