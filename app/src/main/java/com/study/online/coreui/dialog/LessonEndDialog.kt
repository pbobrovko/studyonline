package com.study.online.coreui.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import com.study.online.R

class LessonEndDialog : DialogFragment() {

    private var onDialogDismissListener: OnDialogDismissListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        onDialogDismissListener = parentFragment as? OnDialogDismissListener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_lesson_end, container)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val positiveButton = view.findViewById<MaterialButton>(R.id.lessonEndedButton)
        positiveButton.setOnClickListener { dismiss() }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        super.onCreateDialog(savedInstanceState).apply {
            window?.decorView?.setBackgroundResource(R.color.transparent)
        }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

        onDialogDismissListener?.onDismiss()
    }
}