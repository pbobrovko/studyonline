package com.study.online.coreui.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.study.online.domain.entity.socket.lessonboard.Line
import com.study.online.domain.entity.socket.lessonboard.Position
import com.study.online.domain.entity.socket.lessonboard.Shape
import com.study.online.domain.entity.socket.lessonboard.ShapeData

class BoardView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : View(context, attrs) {

    private var boardWidth = 0
    private var boardHeight = 0
    private var percentBoardWidth = 0f
    private var percentBoardHeight = 0f
    private var percentWidth = 0f
    private var percentHeight = 0f
    private val shapes = mutableMapOf<String, MutableList<Shape>>()
    private val paints = mutableMapOf<String, Paint>()
    private var onSizeChangedListener: OnSizeChangedListener? = null

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (boardWidth > 0 && boardHeight > 0) {
            val height = getBoardHeight(measuredWidth)
            setMeasuredDimension(widthMeasureSpec, height)
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        percentWidth = w / HUNDRED_PERCENT
        percentHeight = h / HUNDRED_PERCENT

        if (boardWidth != 0 && boardHeight != 0) {
            onSizeChangedListener?.onSizeChanged(this)
        }
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        shapes.forEach { (userName, shapes) ->
            var paint = paints[userName]
            paint?.let {
                shapes.forEach { shape ->
                    it.color = Color.parseColor(shape.color)
                    it.strokeWidth = shape.thickness * POINT_WIDTH

                    shape.coordinates.forEach { line ->
                        canvas.drawLine(
                            line.startPos.x,
                            line.startPos.y,
                            line.endPos.x,
                            line.endPos.y,
                            it
                        )
                    }
                }
            }
        }
    }

    fun setOnSizeChangedListener(listener: OnSizeChangedListener) {
        onSizeChangedListener = listener
    }

    fun setBoardSize(width: Int, height: Int) {
        boardWidth = width
        boardHeight = height
        percentBoardWidth = width / HUNDRED_PERCENT
        percentBoardHeight = height / HUNDRED_PERCENT
        requestLayout()
    }

    fun drawShapes(shapesData: List<ShapeData>) {
        shapesData.forEach { calculatePosition(it) }

        if (boardWidth != 0 && boardHeight != 0) {
            invalidate()
        }
    }

    fun undoShape(shapeData: ShapeData) {
        val userName = shapeData.changeId.userName
        val userShapes = shapes[userName]

        val undoShape = userShapes?.find {
            it.changeId.userName == userName
            it.changeId.changeNumber == shapeData.changeId.changeNumber
        }

        if (undoShape != null) {
            userShapes.remove(undoShape)
            invalidate()
        }
    }

    fun cleanBoard() {
        shapes.clear()
        paints.clear()
        invalidate()
    }

    private fun getBoardHeight(widthMeasureSpec: Int): Int {
        val boardRatio = boardWidth / boardHeight.toFloat()

        return (widthMeasureSpec / boardRatio).toInt()
    }

    private fun calculatePosition(shapeData: ShapeData) {
        val shape = shapeData.shape
        val localCoordinates = mutableListOf<Line>()
        shape.coordinates.forEach {
            val startX = calculateX(it.startPos.x)
            val startY = calculateY(it.startPos.y)
            val endX = calculateX(it.endPos.x)
            val endY = calculateY(it.endPos.y)

            val startPosition = Position(startX, startY)
            val endPosition = Position(endX, endY)
            val localLine = Line(startPosition, endPosition)

            localCoordinates.add(localLine)
        }

        val localShape = shape.copy(coordinates = localCoordinates)

        val userShape = if (shapes.contains(shapeData.changeId.userName)) {
            shapes[shapeData.changeId.userName]
        } else {
            checkUserPaint(shapeData.changeId.userName)
            mutableListOf()
        }

        userShape?.let {
            it.add(localShape)
            shapes[shapeData.changeId.userName] = it
        }
    }

    private fun calculateX(x: Float): Float {
        val xPercent = x / percentBoardWidth
        return percentWidth * xPercent
    }

    private fun calculateY(y: Float): Float {
        val yPercent = y / percentBoardHeight
        return percentHeight * yPercent
    }

    private fun checkUserPaint(userName: String) {
        if (!paints.contains(userName)) {
            paints[userName] = getPaint()
        }
    }

    private fun getPaint(): Paint =
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            isAntiAlias = true
            isDither = true
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
        }

    interface OnSizeChangedListener {

        fun onSizeChanged(view: BoardView)
    }

    companion object {
        private const val HUNDRED_PERCENT = 100f
        private const val POINT_WIDTH = 0.75f
    }
}