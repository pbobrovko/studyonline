package com.study.online.coreui.dialog.errordialog

enum class ErrorDialogType {
    NETWORK_CONNECTION_ERROR,
    UNKNOWN_ERROR,
    FEATURE_IN_DEVELOPMENT
}