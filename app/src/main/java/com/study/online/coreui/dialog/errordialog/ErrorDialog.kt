package com.study.online.coreui.dialog.errordialog

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.study.online.R
import com.study.online.app.di.DI
import com.study.online.core.global.Screens
import com.study.online.extension.tryToGetSerializable
import kotlinx.android.synthetic.main.dialog_fragment_unknown_error.errorFragmentDialogButtonClose
import kotlinx.android.synthetic.main.dialog_fragment_unknown_error.errorFragmentDialogButtonPositive
import kotlinx.android.synthetic.main.dialog_fragment_unknown_error.techSupportTextView
import ru.terrakok.cicerone.Router
import toothpick.Toothpick
import javax.inject.Inject

class ErrorDialog : DialogFragment() {

    @Inject
    lateinit var router: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScopes(DI.APP_SCOPE))

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val type = tryToGetSerializable(ERROR_TYPE) as ErrorDialogType
        return inflater.inflate(getLayoutIdByType(type), container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        errorFragmentDialogButtonPositive.setOnClickListener { dismiss() }
        errorFragmentDialogButtonClose.setOnClickListener { dismiss() }
        techSupportTextView?.setOnClickListener {
            dismiss()
            router.navigateTo(Screens.SupportFlow)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        super.onCreateDialog(savedInstanceState).apply {
            window?.decorView?.setBackgroundResource(R.color.transparent)
        }

    private fun getLayoutIdByType(dialogType: ErrorDialogType): Int {
        return when (dialogType) {
            ErrorDialogType.NETWORK_CONNECTION_ERROR -> R.layout.dialog_fragment_network_error
            ErrorDialogType.UNKNOWN_ERROR -> R.layout.dialog_fragment_unknown_error
            ErrorDialogType.FEATURE_IN_DEVELOPMENT -> R.layout.dialog_feature_in_development
        }
    }

    companion object {
        private const val ERROR_TYPE = "ERROR_TYPE"

        fun newInstance(errorDialogType: ErrorDialogType): ErrorDialog {
            return ErrorDialog()
                .apply {
                    arguments = Bundle().apply {
                        putSerializable(ERROR_TYPE, errorDialogType)
                    }
                }
        }
    }
}